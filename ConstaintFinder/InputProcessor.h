#include <iostream>
#include <string>
using namespace std;

namespace ConstraintFinder
{
	namespace UserInterface
	{
		class InputProcessor
		{
		protected:
			bool LMBpressed;
			bool RMBpressed;
			bool MMBpressed;
			int oldX;
			int oldY;
			int deltaX;
			int deltaY;
			string command;

		public:
			InputProcessor();
			virtual void mouseDown(int x, int y, unsigned short button);
			virtual void mouseUp(int x, int y, unsigned short button);
			virtual void mouseMoved(int x, int y);

			virtual void keyDown(unsigned char k);
			virtual void keyUp(unsigned char k);
			virtual void keyPress(unsigned char k);

			string getCommandStr();
			virtual void executeCommand(string command);

			bool getLMBpressed();
			bool getMMBpressed();
			bool getRMBpressed();
		};
	}
}