#include "GLEntityGroup.h"

namespace ConstraintFinder
{
	namespace Graphics
	{
		GLEntityGroup::GLEntityGroup(unsigned int id)
			:EntityGroup(id)
		{
		}

		GLEntityGroup::GLEntityGroup(unsigned int id, vector<GLEntity*> entities)
			:EntityGroup(id, GLEntitiesToEntities(entities))
		{
			this->displayables = entities;
		}

		GLEntityGroup::GLEntityGroup(unsigned int id, vector<GLEntity*> entities, const Vector3& translation, const Matrix33& rotation, const Vector3& scaling)
			:EntityGroup(id, GLEntitiesToEntities(entities), translation, rotation, scaling)
		{
			this->displayables = entities;
		}

		GLEntityGroup::GLEntityGroup(unsigned int id, 
				vector<GLEntity*> entities, 
				const Vector3& translation, 
				const Matrix33& rotation, 
				const Vector3& scaling,
				const Vector3& center, 
				const Frame& localFrame)
				:EntityGroup(id, GLEntitiesToEntities(entities), translation, rotation, scaling, center, localFrame)
		{
			this->displayables = entities;
		}

		void GLEntityGroup::createDisplayList()
		{
			//unused
		}

		void GLEntityGroup::callMyList()
		{
			//usused
		}

		void GLEntityGroup::renderGL()
		{
			for (vector<GLEntity*>::iterator it = displayables.begin(); it != displayables.end(); ++it)
				(*it)->renderGL();
		}

		Entity* GLEntityGroup::toEntity()
		{
			return dynamic_cast<Entity*>(this);
		}

		vector<Entity*> GLEntityGroup::GLEntitiesToEntities(vector<GLEntity*> glEnts)
		{
			vector<Entity*> ents;
			for (vector<GLEntity*>::iterator it = glEnts.begin(); it != glEnts.end(); ++it)
				ents.push_back((*it)->toEntity());
			return ents;
		}

		vector<GLEntity*> GLEntityGroup::getDisplayables()
		{
			return displayables;
		}

		void GLEntityGroup::addEntity(GLEntity *e)
		{
			this->entities.push_back(e->toEntity());
			this->displayables.push_back(e);
		}

		GLEntity* GLEntityGroup::queryId(unsigned int id)
		{
			if (id == toEntity()->getId())
				return this;
			for (vector<GLEntity*>::iterator i = displayables.begin(); i != displayables.end(); ++i)
			{
				GLEntity* t = (*i)->queryId(id);
				if (t) 
					return t;
			}
			return NULL;
		}

	}
}