#pragma once
#include "GLBasicShape.h"
#include "Ellipsoid.h"
using namespace ConstraintFinder::Core;

namespace ConstraintFinder
{
	namespace Graphics
	{
		/*! \biref A local axis aligned ellipsoid displayable using OpenGL.
		* 
		* Extends the Cuboid and GLDisplayable classes.
		* \sa Ellipsoid, GLDisplayable
		*/
		class GLEllipsoid : public Ellipsoid, public GLBasicShape
		{
		protected:
			void createDisplayList();
		public:
			GLEllipsoid(unsigned int id, double eX, double eY, double eZ);
			void renderGL();
			void callMyList();
			Entity* toEntity();
			GLEntity* queryId(unsigned int id);
		};
	};
};