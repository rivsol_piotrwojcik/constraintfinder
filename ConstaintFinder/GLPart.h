#pragma once
#include "GLEntityGroup.h"
#include "ConstraintProcessor.h"
#include "GLCsgNode.h"
using namespace ConstraintFinder::Core;

namespace ConstraintFinder
{
	namespace Graphics
	{
		/*! \brief Represents a single part in the constraint finder program displayable by OpenGL
		* 
		* A single displayable part may consisit of any number of displayable entities (Eeither base shapes, 
		* csg trees or entity groups).
		* The GLPart class is implemented to display CSG geometry using the OpenCSG rendering library.
		* The GLPart class is a derivative of Core::Part
		* \sa Part, Entity, GLDisplayable, EntityGroup
		*/
		class GLPart : public GLEntityGroup
		{
		protected:
			vector<Constraint*> constraints;
			vector<GLEntity*> selected;
			bool renderSelected;
		public:
			/*! Constructor.
			* \param id the unique id assigend to each entity.
			*/
			GLPart(unsigned int id);
			void detectConstraints();
			void listConstraints();
			void listEntities();
			void selectEntity(unsigned int id);
			void selectConstraint(int index);
			void renderGL();
			void clearSelect();
		};
	}
}