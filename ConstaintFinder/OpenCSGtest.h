#pragma once
#include <vector>
using namespace std;

#include <opencsg.h>

namespace OpenCSG {

	class DisplayListPrimitive : public Primitive {
    public:
        /// An object of this class contains the OpenGL id of a display
        /// list that is compiled by the application. render() just invokes
        /// this display list. 
        /// Operation and convexity are just forwarded to the base Primitive class.
        DisplayListPrimitive(unsigned int displayListId_, Operation, unsigned int convexity);

        /// Sets the display list id
        void setDisplayListId(unsigned int);
        /// Returns the display list id
        unsigned int getDisplayListId() const;

        /// Calls the display list.
        virtual void render();

    private:
        unsigned int mDisplayListId;
    };
}

namespace ConstraintFinder
{
	namespace UserInterface
	{
		void setBasicShape(vector<OpenCSG::Primitive*>* primitives);
	}
}