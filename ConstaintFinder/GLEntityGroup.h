#pragma once
#include "GLEntity.h"
#include "EntityGroup.h"

namespace ConstraintFinder
{
	namespace Graphics
	{
		class GLEntityGroup : public GLEntity, public EntityGroup
		{
		protected:
			vector<GLEntity*> displayables;
			virtual vector<Entity*> GLEntitiesToEntities(vector<GLEntity*> glEnts);
		public:
			GLEntityGroup(unsigned int id);
			GLEntityGroup(unsigned int id, vector<GLEntity*> entities);
			GLEntityGroup(unsigned int id, vector<GLEntity*> entities, const Vector3& translation, const Matrix33& rotation, const Vector3& scaling);
			GLEntityGroup(unsigned int id, 
				vector<GLEntity*> entities, 
				const Vector3& translation, 
				const Matrix33& rotation, 
				const Vector3& scaling,
				const Vector3& center, 
				const Frame& localFrame);
			void createDisplayList();
			void callMyList();
			void renderGL();
			Entity* toEntity();
			vector<GLEntity*> getDisplayables();
			void addEntity(GLEntity* e);
			GLEntity* queryId(unsigned int id);
		};
	}
}