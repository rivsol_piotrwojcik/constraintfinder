#include "GLEllipsoid.h"

namespace ConstraintFinder
{
	namespace Graphics
	{
		GLEllipsoid::GLEllipsoid(unsigned int id, double eX, double eY, double eZ)
			:Ellipsoid(id, eX, eY, eZ), GLBasicShape()
		{
		}

		void GLEllipsoid::createDisplayList()
		{
		}

		void GLEllipsoid::renderGL()
		{
			glPushMatrix();
			glTranslatef((GLfloat)translation.getX(), (GLfloat)translation.getY(), (GLfloat)translation.getZ());
			GLfloat* rot = rotation.toGLfloat16();
			glMultMatrixf(rot);
			delete [] rot;
			glScalef((GLfloat)scaling.getX(), (GLfloat)scaling.getY(), (GLfloat)scaling.getZ());
			this->callMyList();
			glPopMatrix();
		}

		void GLEllipsoid::callMyList()
		{
			if (UnitShapes::unitSphere == 0)
				UnitShapes::unitSphere = UnitShapes::initUnitSphere();
			glScalef( this->eX, this->eY, this->eZ );
			glCallList(UnitShapes::unitSphere);
		}

		Entity* GLEllipsoid::toEntity()
		{
			return dynamic_cast<Entity*>(this);
		}

		GLEntity* GLEllipsoid::queryId(unsigned int id)
		{
			if (id == toEntity()->getId())
				return this;
			return NULL;
		}
	}
}