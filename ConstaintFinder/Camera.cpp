#include "Camera.h"

namespace ConstraintFinder
{
	namespace UserInterface
	{
		Camera::Camera()
		{
			up = Vector3::unitY();
			position = Vector3(0,0,10);
			lookAt = Vector3::zero();
		}

		Camera::Camera(const Vector3& pos, const Vector3& lookAt, const Vector3& up)
		{
			position = pos;
			this->lookAt = lookAt;
			this->up = up;
		}

		void Camera::applyView()
		{
			gluLookAt(position.getX(), position.getY(), position.getZ(),
				lookAt.getX(), lookAt.getY(), lookAt.getZ(),
				up.getX(), up.getY(), up.getZ());
		}

		Vector3 Camera::getPosition()
		{
			return position;
		}

		void Camera::setPosition(const Vector3& v)
		{
			position = v;
		}

		Vector3 Camera::getLookAt()
		{
			return lookAt;
		}

		void Camera::setLookAt(const Vector3& v)
		{
			lookAt = v;
		}

		Vector3 Camera::getUpVector()
		{
			return up;
		}

		void Camera::setUpVector(const Vector3& v)
		{
			up = v;
		}

		void Camera::move(Vector3& v)
		{
			this->position += v;
		}

		void Camera::rotate(double rX, double rY, double rZ)
		{
			Matrix33 rot = Matrix33::rotationX(rX) * Matrix33::rotationY(rY) * Matrix33::rotationZ(rZ);
			this->position =  rot * this->position;
		}

		SphericalCamera::SphericalCamera() :Camera()
		{
			radius = 10;
			alpha = 0;
			beta = 0;
			this->setPosition(Vector3(radius*cos(alpha)*sin(beta), radius*sin(alpha)*cos(beta), radius*cos(beta)));
		}

		void SphericalCamera::move(Vector3& t)
		{
			rotate(t.getX(), t.getY(), t.getZ());
		}

		void SphericalCamera::rotate(double rX, double rY, double rZ)
		{
			radius += rX;
			alpha += rY;
			beta += rZ;
			this->setPosition(Vector3(radius*cos(alpha)*sin(beta), radius*sin(alpha)*cos(beta), radius*cos(beta)));
		}
	}
}