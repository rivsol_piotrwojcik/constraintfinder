#include "tinyxml.h"
#include "tinystr.h"
#include "GLPart.h"
#include "GLCuboid.h"
#include "GLEllipsoid.h"
#include "GLCylinder.h"
#include "GLCsgNode.h"
#include "GLEntityGroup.h"
#include "IdMaster.h"

using namespace std;
using namespace ConstraintFinder::Graphics;

namespace ConstraintFinder
{
	namespace UserInterface
	{
		class DataFileProcessor
		{
		protected:
			static void validateDefinition(TiXmlElement* definition, vector<string>& ids);
			static void validateTransform(TiXmlElement* translate, TiXmlElement* rotate, TiXmlElement* scale);
			static bool idOnList(string id, vector<string>& ids);
			static bool hasChild(TiXmlElement* e, string name);
			static bool hasUniqueChild(TiXmlElement* e, string name);
			static bool hasAttribute(TiXmlElement* e, string name);
			static bool hasUniqueAttribute(TiXmlElement* e, string name);
			static bool isCuboid(TiXmlElement* e);
			static bool isEllipsoid(TiXmlElement* e);
			static bool isCylinder(TiXmlElement* e);
			static bool isCSG(TiXmlElement* e);
			static bool isGroup(TiXmlElement* e);
			static bool isTopLevel(TiXmlElement* e);
			static GLPart* constructPart(const TiXmlHandle& root);
			static GLCuboid* loadCuboid(TiXmlElement* e);
			static GLEllipsoid* loadEllipsoid(TiXmlElement* e);
			static GLCylinder* loadCylinder(TiXmlElement* e);
			static GLCsgNode* loadCSG(TiXmlElement* e, vector<TiXmlElement*> entities);
			static GLEntityGroup* loadGroup(TiXmlElement* e, vector<TiXmlElement*> entities);
			static unsigned int getNextId();
		public:
			static void validateDataFile(TiXmlDocument* dataFile);
			static GLPart* loadPartFromFile(string filename);
		};
	}
}