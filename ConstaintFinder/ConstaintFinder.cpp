#include "ConstraintFinder.h"

int main(int argc, char* argv[])
{
	Engine* engine = Engine::instance();
	engine->initialize(argc, argv, SCRWIDTH, SCRHEIGHT);
	engine->mainloop();
	return 0;
}

