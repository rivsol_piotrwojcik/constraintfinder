#include "GLCylinder.h"

namespace ConstraintFinder
{
	namespace Graphics
	{
		GLCylinder::GLCylinder(unsigned int id, double radius, double length)
			:Cylinder(id, radius, length), GLBasicShape()
		{
		}

		void GLCylinder::createDisplayList()
		{
		}

		void GLCylinder::renderGL()
		{
			glPushMatrix();
			glTranslatef((GLfloat)translation.getX(), (GLfloat)translation.getY(), (GLfloat)translation.getZ());
			GLfloat* rot = rotation.toGLfloat16();
			glMultMatrixf(rot);
			delete [] rot;
			glScalef((GLfloat)scaling.getX(), (GLfloat)scaling.getY(), (GLfloat)scaling.getZ());
			this->callMyList();
			glPopMatrix();
		}

		void GLCylinder::callMyList()
		{
			if (UnitShapes::unitCylinder == 0)
				UnitShapes::unitCylinder = UnitShapes::initUnitCylinder();
			glScalef(radius, radius, length);
			glCallList(UnitShapes::unitCylinder);
		}

		Entity* GLCylinder::toEntity()
		{
			return dynamic_cast<Entity*>(this);
		}

		GLEntity* GLCylinder::queryId(unsigned int id)
		{
			if (id == toEntity()->getId())
				return this;
			return NULL;
		}
	}
}