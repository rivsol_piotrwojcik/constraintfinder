#pragma once
#include <GL/glew.h>
#include <GL/glut.h>
#include <iostream>
#include "Entity.h"
using namespace std;

using namespace ConstraintFinder::Core;

namespace ConstraintFinder
{
	namespace Graphics
	{
		/*! \biref Represents a basic shape entity.
		* 
		* This class represents all basic building blocks of the CSG geometry.
		* Derivatives of this class will be leaves in any CSG tree.
		* All derivatives of the BasicShape class should be identified by the type prefix of "ET_BS_"
		* This class also derives form OpenCSG::Primitive to support processing by the OpenSCG rendering library.
		*/
		class GLEntity
		{
		protected:
			/*! Creates an OpenGL dispaly list representing this shape.*/
			virtual void createDisplayList() = 0;
		public:
			/*! Default constructor.*/
			virtual void renderGL() = 0;
			/*! Used for rendering. Calls the OpenGL display list representing this shape. May also do additional transformations.*/
			virtual void callMyList() = 0;
			virtual Entity* toEntity() = 0;

			virtual GLEntity* queryId(unsigned int id) = 0;
		};
	}
}