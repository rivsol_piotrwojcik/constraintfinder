#pragma once
#include <GL/glew.h>
#include <GL/glut.h>
#include <opencsg.h>
#include "Camera.h"
#include "GLEllipsoid.h"
#include "GLCuboid.h"
#include "GLCylinder.h"
#include "GLPart.h"
#include "IdMaster.h"
#include "InputProcessor.h"
#include "DataFileProcessor.h"

using namespace std;
using namespace OpenCSG;
using namespace ConstraintFinder::Graphics;

namespace ConstraintFinder
{
	namespace UserInterface
	{
		class CFInputProcessor;

		class Engine
		{
		private:
			static Engine* pinstance;
			static GLPart* part;
			static CFInputProcessor* inputProc;
			int scrWidth;
			int scrHeight;
			GLdouble aspect;
		protected:
			static void render();
			static void mouseCallback(int button, int state, int x, int y);
			static void motionCallback(int x, int y);
			static void keyboardCallback(unsigned char key, int x, int y);
			static void idle();
			static void renderLabel();
			Engine();
			Engine(const Engine&);
			Engine& operator=(const Engine&);
		public:
			static unsigned int getNextId();
			static SphericalCamera camera;
			static Engine* instance();
			void initialize(int argc, char** argv, int scrWidth, int scrHeight);
			void mainloop();
			void deinitialize();
			void loadPartFormFile(string path);
			void detectConstraints();
			void listConstraints();
			void listEntities();
			void selectEntity(int id);
			void selectConstraint(int index);
			void clearSelect();
		};

		class CFInputProcessor : public InputProcessor
		{
		private:
			Engine* parent;
		public:
			CFInputProcessor(Engine* parent);
			void mouseDown(int x, int y, unsigned short button);
			void mouseUp(int x, int y, unsigned short button);
			void mouseMoved(int x, int y);
			void executeCommand(string command);
		};
	}
}
