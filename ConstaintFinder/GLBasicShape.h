#pragma once
#include <opencsg.h>
#include <iostream>
#include "GLEntity.h"
using namespace std;
using namespace OpenCSG;

#define GEOMETRY_SLICES 32
#define GEOMETRY_STACKS 32

namespace ConstraintFinder
{
	namespace Graphics
	{
		class GLBasicShape : public GLEntity, public Primitive
		{
		public:
			GLBasicShape();
			void render();
			void renderGL() = 0;
		};

		class UnitShapes
		{
		public:
			static unsigned int unitCube;
			static unsigned int unitSphere;
			static unsigned int unitCylinder;

			static unsigned int initUnitCube();
			static unsigned int initUnitSphere();
			static unsigned int initUnitCylinder();
		};
	}
}