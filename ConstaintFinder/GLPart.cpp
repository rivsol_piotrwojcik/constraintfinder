#include "GLPart.h"

namespace ConstraintFinder
{
	namespace Graphics
	{

		GLPart::GLPart(unsigned int id)
			:GLEntityGroup(id)
		{
			renderSelected = false;
		}

		void GLPart::renderGL()
		{
			if (!renderSelected)
			{
				GLEntityGroup::renderGL();
			}
			else
			{
				for (vector<GLEntity*>::iterator it = selected.begin(); it != selected.end(); ++it)
					(*it)->renderGL();
			}
		}

		void GLPart::detectConstraints()
		{
			this->constraints = ConstraintProcessor::detectConstraints(this);
			this->listConstraints();
		}

		void GLPart::listConstraints()
		{
			cout<<"----Constraints for current part----\n";
			if (constraints.size() == 0)
				cout<<"None\n";
			for (int i=0; i<(int)constraints.size(); ++i)
			{
				cout<<i<<". "<<constraints[i]->toString()<<"\n";
			}
			cout<<"------------------------------------\n";
		}

		void GLPart::listEntities()
		{
			cout<<"----Entities in current part----\n";
			for (vector<Entity*>::iterator it = entities.begin(); it != entities.end(); ++it)
			{
				cout<<(*it)->toString()<<"\n";
			}
			cout<<"--------------------------------\n";
		}

		void GLPart::selectEntity(unsigned int id)
		{
			if (id == -1)
			{
				renderSelected = false;
				return;
			}
			if (id == this->id)
			{
				cout<<"Error, self redundancy!\n";
				return;
			}
			GLEntity* e = queryId(id);
			if (!e)
			{
				cout<<"Entity not found. Canceled.\n";
				return;
			}
			selected.clear();
			selected.push_back(e);
			renderSelected = true;
		}

		void GLPart::selectConstraint(int index)
		{
			if (index == -1)
			{
				renderSelected = false;
			}
			if (index < 0 || index >= (int)constraints.size())
			{
				cout<<"Index out of bounds. Canceled.\n";
			}
			if (constraints[index]->getClass() == CC_SINGLE)
			{
				selected.clear();
				selected.push_back(queryId(constraints[index]->getSubjectIds()[0]));
				renderSelected = true;
			}
			else
			{
				selected.clear();
				for (vector<unsigned int>::iterator i = constraints[index]->getSubjectIds().begin();
					i != constraints[index]->getSubjectIds().end(); ++i)
					selected.push_back(queryId(*i));
				renderSelected = true;
			}
		}

		void GLPart::clearSelect()
		{
			renderSelected = false;
		}
	}
}