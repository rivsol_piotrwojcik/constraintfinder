#pragma once
#include "EntityGroup.h"
#include "CsgNode.h"
#include "ToleranceSet.h"
#include "IdMaster.h"
#include <string>
using namespace std;

namespace ConstraintFinder
{
	namespace Core
	{
		/*! Enumerates te possible classes of Constraints. SINGLE constraints not treated as groups of entities,
		* while MULTI constraints are trated so.
		* \sa Constraint
		*/
		typedef enum ConstraintClass { CC_SINGLE, CC_MULTI };
		/*! \brief Represents a geometric constraint.
		* 
		* This class represents a geometric constraint.
		*/
		class Constraint
		{
		public:
			/*! Gets the constraint type
			* \return the constraint type as a string. All constraint type being with the prefix CON_
			*/
			virtual string getType() = 0;
			/*! Gets the class of this constraint
			* \return the class of this constraint
			*/
			virtual ConstraintClass getClass() = 0;
			/*! Eeturns an information string about this constraint
			* \return the information string
			*/
			virtual string toString() = 0;
			/*! Detects all instances of this constraint withtin a given Entity.
			* \param e the entity in which to search for this constraint
			* \param ts the tolerace set to be used durig the search.
			* \param if set to true, the algorithm should not descend into CSG trees, unless parameter e is itself the root of a CSG tree
			*/
			virtual vector<Constraint*> findInTree(Entity* e, ToleranceSet* ts, SearchPattern sp) = 0;

			/*! Gets the Entities subject to this constraint
			* \return the entities subject to this constraint
			*/
			virtual vector<Entity*> getSubjects() = 0;

			/*! Gets the ids of the Entities subject to this constraint
			* \return the ids of the entities subject to this constraint
			*/
			vector<unsigned int> getSubjectIds();

			unsigned int getNextId() {return IdMaster::getNextId();};
		};
	}
}