#include "Parallel.h"

namespace ConstraintFinder
{
	namespace Core
	{
		namespace Constraints
		{
			Parallel::Parallel()
				:MultiConstraint(-1)
			{
			}

			Parallel::Parallel(unsigned int id, Entity *e1, Entity *e2)
				:MultiConstraint(id)
			{
				this->entities.push_back(e1);
				this->entities.push_back(e2);
			}

			string Parallel::toString()
			{
				string s = "[Parallel] for entities: ";
				s += entities[0]->getType();
				s += ", ";
				s += entities[1]->getType();
				s += " with ids: ";
				char* buf = new char[32];
				_itoa_s(entities[0]->getId(), buf, 32, 10);
				s += string(buf);
				s += ", ";
				_itoa_s(entities[1]->getId(), buf, 32, 10);
				s += string(buf);
				s += "; Group  id: ";
				_itoa_s(getId(), buf, 32, 10);
				s += string(buf);
				delete[] buf;
				return s;
			}

			bool checkParallelForAxis(const Vector3& a11, const Vector3& a21,
				const Vector3& a12,
				const Vector3& a13,
				const Vector3& a22,
				const Vector3& a23,
				double vDev)
			{
				return Vector3::Equals(a11, a21, vDev) 
					&& !Vector3::Equals(a12, a22, vDev) && !Vector3::Equals(a12, a23, vDev);
			}

			bool areParallel(Entity* e1, Entity* e2, double vDev)
			{
				Frame f1, f2;
				f1 = e1->getLocalFrame();
				f2 = e2->getLocalFrame();
				return checkParallelForAxis(f1.x, f2.x, f1.y, f1.z, f2.y, f2.z, vDev) ||
					checkParallelForAxis(f1.x, f2.y, f1.y, f1.z, f2.x, f2.z, vDev) ||
					checkParallelForAxis(f1.x, f2.z, f1.y, f1.z, f2.x, f2.y, vDev) ||
					checkParallelForAxis(f1.y, f2.x, f1.x, f1.z, f2.y, f2.z, vDev) ||
					checkParallelForAxis(f1.y, f2.y, f1.x, f1.z, f2.x, f2.z, vDev) ||
					checkParallelForAxis(f1.y, f2.z, f1.x, f1.z, f2.x, f2.y, vDev) ||
					checkParallelForAxis(f1.z, f2.x, f1.x, f1.y, f2.y, f2.z, vDev) ||
					checkParallelForAxis(f1.z, f2.y, f1.x, f1.y, f2.x, f2.z, vDev) ||
					checkParallelForAxis(f1.z, f2.z, f1.x, f1.y, f2.x, f2.y, vDev);
			}
			
			vector<Constraint*> Parallel::findInTree(Entity* root, ToleranceSet* ts, SearchPattern sp)
			{
				vector<Constraint*> result;
				vector<Entity*> entityList;
				string rootType = root->getType();

				entityList = root->toEntityList(sp);
				for(vector<Entity*>::iterator it = entityList.begin(); it != entityList.end(); ++it)
				{
					for(vector<Entity*>::iterator subit = entityList.begin(); subit != entityList.end(); ++subit)
					{
						if (*it != *subit && areParallel(*it, *subit, ts->vector3DDeviation)) 
							result.push_back(new Parallel(getNextId(), *it, *subit));
					}
				}
				return result;
			}
		}
	}
}