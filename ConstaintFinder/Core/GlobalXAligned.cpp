#include "GlobalXAligned.h"

namespace ConstraintFinder
{
	namespace Core
	{
		namespace Constraints
		{
			GlobalXAligned::GlobalXAligned()
				:SingleConstraint(NULL)
			{
			}

			GlobalXAligned::GlobalXAligned(Entity* e)
				:SingleConstraint(e)
			{
			}

			bool isAlignedX(Frame* f, ToleranceSet* ts)
			{
				if (Vector3::Equals(f->x, Vector3::unitX(), ts->vector3DDeviation) ||
					Vector3::Equals(f->y, Vector3::unitX(), ts->vector3DDeviation) ||
					Vector3::Equals(f->z, Vector3::unitX(), ts->vector3DDeviation))
					return true;
				return false;
			}

			string GlobalXAligned::toString()
			{
				string s = "[Aligned along the X axis] for entity: ";
				s += e->getType();
				s += " with id: ";
				char* buf = new char[32];
				_itoa_s(e->getId(), buf, 32, 10);
				s += string(buf);
				delete[] buf;
				return s;
			}

			vector<Constraint*> GlobalXAligned::findInTree(Entity* root, ToleranceSet* ts, SearchPattern sp)
			{
				vector<Constraint*> result;
				vector<Entity*> entityList;
				entityList = root->toEntityList(sp);
				for(vector<Entity*>::iterator it = entityList.begin(); it != entityList.end(); ++it)
				{
					if (isAlignedX(&(*it)->getLocalFrame(), ts))
						result.push_back(new GlobalXAligned(*it));
				}
				return result;
			}
		}
	}
}