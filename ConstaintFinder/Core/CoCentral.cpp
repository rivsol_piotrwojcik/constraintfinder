#include "CoCentral.h"

namespace ConstraintFinder
{
	namespace Core
	{
		namespace Constraints
		{
			CoCentral::CoCentral()
				:MultiConstraint(-1)
			{
			}

			CoCentral::CoCentral(unsigned int id, Entity *e1, Entity *e2)
				:MultiConstraint(id)
			{
				this->entities.push_back(e1);
				this->entities.push_back(e2);
			}

			string CoCentral::toString()
			{
				string s = "[Cocentral] for entities: ";
				s += entities[0]->getType();
				s += ", ";
				s += entities[1]->getType();
				s += " with ids: ";
				char* buf = new char[32];
				_itoa_s(entities[0]->getId(), buf, 32, 10);
				s += string(buf);
				s += ", ";
				_itoa_s(entities[1]->getId(), buf, 32, 10);
				s += string(buf);
				s += "; Group  id: ";
				_itoa_s(getId(), buf, 32, 10);
				s += string(buf);
				delete[] buf;
				return s;
			}

			bool areCoCentral(Entity* e1, Entity* e2, double zdDev)
			{
				return (e2->getCenter() - e1->getCenter()).length() <= zdDev;
			}
			
			vector<Constraint*> CoCentral::findInTree(Entity* root, ToleranceSet* ts, SearchPattern sp)
			{
				vector<Constraint*> result;
				vector<Entity*> entityList;
				string rootType = root->getType();

				entityList = root->toEntityList(sp);
				for(vector<Entity*>::iterator it = entityList.begin(); it != entityList.end(); ++it)
				{
					for(vector<Entity*>::iterator subit = entityList.begin(); subit != entityList.end(); ++subit)
					{
						if (*it != *subit && areCoCentral(*it, *subit, ts->zeroDistanceDeviation))
							result.push_back(new CoCentral(getNextId(), *it, *subit));
					}
				}
				return result;
			}
		}
	}
}