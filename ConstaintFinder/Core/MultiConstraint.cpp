#include "MultiConstraint.h"

namespace ConstraintFinder
{
	namespace Core
	{
		MultiConstraint::MultiConstraint(unsigned int id)
			:EntityGroup(id)
		{
		}

		MultiConstraint::MultiConstraint(unsigned int id, vector<Entity*> entities)
			:EntityGroup(id, entities)
		{
		}

		vector<Entity*> MultiConstraint::getSubjects()
		{
			return this->getEntities();
		}
	}
}