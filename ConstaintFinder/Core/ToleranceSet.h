#pragma once

namespace ConstraintFinder
{
	namespace Core
	{
		/*! \brief Represents a set of tolerances used in cosntraint detection.
		*
		* The values of various tolerances applied while detecting geometric constraints in CSG gemoetry are contained in this structure.
		* \sa Contraint, ConstraintProcessor
		*/
		struct ToleranceSet
		{
			/*! Represents the radius of the cylinder around vector A, into which vector B should fit to satisty this tolerance.*/
			double vector3DDeviation;
			/*! Represents the maximal number, by which lengths A and B can differ to satisfy the tolerance.*/
			double lengthDeviation;
			/*! Represents the maximal number (i radians), by which angles A and B can differ to satisfy the tolerance.*/
			double angularDeviation;
			/*! Represents the maximal number by which two almost otrhogonal vectors can deviate from orthogonality.*/
			double orthogonalityDeviation;
			/*! Represents the maximal distance between two points for them to be considered equal.*/
			double zeroDistanceDeviation;
			/*! Default constructor.*/
			ToleranceSet();
			/*! Returns a defualt tolerance set.
			* \return a defualt tolerance set.
			*/
			static ToleranceSet* defaultToleranceSet();
		};
	}
}