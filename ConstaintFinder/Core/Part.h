#pragma once
#include "EntityGroup.h"
#include "CsgNode.h"
#include "ConstraintProcessor.h"
//#include "Constraint.h"

namespace ConstraintFinder
{
	namespace Core
	{
		/*! \brief Represents a single part in the constraint finder program.
		* 
		* A single part may consisit of any number of entities (Eeither base shapes, csg trees or entity groups).
		* The Part class is a derivative of EntityGroup, with the list of entities serving as the main entity pool storing all entities.
		* \sa Entity, EntityGroup, CsgProcessor, Constraint
		*/
		class Part : public EntityGroup
		{
		protected:
			/*! Constraints existing in this part.*/
			vector<Constraint*> constraints;
			void computeLocalFrame();
		public:
			/*! Laods a CSG tree as an element of this part.
			* \param root the root node of the CSG tree.
			*/
			virtual void loadCsgTree(CsgNode* root);
			/*! Loads a basic shape as an element of this part.
			* \param bs the basic shape.
			*/
			virtual void loadBaseShape(Entity* bs);
			/*! Loads an entity group as an element of this part.
			* \param g the entity group to load.
			* \param parentGroup the EntityGroup in this part this group blengs to; null if there is no parent group
			*/
			virtual void loadEntityGroup(EntityGroup* g, EntityGroup* parentGroup = NULL);
			/*! Default constructor.
			* \param id the unique id assigend to each entity.
			*/
			Part(unsigned int id)
				:EntityGroup(id)
			{
			}
			/*! Constructor.
			* \param id the unique id assigend to each entity.
			* \param objects The array of entities constituting the part.
			* \param obj_count The size of objects array.
			*/
			Part(unsigned int id, Entity** objects, unsigned int obj_conut);
			/*! Constructor.
			* \param id the unique id assigend to each entity.
			* \param objects The vector of entities constituting the part.
			*/
			Part(unsigned int id, vector<Entity*> objects);
			/*! Destructor.*/
			~Part();
			string getType() { return "ET_PART"; };

			/*! Detect the geometrical constraints within this part.
			* Detection parameters are passed as static fields of the ConstraintProcessor class.
			*/
			virtual void detectConstraints();
		};
	}
}