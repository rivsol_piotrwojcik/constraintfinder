#include "Entity.h"

namespace ConstraintFinder
{
	namespace Core
	{
		/*! \biref A local axis aligned cuboid.
		* 
		* Represents a cylinder with given radius and given length along its local eZ axis.
		* \sa Entity
		*/
		class Cylinder : public Entity
		{
		protected:
			/*! Radius of the cylider.*/
			double radius;
			/*! Length of the cylider.*/
			double length;
		public:
			/*! Constructor. 
			* \param id a unique id assigned to each entity.
			* \param radius radius of the base of the clinder.
			* \param length length of the cluinder along the local z axis
			*/
			Cylinder(unsigned int id, double radius, double length);
			/*! Gets the radius of this cylinder
			* \return the radius of the clinder
			*/
			double getRadius();
			/*! Gets the length of the clinder
			* \return length of the clinder
			*/
			double getLength();
			string getType() {return "ET_CYLINDER";};

			vector<Entity*> toEntityList(SearchPattern sp);
		};
	}
}