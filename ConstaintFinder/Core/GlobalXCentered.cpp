#include "GlobalXCentered.h"

namespace ConstraintFinder
{
	namespace Core
	{
		namespace Constraints
		{
			GlobalXCentered::GlobalXCentered()
				:SingleConstraint(NULL)
			{
			}

			GlobalXCentered::GlobalXCentered(Entity* e)
				:SingleConstraint(e)
			{
			}

			string GlobalXCentered::toString()
			{
				string s = "[Centered on the X axis] for entity: ";
				s += e->getType();
				s += " with id: ";
				char* buf = new char[32];
				_itoa_s(e->getId(), buf, 32, 10);
				s += string(buf);
				delete[] buf;
				return s;
			}

			bool isCenteredX(Vector3& c, double zdDev)
			{
				return abs(c.getX()) <= zdDev;
			}

			vector<Constraint*> GlobalXCentered::findInTree(Entity* root, ToleranceSet* ts, SearchPattern sp)
			{
				vector<Constraint*> result;
				vector<Entity*> entityList;
				entityList = root->toEntityList(sp);
				for(vector<Entity*>::iterator it = entityList.begin(); it != entityList.end(); ++it)
				{
					if (isCenteredX((*it)->getCenter(), ts->zeroDistanceDeviation))
						result.push_back(new GlobalXCentered(*it));
				}
				return result;
			}
		}
	}
}