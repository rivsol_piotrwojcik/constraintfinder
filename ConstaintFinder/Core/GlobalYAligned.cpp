#include "GlobalYAligned.h"

namespace ConstraintFinder
{
	namespace Core
	{
		namespace Constraints
		{
			GlobalYAligned::GlobalYAligned()
				:SingleConstraint(NULL)
			{
			}

			GlobalYAligned::GlobalYAligned(Entity* e)
				:SingleConstraint(e)
			{
			}

			bool isAlignedY(Frame* f, ToleranceSet* ts)
			{
				if (Vector3::Equals(f->x, Vector3::unitY(), ts->vector3DDeviation) ||
					Vector3::Equals(f->y, Vector3::unitY(), ts->vector3DDeviation) ||
					Vector3::Equals(f->z, Vector3::unitY(), ts->vector3DDeviation))
					return true;
				return false;
			}

			string GlobalYAligned::toString()
			{
				string s = "[Aligned along the Y axis] for entity: ";
				s += e->getType();
				s += " with id: ";
				char* buf = new char[32];
				_itoa_s(e->getId(), buf, 32, 10);
				s +=  string(buf);
				delete[] buf;
				return s;
			}

			vector<Constraint*> GlobalYAligned::findInTree(Entity* root, ToleranceSet* ts, SearchPattern sp)
			{
				vector<Constraint*> result;
				vector<Entity*> entityList;
				entityList = root->toEntityList(sp);
				for(vector<Entity*>::iterator it = entityList.begin(); it != entityList.end(); ++it)
				{
					if (isAlignedY(&(*it)->getLocalFrame(), ts))
						result.push_back(new GlobalYAligned(*it));
				}
				return result;
			}
		}
	}
}