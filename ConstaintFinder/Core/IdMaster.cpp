#include "IdMaster.h"

namespace ConstraintFinder
{
	namespace Core
	{
		unsigned int IdMaster::entIdCnt = 0;

		void IdMaster::initializeIdMaster()
		{
			entIdCnt = 0;
		}

		unsigned int IdMaster::getNextId()
		{
			return entIdCnt++;
		}
	}
}