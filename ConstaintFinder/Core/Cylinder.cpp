#include "Cylinder.h"

namespace ConstraintFinder
{
	namespace Core
	{
		Cylinder::Cylinder(unsigned int id, double radius, double length)
			:Entity(id)
		{
			this->radius = radius;
			this->length = length;
			computeLocalFrame();
		}

		double Cylinder::getLength() 
		{
			return length;
		}

		double Cylinder::getRadius()
		{
			return radius;
		}

		vector<Entity*> Cylinder::toEntityList(SearchPattern sp)
		{
			vector<Entity*> result;
			result.push_back(this);
			return result;
		}
	}
}