#pragma once

namespace ConstraintFinder
{
	namespace Core
	{
		class IdMaster
		{
		private:
			static unsigned int entIdCnt;
		public:
			static void initializeIdMaster();
			static unsigned int getNextId();
		};
	}
}