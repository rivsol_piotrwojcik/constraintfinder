#include "Cuboid.h"

namespace ConstraintFinder
{
	namespace Core
	{
		Cuboid::Cuboid(unsigned int id, double eX, double eY, double eZ)
			:Entity(id)
		{
			this->eX = eX;
			this->eY = eY;
			this->eZ = eZ;
			computeLocalFrame();
		}

		double Cuboid::getEX() { return this->eX; }
		double Cuboid::getEY() { return this->eY; }
		double Cuboid::getEZ() { return this->eZ; }

		vector<Entity*> Cuboid::toEntityList(SearchPattern sp)
		{
			vector<Entity*> result;
			result.push_back(this);
			return result;
		}
	}
}