#include "SingleConstraint.h"

namespace ConstraintFinder
{
	namespace Core
	{
		namespace Constraints
		{
			class GlobalZCentered : public SingleConstraint
			{
			public:
				/*!Default constructor*/
				GlobalZCentered();
				/*!Detailed constructor
				* \param e the entitiy subject to this constraint
				*/
				GlobalZCentered(Entity* e);
				string getType() { return "CON_GLOBALZCENTERED"; };
				vector<Constraint*> findInTree(Entity* root, ToleranceSet* ts, SearchPattern sp);
				string toString();
			};
		}
	}
}