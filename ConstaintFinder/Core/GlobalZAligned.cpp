#include "GlobalZAligned.h"

namespace ConstraintFinder
{
	namespace Core
	{
		namespace Constraints
		{
			GlobalZAligned::GlobalZAligned()
				:SingleConstraint(NULL)
			{
			}

			GlobalZAligned::GlobalZAligned(Entity* e)
				:SingleConstraint(e)
			{
			}

			bool isAlignedZ(Frame* f, ToleranceSet* ts)
			{
				if (Vector3::Equals(f->x, Vector3::unitZ(), ts->vector3DDeviation) ||
					Vector3::Equals(f->y, Vector3::unitZ(), ts->vector3DDeviation) ||
					Vector3::Equals(f->z, Vector3::unitZ(), ts->vector3DDeviation))
					return true;
				return false;
			}

			string GlobalZAligned::toString()
			{
				string s = "[Aligned along the Z axis] for entity: ";
				s += e->getType();
				s += " with id: ";
				char* buf = new char[32];
				_itoa_s(e->getId(), buf, 32, 10);
				s +=  string(buf);
				delete[] buf;
				return s;
			}

			vector<Constraint*> GlobalZAligned::findInTree(Entity* root, ToleranceSet* ts, SearchPattern sp)
			{
				vector<Constraint*> result;
				vector<Entity*> entityList;
				entityList = root->toEntityList(sp);
				for(vector<Entity*>::iterator it = entityList.begin(); it != entityList.end(); ++it)
				{
					if (isAlignedZ(&(*it)->getLocalFrame(), ts))
						result.push_back(new GlobalZAligned(*it));
				}
				return result;
			}
		}
	}
}