#include "CsgNode.h"

namespace ConstraintFinder
{
	namespace Core
	{
		CsgNode::CsgNode(unsigned int id)
			:Entity(id)
		{
			left = NULL;
			right = NULL;
			op = CSG_None;
		}

		CsgNode::CsgNode(unsigned int id, Entity *left, Entity *right, CSG_Operation op)
			:Entity(id)
		{
			this->left = left;
			this->right = right;
			this->op = op;
			this->computeLocalFrame();
		}

		CsgNode::CsgNode(unsigned int id, Entity *left, Entity *right, CSG_Operation op, const Vector3 &center, const Frame &localframe)
			:Entity(id)
		{
			this->left = left;
			this->right = right;
			this->op = op;
			this->center = center;
			this->localFrame = localFrame;
		}

		CsgNode::~CsgNode()
		{
			if (left != NULL)
				delete left;
			if (right != NULL)
				delete right;
		}

		Entity* CsgNode::getLeftChild()
		{
			return left;
		}

		Entity* CsgNode::getRightChild()
		{
			return right;
		}

		CSG_Operation CsgNode::getOperation()
		{
			return op;
		}

		void CsgNode::computeLocalFrame()
		{
			this->center = (left->getCenter() + right->getCenter())/2.0 + this->translation;
			this->localFrame = Frame::mean(left->getLocalFrame(), right->getLocalFrame());
			if (!Frame::isOrthogonal(this->localFrame, CSGNODE_ORTHOGONAL_TOLERANCE))
			{
				cout<<"CsgNode::computeLocalFrame -> (!)Warning, frame is not orthogonal:\n";
				cout<<"Frame: "<<this->localFrame<<"\n";
				cout<<"Dot products: xy="<<Vector3::Dot(this->localFrame.x, this->localFrame.y)<<", yz="<<
					Vector3::Dot(this->localFrame.y, this->localFrame.z)<<", xz="<<
					Vector3::Dot(this->localFrame.x, this->localFrame.z)<<"\n";
			}
			this->localFrame.x = this->rotation * this->localFrame.x;
			this->localFrame.y = this->rotation * this->localFrame.y;
			this->localFrame.z = this->rotation * this->localFrame.z;
		}

		string CsgNode::toString()
		{
			string s = Entity::toString();
			s += "\n\t";
			s += left->toString();
			s += "\n\t";
			s += right->toString();
			return s;
		}

		vector<Entity*> CsgNode::toEntityList(SearchPattern sp)
		{
			vector<Entity*> result;
			result.push_back(this);
			if (sp == SP_BRUTEFORCE)
			{
				vector<Entity*> l = this->left->toEntityList(sp);
				result.insert(result.end(), l.begin(), l.end());
				vector<Entity*> r = this->right->toEntityList(sp);
				result.insert(result.end(), r.begin(), r.end());
			}
			return result;
		}
	}
}