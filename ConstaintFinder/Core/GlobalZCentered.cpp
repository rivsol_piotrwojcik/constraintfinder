#include "GlobalZCentered.h"

namespace ConstraintFinder
{
	namespace Core
	{
		namespace Constraints
		{
			GlobalZCentered::GlobalZCentered()
				:SingleConstraint(NULL)
			{
			}

			GlobalZCentered::GlobalZCentered(Entity* e)
				:SingleConstraint(e)
			{
			}

			string GlobalZCentered::toString()
			{
				string s = "[Centered on the Z axis] for entity: ";
				s += e->getType();
				s += " with id: ";
				char* buf = new char[32];
				_itoa_s(e->getId(), buf, 32, 10);
				s += string(buf);
				delete[] buf;
				return s;
			}

			bool isCenteredZ(Vector3& c, double zdDev)
			{
				return abs(c.getZ()) <= zdDev;
			}

			vector<Constraint*> GlobalZCentered::findInTree(Entity* root, ToleranceSet* ts, SearchPattern sp)
			{
				vector<Constraint*> result;
				vector<Entity*> entityList;
				entityList = root->toEntityList(sp);
				for(vector<Entity*>::iterator it = entityList.begin(); it != entityList.end(); ++it)
				{
					if (isCenteredZ((*it)->getCenter(), ts->zeroDistanceDeviation))
						result.push_back(new GlobalZCentered(*it));
				}
				return result;
			}
		}
	}
}