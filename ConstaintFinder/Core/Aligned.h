#include "MultiConstraint.h"

namespace ConstraintFinder
{
	namespace Core
	{
		namespace Constraints
		{
			class Aligned : public MultiConstraint
			{
			public:
				Aligned();
				Aligned(unsigned int id, vector<Entity*> ents);
				string getType() { return "CON_ALIGNED"; };
				vector<Constraint*> findInTree(Entity* root, ToleranceSet* ts, SearchPattern sp);
				string toString();
			};
		}
	}
}