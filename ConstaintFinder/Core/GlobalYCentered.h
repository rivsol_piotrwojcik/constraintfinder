#include "SingleConstraint.h"

namespace ConstraintFinder
{
	namespace Core
	{
		namespace Constraints
		{
			class GlobalYCentered : public SingleConstraint
			{
			public:
				/*!Default constructor*/
				GlobalYCentered();
				/*!Detailed constructor
				* \param e the entitiy subject to this constraint
				*/
				GlobalYCentered(Entity* e);
				string getType() { return "CON_GLOBALYCENTERED"; };
				vector<Constraint*> findInTree(Entity* root, ToleranceSet* ts, SearchPattern sp);
				string toString();
			};
		}
	}
}