#include "CoAxial.h"

namespace ConstraintFinder
{
	namespace Core
	{
		namespace Constraints
		{
			CoAxial::CoAxial()
				:MultiConstraint(-1)
			{
			}

			CoAxial::CoAxial(unsigned int id, Entity *e1, Entity *e2)
				:MultiConstraint(id)
			{
				entities.push_back(e1);
				entities.push_back(e2);
			}

			string CoAxial::toString()
			{
				string s = "[Coaxial] for entities: ";
				s += entities[0]->getType();
				s += ", ";
				s += entities[1]->getType();
				s += " with ids: ";
				char* buf = new char[32];
				_itoa_s(entities[0]->getId(), buf, 32, 10);
				s += string(buf);
				s += ", ";
				_itoa_s(entities[1]->getId(), buf, 32, 10);
				s += string(buf);
				s += "; Group  id: ";
				_itoa_s(getId(), buf, 32, 10);
				s += string(buf);
				delete[] buf;
				return s;
			}

			bool areCoaxial(const Frame& f1, const Frame& f2, double vDev)
			{
				return ((Vector3::Equals(f1.x, f2.x, vDev) || Vector3::Equals(f1.x, f2.y, vDev) || Vector3::Equals(f1.x, f2.z, vDev)) 
					&& (Vector3::Equals(f1.y, f2.x, vDev) || Vector3::Equals(f1.y, f2.y, vDev) || Vector3::Equals(f1.y, f2.z, vDev)));
			}

			vector<Constraint*> CoAxial::findInTree(Entity* root, ToleranceSet* ts, SearchPattern sp)
			{
				vector<Constraint*> result;
				vector<Entity*> entityList;
				string rootType = root->getType();

				entityList = root->toEntityList(sp);
				for(vector<Entity*>::iterator it = entityList.begin(); it != entityList.end(); ++it)
				{
					for(vector<Entity*>::iterator subit = entityList.begin(); subit != entityList.end(); ++subit)
						if (*it != *subit && areCoaxial((*it)->getLocalFrame(), (*subit)->getLocalFrame(), ts->vector3DDeviation))
							result.push_back(new CoAxial(getNextId(), *it, *subit));
				}
				return result;
			}
		}
	}
}