#pragma once
#include "Vector.h"
#include "Matrix.h"
#include "IdMaster.h"
#include <string>
#include <vector>
#include <iostream>
using namespace std;

#define DUMMYGROUPID -100

/*! The main namespace of the constraint finder program.*/
namespace ConstraintFinder
{
	/*! This namespace contains the core elements of the constraint finder program. */
	namespace Core
	{
		/*!Defines the search pattern of the constraint detection algorithm.*/
		typedef enum SearchPattern {SP_BRUTEFORCE, SP_DETACHCSG, SP_PYRAMID};

		/*! 
		* \brief The basic entity processed by the constraint finder program.
		* 
		* The entity can be either a single object or a group of objects that have teir local coordinate frame assigned,
		* and can be freely transformed in space. This class is abstract and should be extended by implementations of entities.
		* \sa BaseShape, CsgNode, Part, EntityGroup, Constraint
		*/
		class Entity
		{
		protected:
			/*! A unique id assigned to each entity */
			unsigned int id;
			
			/*! A local translation vector for this entity.*/
			Vector3 translation;
			/*! A local rotation matrix for this entity.*/
			Matrix33 rotation;
			/*! A local scaling vector for this entity. The coordinates represent scaling along local x, y and z axes.*/
			Vector3 scaling;

			/*! The local center of this entity.*/
			Vector3 center;
			/*! The local coordinate frame of this entity.*/
			Frame localFrame;

			/*! Computes the center and local cordinate axes for this entity.*/
			virtual void computeLocalFrame();

		public:
			/*! Default constructor
			* \param id a unique identifier assigned to all entities.
			*/
			Entity(unsigned int id);

			/*! Constructor including geometrical transform
			* \param id a unique identifier assigned to all entities.
			* \param translation the translation vector
			* \param rotation the rotation matrix
			* \param scaling the scaling vector
			*/
			Entity(unsigned int id, const Vector3& translation, const Matrix33& rotation, const Vector3& scaling);

			/*! Destructor.*/
			~Entity();
			
			/*! get the id of this entity.
			* \return the unique id assigend to this entity.
			*/
			unsigned int getId();
			/*! returns the type of this entity.
			* \return the type indicator of this entity.
			*/
			virtual string getType() = 0;

			/*! gets the local center of this entity.
			* \return the local center of this entity.
			*/
			Vector3 getCenter();
			/*! gets the local coordiante frame of this entity.
			* \return the local coordinate axes of this entity.
			*/
			Frame getLocalFrame();

			/*! translates this entity in the in global space by vector [tX, tY, tZ].
			* \param tX translation along the global X axis.
			* \param tY translation along the global Y axis.
			* \param tZ translation along the global Z axis.
			*/
			virtual void translate(double tX, double tY, double tZ);
			/*! rotates the entity in its local space around the X axis by angle rX.
			* \param rX rotation around local X axis in radians.
			*/
			virtual void rotateX(double rX);
			/*! rotates the entity in its local space around the Y axis by angle rY.
			* \param rY rotation around local Y axis in radians.
			*/
			virtual void rotateY(double rY);
			/*! rotates the entity in its local space around the Z axis by angle rZ.
			* \param rZ rotation around local Z axis in radians.
			*/
			virtual void rotateZ(double rZ);
			/*! rotates the entity around the given axis by the given angle.
			* \param axis the axis of rotation.
			* \param angle the angle of rotation in radians.
			*/
			virtual void rotateAxis(const Vector3& axis, double angle);
			/*! Scales the entity in its local coordinate frame.
			* \param sX scaling factor along the local X axis.
			* \param sY scaling factor along the local Y axis.
			* \param sZ scaling factor along the local Z axis.
			*/
			virtual void scale(double sX, double sY, double sZ);

			/*!Gets the translation for this object
			* \return the translation vector
			*/
			Vector3 getTranslation();

			/*!Gets the rotation for this object
			* \return the rotation matrix
			*/
			Matrix33 getRotation();

			/*!Gets the scaling of this object
			* \return the scaling vector
			*/
			Vector3 getScaling();

			/*! Gets info on this entity as string.
			* \return info on this entity
			*/
			virtual string toString();

			/*! Returns a vector of all Entities in the hierarchy rooted at this entity.
			* \param sp ...
			* return vector of all Entities in this Entity's hierarchy.
			*/
			virtual vector<Entity*> toEntityList(SearchPattern sp) = 0;
		};
	}
}