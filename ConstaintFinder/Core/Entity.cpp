#include "Entity.h"

namespace ConstraintFinder
{
	namespace Core
	{
		Entity::Entity(unsigned int id)
			:translation(), rotation(), scaling(1,1,1), center()
		{
			this->id = id;
			this->rotation = Matrix33::identity();
			this->localFrame = Frame::orthonormal();
		}

		Entity::Entity(unsigned int id, const Vector3& translation, const Matrix33& rotation, const Vector3& scaling)
			:translation(), rotation(), scaling(1,1,1), center()
		{
			this->id = id;
			this->translation = translation;
			this->rotation = rotation;
			this->scaling  = scaling;
		}

		Entity::~Entity()
		{
		}

		unsigned int Entity::getId()
		{
			return this->id;
		}

		Vector3 Entity::getCenter()
		{
			return center;
		}

		Frame  Entity::getLocalFrame()
		{
			return localFrame;
		}

		void Entity::translate(double tX, double tY, double tZ)
		{
			this->translation += Vector3(tX, tY, tZ);
			computeLocalFrame();
		}

		void Entity::rotateX(double rX)
		{
			rotation *= Matrix33::rotationX(rX);
			computeLocalFrame();
		}

		void  Entity::rotateY(double rY)
		{
			rotation *= Matrix33::rotationY(rY);
			computeLocalFrame();
		}

		void Entity::rotateZ(double rZ)
		{
			rotation *= Matrix33::rotationZ(rZ);
			computeLocalFrame();
		}

		void Entity::rotateAxis(const Vector3& axis, double angle)
		{
			//TODO
		}

		void Entity::scale(double sX, double sY, double sZ)
		{
			this->scaling.setX( this->scaling.getX()*sX);
			this->scaling.setY( this->scaling.getY()*sY);
			this->scaling.setZ( this->scaling.getZ()*sZ);
		}

		void Entity::computeLocalFrame()
		{
			this->center = Vector3::zero() + this->translation;
			this->localFrame = Frame::orthonormal();
			this->localFrame.x = this->rotation * this->localFrame.x;
			this->localFrame.y = this->rotation * this->localFrame.y;
			this->localFrame.z = this->rotation * this->localFrame.z;
		}

		Vector3 Entity::getTranslation()
		{
			return translation;
		}

		Matrix33 Entity::getRotation()
		{
			return rotation;
		}

		Vector3 Entity::getScaling()
		{
			return scaling;
		}

		string Entity::toString()
		{
			string s="-> id: ";
			char* buf = new char[32];
			_itoa_s(getId(), buf, 32, 10);
			s += string(buf);
			s += "  type: ";
			s += getType();
			return s;
		}
	}
}