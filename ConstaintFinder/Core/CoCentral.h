#include "MultiConstraint.h"

namespace ConstraintFinder
{
	namespace Core
	{
		namespace Constraints
		{
			class CoCentral : public MultiConstraint
			{
			public:
				CoCentral();
				CoCentral(unsigned int id, Entity* e1, Entity* e2);
				string getType() { return "CON_COCENTRAL"; };
				vector<Constraint*> findInTree(Entity* root, ToleranceSet* ts, SearchPattern sp);
				string toString();
			};
		}
	}
}