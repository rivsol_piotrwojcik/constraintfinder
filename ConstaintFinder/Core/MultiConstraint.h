#pragma once
#include "Constraint.h"
#include "EntityGroup.h"

namespace ConstraintFinder
{
	namespace Core
	{
		/*! \brief Represents group constraint.
		* Constraints of this type are treated as entity groups, and are passed along
		* as input to further iterations, form the one in which they are detected.
		* \sa Constraint
		*/
		class MultiConstraint : public Constraint, public EntityGroup
		{
		public:
			/*!Constructor
			* \param id a unique identifier assigned to all entities.
			*/
			MultiConstraint(unsigned int id);
			/*!Constructor
			* \param id a unique identifier assigned to all entities.
			* \param entities the vector of entities subject to this constraint
			*/
			MultiConstraint(unsigned int id, vector<Entity*> entities);
			ConstraintClass getClass() { return CC_MULTI; };
			vector<Entity*> getSubjects();
		};
	}
}