#pragma once
#include <iostream>
using namespace std;

namespace ConstraintFinder
{
	namespace Core
	{
		/*!
		* \brief Represents a 2D vector.
		*
		* Represents a 2-dimensional vector of the form [x,y].
		* \sa Matrix22
		*/
		class Vector2
		{
			friend ostream& operator <<(ostream& os, const Vector2& v);
		protected:
			/*! The x coordiante.*/
			double x;
			/*! The y cooridiante*/
			double y;
		public:
			/*! Default contructor.*/
			Vector2();
			/*! Detailed contructor.
			* \param x the x coordinate of the new vector.
			* \param y the y coordinate of the new vector.
			*/
			Vector2(double x, double y);
			/*! Gets the x coordiante.
			* \return the x coordiante.
			*/
			double getX();
			/*! Gets the y coordinte.
			* \return the y coordinate.
			*/
			double getY();
			/*! Sets the x coordinate.
			* \param the new x coordinate.
			*/
			void setX(double x);
			/*! Sets the y coordinate.
			* \param the new y coordiante.
			*/
			void setY(double y);
			/*! Gets the length of the vector.
			* \return the length of the vector.
			*/
			virtual double length();
			/*! Normalizes the vector, so that the its length is equal to 1.
			*/
			virtual void normalize();
			/*! Calculates the dot (scalar) product of two 2D vectors.
			* \param v1 the left operand.
			* \param v2 the right operand.
			* \return the dot product <v1, v2>.
			*/
			static double Dot(const Vector2& v1, const Vector2& v2);
			/*! Calculates the cross (vector) product of two 2D vectors.
			* \param v1 the left operand.
			* \param v2 the right operand.
			* \return the dot product v1 x v2.
			*/
			static Vector2 Cross(const Vector2& v1, const Vector2& v2);
			/*! returns a sum of this vector and another 2D vector.
			* \param v the vector to add to this vector.
			* \return the sum of the two vectors;
			*/
			Vector2 operator + (const Vector2& v);
			/*! Returns the difference of this vector and another 2D vector.
			* \param v the vector to subtract form this 2D vector.
			* \return the difference of the two vectors.
			*/
			Vector2 operator - (const Vector2& v);
			/*! Returns this vector multiplied by scalar s;
			* \param s the scalar by which this vector is to be multiplied.
			* \return this vector multiplied by scalar s.
			*/
			Vector2 operator * (double s);
			/*! Returns this vector divided by value s;
			* \param s the scalar by which this vector is to be multiplied.
			* \return this vector divided by scalar s.
			*/
			Vector2 operator / (double s);
			/*! Adds a vector to this vector.
			* \param v the vector to add to this vector.
			*/
			void operator += (const Vector2& v);
			/*! Subtracts a vector from this vector.
			* \param v the vector to subtract from this vector.
			*/
			void operator -= (const Vector2& v);
			/*! Multiplies this vector by a scalar.
			* \param s the scalar by which to multiply this vector.
			*/
			void operator *= (double s);
			/*! Divides this vector by a scalar.
			* \param s the scalar by which to divide this vector.
			*/
			void operator /= (double s);
			/*! Compares this vector with another 2D vector.
			* \param v the vector to compare with this vector.
			* \return true if the two vectors are equal, false otherwise.
			*/
			bool operator == (const Vector2& v);
			/*! Compares tow 2D vectors with a given tolerance.
			* \param v1 left operand.
			* \param v2 right operand.
			* \param tolerance the desired tolerance.
			* \return true if the difference between each of the vector's coordiantes is smaller than the tolerance.
			*/
			static bool Equals(const Vector2& v1, const Vector2& v2, double tolerance = 0, bool normalizeVectors = true);
			/*! Returns a unit vector in the x axis.
			* \return the vector [1, 0].
			*/
			static Vector2 unitX();
			/*! Returns a unit vector in the y axis.
			* \return the vector [0, 1].
			*/
			static Vector2 unitY();
			/*! Returns a zero vector.
			* \return the vector [0, 0].
			*/
			static Vector2 zero();
			/*! Returns a unit vector.
			* \return the vector [1, 1].
			*/
			static Vector2 one();
		};

		/*!
		* \brief Represents a 3D vector.
		*
		* Represents a 3-dimensional vector of the form [x,y,z].
		* \sa Vector2, Matrix33
		*/
		class Vector3 : public Vector2
		{
			friend ostream& operator <<(ostream& os, const Vector3& v);
		protected:
			/*! The z coordinate*/
			double z;
		public:
			/*! Default contructor.*/
			Vector3();
			/*! Detailed contructor.
			* \param x the x coordinate of the new vector.
			* \param y the y coordinate of the new vector.
			* \param z the z coordinate of the new vector.
			*/
			Vector3(double x, double y, double z);
			/*! Constructs a 3D vector form a 2D vector and a Z value.
			* \param v 2D vector containing the x and y coordintes of the new vector.
			* \param z the z coordinate of the new vector.
			*/
			Vector3(const Vector2& v, double z = 0);
			/*! Gets the z coordinte.
			* \return the z coordinate.
			*/
			double getZ();
			/*! Sets the z coordinate.
			* \param the new z coordiante.
			*/
			void setZ(double z);
			double length();
			void normalize();
			/*! Calculates the dot (scalar) product of two 3D vectors.
			* \param v1 the left operand.
			* \param v2 the right operand.
			* \return the dot product <v1, v2>.
			*/
			static double Dot(const Vector3& v1, const Vector3& v2);
			/*! Calculates the cross (vector) product of two 3D vectors.
			* \param v1 the left operand.
			* \param v2 the right operand.
			* \return the dot product v1 x v2.
			*/
			static Vector3 Cross(const Vector3& v1, const Vector3& v2);
			/*! returns a sum of this vector and another 3D vector.
			* \param v the vector to add to this vector.
			* \return the sum of the two vectors;
			*/
			Vector3 operator + (const Vector3& v);
			/*! returns the difference of this vector and another 3D vector.
			* \param v the vector to subtract to this vector.
			* \return the difference of the two vectors;
			*/
			Vector3 operator - (const Vector3& v);
			/*! Returns this vector multiplied by scalar s;
			* \param s the scalar by which this vector is to be multiplied.
			* \return this vector multiplied by scalar s.
			*/
			Vector3 operator * (double s);
			/*! Returns this vector divided by scalar s;
			* \param s the scalar by which this vector is to be multiplied.
			* \return this vector divided by scalar s.
			*/
			Vector3 operator / (double s);
			/*! Adds a vector to this vector.
			* \param v the vector to add to this vector.
			*/
			void operator += (const Vector3& v);
			/*! Subtracts a vector from this vector.
			* \param v the vector to subtract from this vector.
			*/
			void operator -= (const Vector3& v);
			/*! Multiplies this vector by a scalar.
			* \param s the scalar by which to multiply this vector.
			*/
			void operator *= (double s);
			/*! Divides this vector by a scalar.
			* \param s the scalar by which to divide this vector.
			*/
			void operator /= (double s);
			/*! Compares this vector with another 3D vector.
			* \param v the vector to compare with this vector.
			* \return true if the two vectors are equal, false otherwise.
			*/
			bool operator == (const Vector3& v);
			/*! Compares tow 3D vectors with a given tolerance.
			* \param v1 left operand.
			* \param v2 right operand.
			* \param tolerance the desired tolerance.
			* \return true if the difference between each of the vector's coordiantes is smaller than the tolerance.
			*/
			static bool Equals(const Vector3& v1, const Vector3& v2, double tolerance = 0, bool normalizeVectors = true);
			/*! Returns a unit vector in the x axis.
			* \return the vector [1, 0, 0].
			*/
			static Vector3 unitX();
			/*! Returns a unit vector in the y axis.
			* \return the vector [0, 1, 0].
			*/
			static Vector3 unitY();
			/*! Returns a unit vector in the z axis.
			* \return the vector [0, 0, 1].
			*/
			static Vector3 unitZ();
			/*! Returns a zero vector.
			* \return the vector [0, 0, 0].
			*/
			static Vector3 zero();
			/*! Returns a unit vector.
			* \return the vector [1, 1, 1].
			*/
			static Vector3 one();
			/*! Returns a string decription of this vector.
			* \return a string decription of this vector.
			*/
			string toString();
		};

		/*!
		* \brief Represents a 4D vector.
		*
		* Represents a 4-dimensional vector of the form [x,y,z,w].
		* \sa Vector2, Vector3, Matrix44
		*/
		class Vector4 : public Vector3
		{
			friend ostream& operator <<(ostream& os, const Vector4& v);
		protected:
			/*! The w coordiante*/
			double w;
		public:
			/*! Default constructor.*/
			Vector4();
			/*! Detailed contructor.
			* \param x the x coordinate of the new vector.
			* \param y the y coordinate of the new vector.
			* \param z the z coordinate of the new vector.
			* \param w the w coordinate of the new vector.
			*/
			Vector4(double x, double y, double z, double w);
			/*! Constructs a 4D vector form a 3D vector and a W value.
			* \param v 3D vector containing the x, y and z coordintes of the new vector.
			* \param w the w coordinate of the new vector.
			*/
			Vector4(const Vector3& v, double w = 0 );
			/*! Gets the w coordinte.
			* \return the w coordinate.
			*/
			double getW();
			/*! Sets the w coordinate.
			* \param the new w coordiante.
			*/
			void setW(double w);
			double length();
			void normalize();
			/*! Calculates the dot (scalar) product of two 4D vectors.
			* \param v1 the left operand.
			* \param v2 the right operand.
			* \return the dot product <v1, v2>.
			*/
			static double Dot(const Vector4& v1, const Vector4& v2);
			/*! Calculates the cross (vector) product of two 4D vectors.
			* \param v1 the left operand.
			* \param v2 the right operand.
			* \return the dot product v1 x v2.
			*/
			static Vector4 Cross(const Vector4& v1, const Vector4& v2);
			/*! returns a sum of this vector and another 4D vector.
			* \param v the vector to add to this vector.
			* \return the sum of the two vectors;
			*/
			Vector4 operator + (const Vector4& v);
			/*! returns the difference of this vector and another 4D vector.
			* \param v the vector to subtract from this vector.
			* \return the difference of the two vectors;
			*/
			Vector4 operator - ( const Vector4& v);
			/*! Returns this vector multiplied by scalar s;
			* \param s the scalar by which this vector is to be multiplied.
			* \return this vector multiplied by scalar s.
			*/
			Vector4 operator * (double s);
			/*! Returns this vector divided by scalar s;
			* \param s the scalar by which this vector is to be divided.
			* \return this vector divided by scalar s.
			*/
			Vector4 operator / (double s);
			/*! Adds a vector to this vector.
			* \param v the vector to add tio this vector.
			*/
			void operator += (const Vector4& v);
			/*! Subtracts a vector from this vector.
			* \param v the vector to subtract from this vector.
			*/
			void operator -= (const Vector4& v);
			/*! Multiplies this vector by a scalar.
			* \param s the scalar by which to multiply this vector.
			*/
			void operator *= (double s);
			/*! Divides this vector by a scalar.
			* \param s the scalar by which to divide this vector.
			*/
			void operator /= (double s);
			/*! Compares this vector with another 4D vector.
			* \param v the vector to compare with this vector.
			* \return true if the two vectors are equal, false otherwise.
			*/
			bool operator == (const Vector4& v);
			/*! Compares tow 4D vectors with a given tolerance.
			* \param v1 left operand.
			* \param v2 right operand.
			* \param tolerance the desired tolerance.
			* \return true if the difference between each of the vector's coordiantes is smaller than the tolerance.
			*/
			static bool Equals(const Vector4& v1, const Vector4& v2, double tolerance = 0, bool normalizeVectors = true);
			/*! Returns a unit vector in the x axis.
			* \return the vector [1, 0, 0, 0].
			*/
			static Vector4 unitX();
			/*! Returns a unit vector in the y axis.
			* \return the vector [0, 1, 0, 0].
			*/
			static Vector4 unitY();
			/*! Returns a unit vector in the z axis.
			* \return the vector [0, 0, 1, 0].
			*/
			static Vector4 unitZ();
			/*! Returns a unit vector in the w axis.
			* \return the vector [0, 0, 0, 1].
			*/
			static Vector4 unitW();
			/*! Returns a zero vector.
			* \return the vector [0, 0, 0, 0].
			*/
			static Vector4 zero();
			/*! Returns a unit vector.
			* \return the vector [1, 1, 1, 1].
			*/
			static Vector4 one();
			/*! Returns a string decription of this vector.
			* \return a string decription of this vector.
			*/
			string toString();
		};

		/*! Stream operator for the Vector2 class.
		* \param os output stream.
		* \param v the vector to be sent to output.
		*/
		ostream& operator <<(ostream& os, const Vector2& v);
		/*! Stream operator for the Vector3 class.
		* \param os output stream.
		* \param v the vector to be sent to output.
		*/
		ostream& operator <<(ostream& os, const Vector3& v);
		/*! Stream operator for the Vector4 class.
		* \param os output stream.
		* \param v the vector to be sent to output.
		*/
		ostream& operator <<(ostream& os, const Vector4& v);
	}
}