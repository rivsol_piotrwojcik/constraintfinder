#include "SingleConstraint.h"

namespace ConstraintFinder
{
	namespace Core
	{
		SingleConstraint::SingleConstraint(ConstraintFinder::Core::Entity *e)
		{
			this->e = e;
		}

		vector<Entity*> SingleConstraint::getSubjects()
		{
			vector<Entity*> result;
			result.push_back(this->e);
			return result;
		}
	}
}