#include "SingleConstraint.h"

namespace ConstraintFinder
{
	namespace Core
	{
		namespace Constraints
		{
			class GlobalXCentered : public SingleConstraint
			{
			public:
				/*!Default constructor*/
				GlobalXCentered();
				/*!Detailed constructor
				* \param e the entitiy subject to this constraint
				*/
				GlobalXCentered(Entity* e);
				string getType() { return "CON_GLOBALXCENTERED"; };
				vector<Constraint*> findInTree(Entity* root, ToleranceSet* ts, SearchPattern sp);
				string toString();
			};
		}
	}
}