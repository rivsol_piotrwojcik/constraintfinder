#pragma once
#include "Entity.h"
#include <vector>
using namespace std;

namespace ConstraintFinder
{
	namespace Core
	{
		/*! \brief Represents a group of entities.
		* 
		* Represents a group of entities renderable independently.
		* \sa Entity
		*/
		class EntityGroup : public Entity
		{
		protected:
			/*! The list of entities belonging to this group.*/
			vector<Entity*> entities;
			void computeLocalFrame();
		public:
			/*! Default constructor.\
			* \param id the unique id assigned to each entity.
			*/
			EntityGroup(unsigned int id);
			/*! Detailed constructor.
			* \param id the unique id assigned to each entity.
			* \param entities the entities belonging to this group.
			*/
			EntityGroup(unsigned int id, vector<Entity*> entities);
			/*! Detailed constructor.
			* \param id the unique id assigned to each entity.
			* \param entities the entities belonging to this group.
			* \param translation translation vector
			* \param rotation rotation matrix
			* \param scaling scaling vector
			*/
			EntityGroup(unsigned int id, vector<Entity*> entities, const Vector3& translation, const Matrix33& rotation, const Vector3& scaling);
			/*! Detailed constructor.
			* \param id the unique id assigned to each entity.
			* \param entities the entities belonging to this group.
			* \param translation translation vector
			* \param rotation rotation matrix
			* \param scaling scaling vector
			* \param center the point defined as the center of this entity group.
			* \param localFrame the coordinate frame of this entity group.
			*/
			EntityGroup(unsigned int id, vector<Entity*> entities, const Vector3& translation, const Matrix33& rotation, const Vector3& scaling, const Vector3& center, const Frame& lcoalFrame);

			string getType() { return "ET_GROUP"; };

			/*! Index operator of the entity group.
			* \param i index of the entity to retreive.
			* \return entity at index i.
			*/
			Entity* operator [](unsigned int i);
			/*! Gets the number of entities in this group.
			* \return the number of entities in this group.
			*/
			int size();
			/*! Gets the vector of entities belonging to this group.
			* \return the vector of entities belonging to this group.
			*/
			vector<Entity*> getEntities();

			/*! Returns all CSG members of this entity group.
			* Helper method for constraint detection algorithm.
			* \return CSG members of this group.
			*/
			vector<Entity*> getCsgMembers();

			/*! Adds an entity to this entity group
			* \param e the entity to be added
			*/
			void append(Entity* e);

			/*! Adds entities to this entity group
			* \param ents the vector of entities to be added to this group
			*/
			void append(vector<Entity*> ents);

			string toString();

			vector<Entity*> toEntityList(SearchPattern sp);
		};
	}
}