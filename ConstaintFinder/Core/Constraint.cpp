#include "Constraint.h"

namespace ConstraintFinder
{
	namespace Core
	{
		vector<unsigned int> Constraint::getSubjectIds()
		{
			vector<Entity*> s = getSubjects();
			vector<unsigned int> result;
			for (vector<Entity*>::iterator it = s.begin(); it != s.end(); ++it)
				result.push_back((*it)->getId());
			return result;
		}
	}
}