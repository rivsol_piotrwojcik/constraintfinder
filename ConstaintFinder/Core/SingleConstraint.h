#pragma once
#include "Constraint.h"

namespace ConstraintFinder
{
	namespace Core
	{
		/*! \brief Represents a single, or non-group constraint.
		* Constraints of this type are not treated as entity groups, and are not passed along
		* as input to further iterations, form the one in which they are detected.
		* \sa Constraint
		*/
		class SingleConstraint : public Constraint
		{
		protected:
			/*!The entity subject to this constraint*/
			Entity* e;
		public:
			/*!Constructor.
			* \param e the entity subject to this constraint.
			*/
			SingleConstraint(Entity* e);
			ConstraintClass getClass() { return CC_SINGLE; };
			vector<Entity*> getSubjects();
		};
	}
}