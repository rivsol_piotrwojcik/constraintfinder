#include "ToleranceSet.h"

namespace ConstraintFinder
{
	namespace Core
	{
		ToleranceSet::ToleranceSet()
		{
			this->angularDeviation = 0.0;
			this->lengthDeviation = 0.0;
			this->vector3DDeviation = 0.0;
			this->orthogonalityDeviation = 0.0;
			this->zeroDistanceDeviation = 0.0;
		}

		ToleranceSet* ToleranceSet::defaultToleranceSet()
		{
			ToleranceSet* ts = new ToleranceSet();
			ts->vector3DDeviation = 0.001;
			ts->lengthDeviation = 0.001;
			ts->angularDeviation = 0.0001;
			ts->orthogonalityDeviation = 0.0001;
			ts->zeroDistanceDeviation = 0.0001;

			return ts;
		}
	}
}