#include "Vector.h"
#include <cmath>

namespace ConstraintFinder
{
	namespace Core
	{
		Vector2::Vector2() { x = y = 0;}

		Vector2::Vector2(double x, double y) {this->x = x; this->y=y;}

		double Vector2::getX() {return x;}

		double Vector2::getY() {return y;}

		void Vector2::setX(double x) {this->x=x;}

		void Vector2::setY(double y) {this->y=y;}

		double Vector2::length()
		{
			return sqrt(x*x + y*y);
		}

		void Vector2::normalize()
		{
			double len = sqrt(x*x + y*y);
			x = x / len;
			y = y / len;
		}

		double Vector2::Dot(const Vector2& v1, const Vector2& v2)
		{
			return v1.x*v2.x + v1.y*v2.y;
		}

		Vector2 Vector2::Cross(const Vector2& v1, const Vector2& v2)
		{
			return Vector2(); //TODO
		}

		Vector2 Vector2::operator +(const Vector2 &v)
		{
			return Vector2(x + v.x, y + v.y);
		}

		Vector2 Vector2::operator -(const Vector2 &v)
		{
			return Vector2(x - v.x, y - v.y);
		}

		Vector2 Vector2::operator *(double s)
		{
			return Vector2(x*s, y*s);
		}

		Vector2 Vector2::operator /(double s)
		{
			return Vector2(x/s, y/s);
		}

		void Vector2::operator +=(const Vector2 &v)
		{
			x += v.x;
			y += v.y;
		}

		void Vector2::operator -=(const Vector2 &v)
		{
			x -= v.x;
			y -= v.y;
		}

		void Vector2::operator *=(double s)
		{
			x *= s;
			y *= s;
		}

		void Vector2::operator /=(double s)
		{
			x /= s;
			y /= s;
		}

		bool Vector2::operator ==(const Vector2& v)
		{
			return x == v.x && y == v.y;
		}

		bool Vector2::Equals(const Vector2& v1, const Vector2& v2, double tolerance, bool normalizeVectors)
		{
			Vector2 v3;
			Vector2 n1 = v1;
			Vector2 n2 = v2;
			if (normalizeVectors)
			{
				n1.normalize();
				n2.normalize();
				
			}
			v3 = n2 - n1;
			if (v3.length() < tolerance)
				return true;
			return false;
		}

		Vector2 Vector2::unitX()
		{
			return Vector2(1,0);
		}

		Vector2 Vector2::unitY()
		{
			return Vector2(0,1);
		}

		Vector2 Vector2::zero()
		{
			return Vector2();
		}

		Vector2 Vector2::one()
		{
			return Vector2(1,1);
		}


		Vector3::Vector3() :Vector2() {z = 0;}

		Vector3::Vector3(double x, double y, double z) :Vector2(x,y) {this->z = z;}

		Vector3::Vector3(const Vector2 &v, double z) :Vector2(v) {this->z=z;}

		double Vector3::getZ() {return z;}

		void Vector3::setZ(double z) {this->z=z;}

		double Vector3::length()
		{
			return sqrt(x*x + y*y + z*z);
		}

		void Vector3::normalize()
		{
			double len = sqrt(x*x + y*y + z*z);
			x = x / len;
			y = y / len;
			z = z / len;
		}

		double Vector3::Dot(const Vector3& v1, const Vector3& v2)
		{
			return v1.x*v2.x + v1.y*v2.y + v1.z*v2.z;
		}

		Vector3 Vector3::Cross(const Vector3& v1, const Vector3& v2)
		{
			return Vector3(
				v1.y*v2.z - v1.z*v2.y, 
				v1.z*v2.x - v1.x*v2.z, 
				v1.x*v2.y - v1.y*v2.x
				);
		}

		Vector3 Vector3::operator +(const Vector3 &v)
		{
			return Vector3(x + v.x, y + v.y, z + v.z);
		}

		Vector3 Vector3::operator -(const Vector3 &v)
		{
			return Vector3(x - v.x, y - v.y, z - v.z);
		}

		Vector3 Vector3::operator *(double s)
		{
			return Vector3(x*s, y*s, z*s);
		}

		Vector3 Vector3::operator /(double s)
		{
			return Vector3(x/s, y/s, z/s);
		}

		void Vector3::operator +=(const Vector3 &v)
		{
			x += v.x;
			y += v.y;
			z += v.z;
		}

		void Vector3::operator -=(const Vector3 &v)
		{
			x -= v.x;
			y -= v.y;
			z -= v.z;
		}

		void Vector3::operator *=(double s)
		{
			x *= s;
			y *= s;
			z *= s;
		}

		void Vector3::operator /=(double s)
		{
			x /= s;
			y /= s;
			z /= s;
		}

		bool Vector3::operator ==(const Vector3& v)
		{
			return x == v.x && y == v.y && z == v.z;
		}

		bool Vector3::Equals(const Vector3& v1, const Vector3& v2, double tolerance, bool normalizeVectors)
		{
			Vector3 v3;
			Vector3 n1 = v1;
			Vector3 n2 = v2;
			if (normalizeVectors)
			{	
				n1.normalize();
				n2.normalize();
			}
			v3 = n2 - n1;
			if (v3.length() < tolerance)
				return true;
			return false;
		}

		Vector3 Vector3::unitX()
		{
			return Vector3(1,0,0);
		}

		Vector3 Vector3::unitY()
		{
			return Vector3(0,1,0);
		}

		Vector3 Vector3::unitZ()
		{
			return Vector3(0,0,1);
		}

		Vector3 Vector3::zero()
		{
			return Vector3();
		}

		Vector3 Vector3::one()
		{
			return Vector3(1,1,1);
		}


		Vector4::Vector4() :Vector3() {w=0;}

		Vector4::Vector4(double x, double y, double z, double w) :Vector3(x,y,z) {this->w=w;}

		Vector4::Vector4(const Vector3& v, double w) :Vector3(v) {this->w=w;}

		double Vector4::getW() {return w;}

		void Vector4::setW(double w) {this->w=w;}

		double Vector4::length()
		{
			return sqrt(x*x + y*y + z*z + w*w);
		}

		void Vector4::normalize()
		{
			double len = sqrt(x*x + y*y + z*z + w*w);
			x = x / len;
			y = y / len;
			z = z / len;
			w = w / len;
		}

		double Vector4::Dot(const Vector4& v1, const Vector4& v2)
		{
			return v1.x*v2.x + v1.y*v2.y + v1.z*v2.z + v1.w*v2.w;
		}

		Vector4 Vector4::Cross(const Vector4 &v1, const Vector4 &v2)
		{
			return Vector4(); //TODO
		}

		Vector4 Vector4::operator +(const Vector4 &v)
		{
			return Vector4(x + v.x, y + v.y, z + v.z, w + v.w);
		}

		Vector4 Vector4::operator -(const Vector4 &v)
		{
			return Vector4(x - v.x, y - v.y, z - v.z, w - v.w);
		}

		Vector4 Vector4::operator *(double s)
		{
			return Vector4(x*s, y*s, z*s, w*s);
		}

		Vector4 Vector4::operator /(double s)
		{
			return Vector4(x/s, y/s, z/s, w/s);
		}

		void Vector4::operator +=(const Vector4 &v)
		{
			x += v.x;
			y += v.y;
			z += v.z;
			w += v.w;
		}

		void Vector4::operator -=(const Vector4 &v)
		{
			x -= v.x;
			y -= v.y;
			z -= v.z;
			w -= v.w;
		}

		void Vector4::operator *=(double s)
		{
			x *= s;
			y *= s;
			z *= s;
			w *= s;
		}

		void Vector4::operator /=(double s)
		{
			x /= s;
			y /= s;
			z /= s;
			w /= s;
		}

		bool Vector4::operator ==(const Vector4& v)
		{
			return x == v.x && y == v.y && z == v.z && w == v.w;
		}

		bool Vector4::Equals(const Vector4& v1, const Vector4& v2, double tolerance, bool normalizeVectors)
		{
			Vector4 v3;
			Vector4 n1 = v1;
			Vector4 n2 = v2;
			if (normalizeVectors)
			{	
				n1.normalize();
				n2.normalize();
			}
			v3 = n2 - n1;
			if (v3.length() < tolerance)
				return true;
			return false;
		}

		Vector4 Vector4::unitX()
		{
			return Vector4(1,0,0,0);
		}

		Vector4 Vector4::unitY()
		{
			return Vector4(0,1,0,0);
		}

		Vector4 Vector4::unitZ()
		{
			return Vector4(0,0,1,0);
		}

		Vector4 Vector4::unitW()
		{
			return Vector4(0,0,0,1);
		}

		Vector4 Vector4::zero()
		{
			return Vector4();
		}

		Vector4 Vector4::one()
		{
			return Vector4(1,1,1,1);
		}

		ostream& operator <<(ostream& os, const Vector2& v)
		{
			return os<<"["<<v.x<<","<<v.y<<"]";
		}

		ostream& operator <<(ostream& os, const Vector3& v)
		{
			return os<<"["<<v.x<<","<<v.y<<","<<v.z<<"]";
		}

		ostream& operator <<(ostream& os, const Vector4& v)
		{
			return os<<"["<<v.x<<","<<v.y<<","<<v.z<<","<<v.w<<"]";
		}
	}
}