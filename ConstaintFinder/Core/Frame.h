#include "Vector.h"

namespace ConstraintFinder
{
	namespace Core
	{
		/*! \brief A coordinate frame structure.
		* 
		* This structure contains 3 coordinate axes that form a coordinate frame.
		* \sa Vector3
		*/
		struct Frame
		{
			/*! the x axis.*/
			Vector3 x;
			/*! the y axis.*/
			Vector3 y;
			/*! the z axis.*/
			Vector3 z;

			/*! Default constructor.*/
			Frame();

			/*! Detailed constructor.
			* \param ox the x axis of the new coordinate frame.
			* \param oy the y axis of the new coordinate frame.
			* \param oz the z axis of the new coordinate frame.
			*/
			Frame (const Vector3& ox, const Vector3& oy, const Vector3& oz);

			/*! Returns an orthonormal coordinate frame.
			* \return the frame ([1,0,0], [0,1,0], [0,0,1]).
			*/
			static Frame orthonormal();

			static Frame mean(const Frame& f1, const Frame& f2);

			static bool isOrthogonal(const Frame& f, double tolerance = 0);

		};

		ostream& operator <<(ostream& os, const Frame& f);
	}
}