#include "Ellipsoid.h"

namespace ConstraintFinder
{
	namespace Core
	{
		Ellipsoid::Ellipsoid(unsigned int id, double eX, double eY, double eZ)
			:Entity(id)
		{
			this->eX = eX;
			this->eY = eY;
			this->eZ = eZ;
			computeLocalFrame();
		}

		double Ellipsoid::getEX() { return this->eX; }
		double Ellipsoid::getEY() { return this->eY; }
		double Ellipsoid::getEZ() { return this->eZ; }

		vector<Entity*> Ellipsoid::toEntityList(SearchPattern sp)
		{
			vector<Entity*> result;
			result.push_back(this);
			return result;
		}
	}
}