#include "MultiConstraint.h"

namespace ConstraintFinder
{
	namespace Core
	{
		namespace Constraints
		{
			class Parallel : public MultiConstraint
			{
			public:
				Parallel();
				Parallel(unsigned int id, Entity* e1, Entity* e2);
				string getType() { return "CON_PARALLEL"; };
				vector<Constraint*> findInTree(Entity* root, ToleranceSet* ts, SearchPattern sp);
				string toString();
			};
		}
	}
}