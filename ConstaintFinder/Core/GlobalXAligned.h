#include "SingleConstraint.h"

namespace ConstraintFinder
{
	namespace Core
	{
		/*!This namespace contains the geometrical constraints implemented in the application.*/
		namespace Constraints
		{
			/*! \biref Represents alignment with the global X axis.
			* 
			* An entity subject to this constraint will have one of its local axes aligned with the global X axis.
			* \sa SingleConstraint, Constraint
			*/
			class GlobalXAligned : public SingleConstraint
			{
			public:
				/*!Default constructor*/
				GlobalXAligned();
				/*!Detailed constructor
				* \param e the entitiy subject to this constraint
				*/
				GlobalXAligned(Entity* e);
				string getType() { return "CON_GLOBALXALIGNED"; };
				vector<Constraint*> findInTree(Entity* root, ToleranceSet* ts, SearchPattern sp);
				string toString();
			};
		}
	}
}