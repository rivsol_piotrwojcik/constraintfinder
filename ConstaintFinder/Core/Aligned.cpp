#include "Aligned.h"

namespace ConstraintFinder
{
	namespace Core
	{
		namespace Constraints
		{
			Aligned::Aligned()
				:MultiConstraint(-1)
			{
			}

			Aligned::Aligned(unsigned int id, vector<Entity*> ents)
				:MultiConstraint(id, ents)
			{
			}

			string Aligned::toString()
			{
				string s = "[Aligned] for entities: ";
				for (vector<Entity*>::iterator it = entities.begin(); it != entities.end(); ++it)
				{
					s += (*it)->getType();
					s += " ";
				}
				s += "with ids: ";
				char* buf = new char[32];
				for (vector<Entity*>::iterator it = entities.begin(); it != entities.end(); ++it)
				{
					_itoa_s((*it)->getId(), buf, 32, 10);
					s += string(buf);
					s += " ";
				}
				s += "; Group  id: ";
				_itoa_s(getId(), buf, 32, 10);
				s += string(buf);
				delete[] buf;
				return s;
			}

			bool areAligned(vector<Entity*> ents, Entity* c, ToleranceSet* ts)
			{
				if (c->getId() == DUMMYGROUPID) 
					return false;
				Vector3 p0 = ents[0]->getCenter();
				Vector3 p2 = c->getCenter();
				Vector3 dir = ents[1]->getCenter() - ents[0]->getCenter();
				dir.normalize();
				if (distancePointPoint(p0, p2) <= ts->zeroDistanceDeviation)
					return false; //objects are cocentral
				double d = distancePointLine(p2, p0, dir);
				return d <= ts->zeroDistanceDeviation;
					
			}
			
			vector<Constraint*> Aligned::findInTree(Entity* root, ToleranceSet* ts, SearchPattern sp)
			{
				vector<Constraint*> result;
				vector<Entity*> entityList;
				vector<Entity*> temp;
				string rootType = root->getType();

				entityList = root->toEntityList(sp);
				for(vector<Entity*>::iterator s1 = entityList.begin(); s1 != entityList.end(); ++s1)
				{
					for(vector<Entity*>::iterator s2 = entityList.begin(); s2 != entityList.end(); ++s2)
					{
						if (*s1 == *s2)
							continue;
						temp.clear();
						temp.push_back(*s1);
						temp.push_back(*s2);
						for(vector<Entity*>::iterator c = entityList.begin(); c != entityList.end(); ++c)
						{
							if (*c == *s1 || *c == *s2)
								continue;
							if (areAligned(temp, *c, ts))
								temp.push_back(*c);
						}
						if (temp.size() > 2)
							result.push_back(new Aligned(getNextId(), temp));
					}
				}
				return result;
			}
		}
	}
}