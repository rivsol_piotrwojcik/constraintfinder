#include "Frame.h"

namespace ConstraintFinder
{
	namespace Core
	{
		Frame::Frame()
		{
			x = Vector3::unitX();
			y = Vector3::unitY();
			z = Vector3::unitZ();
		}

		Frame::Frame (const Vector3& ox, const Vector3& oy, const Vector3& oz)
		{
			x = ox;
			y = oy; 
			z = oz;
		}

		Frame Frame::orthonormal() 
		{ 
			return Frame();
		}

		Frame Frame::mean(const Frame& f1, const Frame& f2)
		{
			Frame result;
			Vector3 v1 = f1.x;
			Vector3 v2 = f2.x;
			result.x = v1 + v2;
			result.x.normalize();
			v1 = f1.y;
			v2 = f2.y;
			result.y = v1 + v2;
			result.y.normalize();
			v1 = f1.z;
			v2 = f2.z;
			result.z = v1 + v2;
			result.z.normalize();
			return result;
		}

		bool Frame::isOrthogonal(const Frame& f, double tolerance)
		{
			if (Vector3::Dot(f.x, f.y) <=tolerance && Vector3::Dot(f.y, f.z) <= tolerance && Vector3::Dot(f.x, f.z) <= tolerance)
				return true;
			return false;
		}

		ostream& operator <<(ostream& os, const Frame& f)
		{
			return os<<"{"<<f.x<<","<<f.y<<","<<f.z<<"}";
		}
	}
}