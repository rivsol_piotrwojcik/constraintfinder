#include "Matrix.h"

namespace ConstraintFinder
{
	namespace Core
	{
		Matrix22::Matrix22()
		{
			this->m00 = 0;
			this->m01 = 0;
			this->m10 = 0;
			this->m11 = 0;
		}

		Matrix22::Matrix22(double m00, double m01, double m10, double m11)
		{
			this->m00 = m00;
			this->m01 = m01;
			this->m10 = m10;
			this->m11 = m11;
		}

		double Matrix22::getEntry(int i, int j)
		{
			switch (i)
			{
			case 0:
				switch (j)
				{
				case 0:
					return m00;
					break;
				case 1:
					return m01;
					break;
				}
				break;
			case 1:
				switch (j)
				{
				case 0:
					return m10;
					break;
				case 1:
					return m11;
					break;
				}
			}
			throw ("Matrix22::getEntry -> Array index out of bounds");
		}

		void Matrix22::setEntry(int i, int j, double a)
		{
			switch (i)
			{
			case 0:
				switch (j)
				{
				case 0:
					m00 = a;
					return;
					break;
				case 1:
					m01 = a;
					return;
					break;
				}
				break;
			case 1:
				switch (j)
				{
				case 0:
					m10 = a;
					return;
					break;
				case 1:
					m11 = a;
					return;
					break;
				}
			}
			throw ("Matrix22::getEntry -> Array index out of bounds");
		}

		Matrix22 Matrix22::operator +(const Matrix22& m)
		{
			return Matrix22(m00 + m.m00, m01 + m.m01, m10 + m.m10, m11 + m.m11);
		}

		void Matrix22::operator +=(const Matrix22& m)
		{
			m00 += m.m00;
			m01 += m.m01;
			m10 += m.m10;
			m11 += m.m11;
		}

		Matrix22 Matrix22::operator -(const Matrix22& m)
		{
			return Matrix22(m00 - m.m00, m01 - m.m01, m10 - m.m10, m11 - m.m11);
		}

		void Matrix22::operator -=(const Matrix22& m)
		{
			m00 -= m.m00;
			m01 -= m.m01;
			m10 -= m.m10;
			m11 -= m.m11;
		}

		Matrix22 Matrix22::operator *(const Matrix22& m)
		{
			return Matrix22(m00 * m.m00 + m01 * m.m10, 
				m00 * m.m01 + m01 * m.m11, 
				m10 * m.m00 + m11 * m.m10, 
				m10 * m.m01 + m11 * m.m11);
		}

		void Matrix22::operator *=(const Matrix22& m)
		{
			Matrix22 r;
			r.m00 = m00 * m.m00 + m01 * m.m10;
			r.m01 = m00 * m.m01 + m01 * m11;
			r.m10 = m10 * m.m00 + m11 * m10;
			r.m11 = m10 * m.m01 + m11 * m11;
			m00 = r.m00;
			m01 = r.m01;
			m10 = r.m10;
			m11 = r.m11;
		}

		GLfloat* Matrix22::toGLfloat16()
		{
			GLfloat* m = new GLfloat[16];
			m[0] = m00;
			m[1] = m10;
			m[2] = 0;
			m[3] = 0;
			m[4] = m01;
			m[5] = m11;
			m[6] = 0;
			m[7] = 0;
			m[8] = 0;
			m[9] = 0;
			m[10] = 0;
			m[11] = 0;
			m[12] = 0;
			m[13] = 0;
			m[14] = 0;
			m[15] = 1;
			return m;
		}

		Matrix22 Matrix22::zero()
		{
			return Matrix22();
		}

		Matrix22 Matrix22::identity()
		{
			return Matrix22(1, 0, 0, 1);
		}

		Matrix33::Matrix33() :Matrix22()
		{
			this->m02 = 0;
			this->m12 = 0;
			this->m20 = 0;
			this->m21 = 0;
			this->m22 = 0;
		}

		Matrix33::Matrix33(double m00, double m01, double m02, double m10, double m11, double m12, double m20, double m21, double m22)
			:Matrix22(m00, m01, m10, m11)
		{
			this->m02 = m02;
			this->m12 = m12;
			this->m20 = m20;
			this->m21 = m21;
			this->m22 = m22;
		}

		Matrix33::Matrix33(const Matrix22 &m, double m02, double m12, double m20, double m21, double m22)
			:Matrix22(m)
		{
			this->m02 = m02;
			this->m12 = m12;
			this->m20 = m20;
			this->m21 = m21;
			this->m22 = m22;
		}

		double Matrix33::getEntry(int i, int j)
		{
			if (i >= 0 && i < 2 && j >= 0 && j < 2)
			{
				return Matrix22::getEntry(i,j);
			}
			switch (i)
			{
			case 0:
				if (j == 2) return m02;
				break;
			case 1:
				if (j == 2) return m12;
				break;
			case 2:
				switch (j)
				{
				case 0:
					return m20;
					break;
				case 1:
					return m21;
					break;
				case 2:
					return m22;
					break;
				}
			}
			throw ("Matrix33::getEntry -> Array index out of bounds");
		}

		void Matrix33::setEntry(int i, int j, double a)
		{
			if (i >= 0 && i < 2 && j >= 0 && j < 2)
			{
				Matrix22::setEntry(i,j,a);
				return;
			}
			switch (i)
			{
			case 0:
				if (j == 2) { m02 = a; return; }
				break;
			case 1:
				if (j == 2) { m12 = a; return; }
				break;
			case 2:
				switch (j)
				{
				case 0:
					m20 = a;
					return;
					break;
				case 1:
					m21 = a;
					return;
					break;
				case 2:
					m22 = a;
					return;
					break;
				}
			}
			throw ("Matrix33::getEntry -> Array index out of bounds");
		}

		Matrix33 Matrix33::operator +(const Matrix33& m)
		{
			return Matrix33(m00 + m.m00, m01 + m.m01, m02 + m.m02, m10 + m.m10, m11 + m.m11, m12 + m.m12, m20 + m.m20, m21 + m.m21, m22 + m.m22);
		}

		void Matrix33::operator +=(const Matrix33& m)
		{
			m00 += m.m00;
			m01 += m.m01;
			m02 += m.m02;
			m10 += m.m10;
			m11 += m.m11;
			m12 += m.m12;
			m20 += m.m20;
			m21 += m.m21;
			m22 += m.m22;
		}

		Matrix33 Matrix33::operator -(const Matrix33& m)
		{
			return Matrix33(m00 - m.m00, m01 - m.m01, m02 - m.m02, m10 - m.m10, m11 - m.m11, m12 - m.m12, m20 - m.m20, m21 - m.m21, m22 - m.m22);
		}

		void Matrix33::operator -=(const Matrix33& m)
		{
			m00 -= m.m00;
			m01 -= m.m01;
			m02 -= m.m02;
			m10 -= m.m10;
			m11 -= m.m11;
			m12 -= m.m12;
			m20 -= m.m20;
			m21 -= m.m21;
			m22 -= m.m22;
		}

		Matrix33 Matrix33::operator *(const Matrix33& m)
		{
			return Matrix33(
				m00 * m.m00 + m01 * m.m10 + m02 * m.m20, 
				m00 * m.m01 + m01 * m.m11 + m02 * m.m21, 
				m00 * m.m02 + m01 * m.m12 + m02 * m.m22,
				m10 * m.m00 + m11 * m.m10 + m12 * m.m20, 
				m10 * m.m01 + m11 * m.m11 + m12 * m.m21,
				m10 * m.m02 + m11 * m.m12 + m12 * m.m22,
				m20 * m.m00 + m21 * m.m10 + m22 * m.m20,
				m20 * m.m01 + m21 * m.m11 + m22 * m.m21,
				m20 * m.m02 + m21 * m.m12 + m22 * m.m22);
		}

		void Matrix33::operator *=(const Matrix33& m)
		{
			Matrix33 r;
			r.m00 = m00 * m.m00 + m01 * m.m10 + m02 * m.m20;
			r.m01 = m00 * m.m01 + m01 * m.m11 + m02 * m.m21;
			r.m02 = m00 * m.m02 + m01 * m.m12 + m02 * m.m22;

			r.m10 = m10 * m.m00 + m11 * m.m10 + m12 * m.m20;
			r.m11 = m10 * m.m01 + m11 * m.m11 + m12 * m.m21;
			r.m12 = m10 * m.m02 + m11 * m.m12 + m12 * m.m22;

			r.m20 = m20 * m.m00 + m21 * m.m10 + m22 * m.m20;
			r.m21 = m20 * m.m01 + m21 * m.m11 + m22 * m.m21;
			r.m22 = m20 * m.m02 + m21 * m.m12 + m22 * m.m22;
			m00 = r.m00;
			m01 = r.m01;
			m02 = r.m02;
			m10 = r.m10;
			m11 = r.m11;
			m12 = r.m12;
			m20 = r.m20;
			m21 = r.m21;
			m22 = r.m22;
		}

		GLfloat* Matrix33::toGLfloat16()
		{
			GLfloat* m = new GLfloat[16];
			m[0] = m00;
			m[1] = m10;
			m[2] = m20;
			m[3] = 0;
			m[4] = m01;
			m[5] = m11;
			m[6] = m21;
			m[7] = 0;
			m[8] = m02;
			m[9] = m12;
			m[10] = m22;
			m[11] = 0;
			m[12] = 0;
			m[13] = 0;
			m[14] = 0;
			m[15] = 1;
			return m;
		}

		Matrix33 Matrix33::zero()
		{
			return Matrix33();
		}

		Matrix33 Matrix33::identity()
		{
			return Matrix33(1,0,0,0,1,0,0,0,1);
		}

		Matrix33 Matrix33::rotationX(double angle)
		{
			double dcos = cos(angle);
			double dsin = sin(angle);
			return Matrix33(1,0,0, 0,dcos,-dsin, 0,dsin,dcos);
		}

		Matrix33 Matrix33::rotationY(double angle)
		{
			double dcos = cos(angle);
			double dsin = sin(angle);
			return Matrix33(dcos,0,dsin, 0,1,0, -dsin,0,dcos);
		}

		Matrix33 Matrix33::rotationZ(double angle)
		{
			double dcos = cos(angle);
			double dsin = sin(angle);
			return Matrix33(dcos,-dsin,0, dsin,dcos,0, 0,0,1);
		}

		ostream& operator <<(ostream& os, const Matrix22& m)
		{
			return os<<"["<<m.m00<<","<<m.m01<<"; "<<m.m10<<","<<m.m11<<"]";
		}

		ostream& operator <<(ostream& os, const Matrix33& m)
		{
			return os<<"["<<m.m00<<","<<m.m01<<","<<m.m02<<"; "<<m.m10<<","<<m.m11<<","<<m.m12<<"; "<<m.m20<<","<<m.m21<<","<<m.m22<<"]";
		}

		Vector3 operator * (Matrix33& m, Vector3& v)
		{
			return Vector3(m.getEntry(0,0)*v.getX() + m.getEntry(0,1)*v.getY() + m.getEntry(0,2)*v.getZ(), 
				m.getEntry(1,0)*v.getX() + m.getEntry(1,1)*v.getY() + m.getEntry(1,2)*v.getZ(), 
				m.getEntry(2,0)*v.getX() + m.getEntry(2,1)*v.getY() + m.getEntry(2,2)*v.getZ());
		}

		Vector3 operator * (Vector3& v, Matrix33& m)
		{
			return Vector3(m.getEntry(0,0)*v.getX() + m.getEntry(1,0)*v.getY() + m.getEntry(2,0)*v.getZ(), 
				m.getEntry(0,1)*v.getX() + m.getEntry(1,1)*v.getY() + m.getEntry(2,1)*v.getZ(), 
				m.getEntry(0,2)*v.getX() + m.getEntry(1,2)*v.getY() + m.getEntry(2,2)*v.getZ());
		}

		double DegToRadian(double deg)
		{
			return ((double)PI/180.0)*deg;
		}

		double RadianToDeg(double rad)
		{
			return (180.0/(double)PI)*rad;
		}

		double distancePointLine(Vector3& p, Vector3& lp, Vector3& ln)
		{
			return (p - (lp + ln*Vector3::Dot(p - lp, ln))).length();
		}

		double distancePointPoint(Vector3& p1, Vector3& p2)
		{
			return (p2 - p1).length();
		}
	}
}