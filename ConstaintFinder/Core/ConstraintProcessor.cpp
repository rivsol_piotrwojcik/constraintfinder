#include "ConstraintProcessor.h"
namespace ConstraintFinder
{
	namespace Core
	{
		ToleranceSet* ConstraintProcessor::tolSet = ToleranceSet::defaultToleranceSet();
		ConstraintSet* ConstraintProcessor::conSet = ConstraintSet::defualtConstraintSet();
		SearchPattern ConstraintProcessor::searchPattern = SP_PYRAMID;
		int ConstraintProcessor::numOfIterations = 1;

		void ConstraintProcessor::intializeConstarintProcessor()
		{
			tolSet = ToleranceSet::defaultToleranceSet();
			conSet = ConstraintSet::defualtConstraintSet();
			numOfIterations = 1;
			searchPattern = SP_PYRAMID;
		}

		ToleranceSet* ConstraintProcessor::getToleranceSet()
		{
			return tolSet;
		}

		void ConstraintProcessor::setToleranceSet(ToleranceSet* ts)
		{
			tolSet = ts;
		}

		ConstraintSet* ConstraintProcessor::getConstraintSet()
		{
			return conSet;
		}

		void ConstraintProcessor::setConstraintSet(ConstraintSet* cs)
		{
			conSet = cs;
		}

		int ConstraintProcessor::getNumberOfInterations()
		{
			return numOfIterations;
		}

		void ConstraintProcessor::setNumberOfIterations(int n)
		{
			if (numOfIterations < 0 || numOfIterations > 1024)
				throw new std::exception("ConstraintProcessor::setNumberOfIterations -> valid numbers: 0 - 1024");
			numOfIterations = n;
		}

		SearchPattern ConstraintProcessor::getSearchPattern()
		{
			return searchPattern;
		}

		void ConstraintProcessor::setSearchPattern(SearchPattern sp)
		{
			searchPattern = sp;
		}

		vector<Constraint*> ConstraintProcessor::detectConstranitsIter(EntityGroup* root, SearchPattern sp)
		{
			vector<Constraint*> result;
			vector<Constraint*> temp;
			vector<Constraint*> dummies = conSet->getEmptyInstances();
			for (int i=0; i<(int)dummies.size(); ++i)
			{
				if (dummies[i])
				{
					temp = dummies[i]->findInTree(root, tolSet, sp);
					if (temp.size() != 0)
						result.insert(result.end(), temp.begin(), temp.end());
				}
			}
			result = filterOutDummyGroup(result);
			return result;
		}

		vector<Constraint*> ConstraintProcessor::detectConstraints(EntityGroup* root)
		{
			EntityGroup* L = new EntityGroup(DUMMYGROUPID, root->getEntities());
			vector<Constraint*> result;
			vector<Constraint*> temp;
			vector<Entity*> multi;
			cout<<"Constraint search started\n";
			if (numOfIterations > 0)
			{
				cout<<"Number of iterations: "<<numOfIterations<<"\n";
				for (int i=0; i<numOfIterations; ++i)
				{
					cout<<"Entities for this iteration: "<<L->size()<<"\n";
					temp = ConstraintProcessor::detectConstranitsIter(L,searchPattern);
					multi = multiConstarintsOnly(temp);
					if (searchPattern != SP_PYRAMID)
						L->append(multi);
					else 
						L = new EntityGroup(DUMMYGROUPID, multi);
					result = insertNoRedundant(result, temp);
					cout<<"completed iteration: "<<i+1<<" of "<<numOfIterations<<"\n";
				}
				cout<<"Search complete. "<<result.size()<<" constraints found.\n";
			}
			if (numOfIterations == 0)
			{
				cout<<"Number of iterations: unbounded\n";
				for (int i=0; i<1024; ++i)
				{
					temp = ConstraintProcessor::detectConstranitsIter(L,searchPattern);
					multi = multiConstarintsOnly(temp);
					size_t n = result.size();
					result = insertNoRedundant(result, temp);
					if (n == result.size())
					{
						cout<<"Search complete. "<<result.size()<<" constraints found.\n";
					}
					if (searchPattern != SP_PYRAMID)
						L->append(multi);
					else 
						L = new EntityGroup(DUMMYGROUPID, multi);
				}
				cout<<"Search stopped. Maximum number of iterations reached. "<<result.size()<<" constranits found.\n";
			}
			if (searchPattern != SP_BRUTEFORCE)
			{
				cout<<"Root search complete. "<<result.size()<<"constraints found\n";
				vector<Entity*> vCsg = root->getCsgMembers();
				cout<<"Searching CSG trees\n";
				for (int i=0; i<(int)vCsg.size(); ++i)
				{
					delete L;
					L = new EntityGroup(DUMMYGROUPID);
					L->append(vCsg[i]);
					int prev = (int)result.size();
					cout<<"-- CSG tree "<<i+1<<"/"<<vCsg.size()<<"\n";
					if (numOfIterations > 0)
					{
						cout<<"Number of iterations: "<<numOfIterations<<"\n";
						for (int i=0; i<numOfIterations; ++i)
						{
							temp = ConstraintProcessor::detectConstranitsIter(L,SP_BRUTEFORCE);
							multi = multiConstarintsOnly(temp);
							if (searchPattern != SP_PYRAMID)
								L->append(multi);
							else 
								L = new EntityGroup(DUMMYGROUPID, multi);
							result = insertNoRedundant(result, temp);
						}
						cout<<"Search complete. "<<result.size() - prev<<" constraints found.\n";
					}
					if (numOfIterations == 0)
					{
						cout<<"Number of iterations: unbounded\n";
						for (int i=0; i<1024; ++i)
						{
							temp = ConstraintProcessor::detectConstranitsIter(L, SP_BRUTEFORCE);
							multi = multiConstarintsOnly(temp);
							size_t n = result.size();
							result = insertNoRedundant(result, temp);
							if (n == result.size())
							{
								result = filterOutDummyGroup(result);
								cout<<"Search complete. "<<result.size()<<" constraints found.\n";
							}
							if (searchPattern != SP_PYRAMID)
								L->append(multi);
							else 
								L = new EntityGroup(DUMMYGROUPID, multi);
						}
						cout<<"Search stopped. Maximum number of iterations reached. "<<result.size() - prev<<" constranits found.\n";
					}
				}
			}
			cout<<"Total constraints: "<<result.size()<<"\n";	
			return result;
		}

		vector<Constraint*> ConstraintProcessor::insertNoRedundant(std::vector<Constraint*> target, std::vector<Constraint*> v)
		{
			vector<Constraint*> result;
			result.insert(result.end(), target.begin(), target.end());
			for (vector<Constraint*>::iterator c = v.begin(); c != v.end(); ++c)
			{
				bool found = false;
				for (vector<Constraint*>::iterator it = result.begin(); it != result.end(); ++it)
				{
					if (Equal(*c, *it))
					{
						found = true;
						break;
					}
				}
				if (!found)
					result.push_back(*c);
			}
			return result;
		}

		vector<Entity*> ConstraintProcessor::multiConstarintsOnly(vector<Constraint*> v)
		{
			vector<Entity*> result;
			vector<Constraint*> multiCons;
			for (vector<Constraint*>::iterator it = v.begin(); it != v.end(); ++it)
			{
				if ((*it)->getClass() == CC_MULTI)
					multiCons.push_back(*it);
			}
			multiCons = insertNoRedundant(vector<Constraint*>(), multiCons);
			for(vector<Constraint*>::iterator it = multiCons.begin(); it != multiCons.end(); ++it)
				result.push_back(dynamic_cast<Entity*>(*it));
			return result;
		}

		bool ConstraintProcessor::Equal(Constraint* c1, Constraint* c2)
		{
			if (c1->getType() != c2->getType())
				return false;
			if (c1->getClass() == CC_SINGLE && c1->getSubjectIds()[0] == c2->getSubjectIds()[0])
			{
				return true;
			}
			else if (c1->getClass() == CC_MULTI)
			{
				vector<unsigned int> v1 = c1->getSubjectIds();
				vector<unsigned int> v2 = c2->getSubjectIds();
				for (vector<unsigned int>::iterator i = v1.begin(); i != v1.end(); ++i)
				{
					bool found = false;
					for (vector<unsigned int>::iterator j = v2.begin(); j != v2.end(); ++j)
					{
						if (*i == *j)
						{
							found = true;
							break;
						}
					}
					if (!found) return false;
				}
				for (vector<unsigned int>::iterator i = v2.begin(); i != v2.end(); ++i)
				{
					bool found = false;
					for (vector<unsigned int>::iterator j = v1.begin(); j != v1.end(); ++j)
					{
						if (*i == *j)
						{
							found = true;
							break;
						}
					}
					if (!found) return false;
				}
				return true;
			}
			return false;
		}

		vector<Constraint*> ConstraintProcessor::filterOutDummyGroup(std::vector<Constraint*> v)
		{
			vector<Constraint*> result;
			vector<unsigned int> subjects;
			for (vector<Constraint*>::iterator it = v.begin(); it != v.end(); ++it)
			{
				subjects = (*it)->getSubjectIds();
				bool hasDummyGroup = false;
				for (vector<unsigned int>::iterator subit = subjects.begin(); subit != subjects.end(); ++subit)
				{
					if (*subit == DUMMYGROUPID)
					{
						hasDummyGroup = true;
						break;
					}
				}
				if (!hasDummyGroup)
					result.push_back(*it);
			}
			return result;
		}
	}
}