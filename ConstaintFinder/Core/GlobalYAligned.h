#include "SingleConstraint.h"

namespace ConstraintFinder
{
	namespace Core
	{
		namespace Constraints
		{
			/*! \biref Represent alignment with the global X axis.
			* 
			* An entity subject to this constraint will have one of its local axes aligned with the global X axis.
			* \sa Entity, Constraint
			*/
			class GlobalYAligned : public SingleConstraint
			{
			public:
				/*!Defualt constructor*/
				GlobalYAligned();
				/*!Detailed constructor
				* \param e the entitiy subject to this constraint
				*/
				GlobalYAligned(Entity* e);
				string getType() { return "CON_GLOBALYALIGNED"; };
				vector<Constraint*> findInTree(Entity* root, ToleranceSet* ts, SearchPattern sp);
				string toString();
			};
		}
	}
}