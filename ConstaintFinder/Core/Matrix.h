#pragma once
#include "Vector.h"
#include "Frame.h"
#include <GL/glut.h>
#include <cmath>
#include <iostream>
using namespace std;

#define PI 3.14159265

namespace ConstraintFinder
{
	namespace Core
	{
		/*! \brief Represents a 2x2 matrix.
		* \sa Vector2
		*/
		class Matrix22
		{
			friend ostream& operator <<(ostream& os, const Matrix22& m);
		protected:
			/*! Row 0 col 0 entry.*/
			double m00;
			/*! Row 0 col 1 entry.*/
			double m01;
			/*! Row 1 col 0 entry.*/
			double m10;
			/*! Row 1 col 1 entry.*/
			double m11;
		public:
			/*! Default constructor.*/
			Matrix22();
			/*! Detailed constructor.
			* \param m00 row 0 col 0 entry.
			* \param m01 row 0 col 1 entry.
			* \param m10 row 1 col 0 entry.
			* \param m11 row 1 col 1 entry.
			*/
			Matrix22(double m00, double m01, double m10, double m11);
			/*! Gets a specified entry of the matrix.
			* \param i specifies the row.
			* \param j specifies the column.
			* \return the value at row i column j.
			*/
			virtual double getEntry(int i, int j);
			/*! Sets a specified entry of the matrix.
			* \param i specifies the row.
			* \param j specifies the column.
			* \param a the new value at row i column j.
			*/
			virtual void setEntry(int i, int j, double a);

			/*! Returns the sum of this matrix and another 2x2 matrix.
			* \param m the matrix to add to this matrix.
			* \return the sum of this matrix and matrix m.
			*/
			Matrix22 operator + (const Matrix22& m);
			/*! Adds a 2x2 matrix to this matrix.
			* \param m the matrix to add to this matrix.
			*/
			void operator += (const Matrix22& m);
			/*! Returns the difference of this matrix and another 2x2 matrix.
			* \param m the matrix to subtract from this matrix.
			* \return the difference of this matrix and matrix m.
			*/
			Matrix22 operator - (const Matrix22& m);
			/*! Subtracts a 2x2 matrix form this matrix.
			* \param m the matrix to subtract from this matrix.
			*/
			void operator -= (const Matrix22& m);
			/*! Returns the product of this matrix and another 2x2 matrix.
			* \param m the matrix by which to multiply this matrix.
			* \return the product of this matrix and matrix m.
			*/
			Matrix22 operator * (const Matrix22& m);
			/*! Multiplies this matrix by matrix m.
			* \param m the matrix by which to multiply this matrix.
			*/
			void operator *= (const Matrix22& m);

			/*! Returns the 2x2 matrix in a form processible by OpenGL.
			* \return the 2x2 matrix as an array of Glfloat values.
			*/
			virtual GLfloat* toGLfloat16();

			/*! Returns a zero 2x2 matrix.
			* \return the 2x2 matrix with all zero values.
			*/
			static Matrix22 zero();
			/*! Returns an identity 2x2 matrix.
			* \return the identity 2x2 matrix.
			*/
			static Matrix22 identity();
		};

		/*! \brief Represents a 3x3 matrix.
		* \sa Vector3, Matrix22
		*/
		class Matrix33 : public Matrix22
		{
			friend ostream& operator <<(ostream& os, const Matrix33& m);
		protected:
			/*! Row 0 col 2 entry.*/
			double m02;
			/*! Row 1 col 2 entry.*/
			double m12;
			/*! Row 2 col 0 entry.*/
			double m20;
			/*! Row 2 col 1 entry.*/
			double m21;
			/*! Row 2 col 2 entry.*/
			double m22;
		public:
			/*! Default constructor.*/
			Matrix33();
			/*! Detailed constructor.
			* \param m00 row 0 col 0 entry.
			* \param m01 row 0 col 1 entry.
			* \param m02 row 0 col 2 entry.
			* \param m10 row 1 col 0 entry.
			* \param m11 row 1 col 1 entry.
			* \param m12 row 1 col 2 entry.
			* \param m20 row 2 col 0 entry.
			* \param m21 row 2 col 1 entry.
			* \param m22 row 2 col 2 entry.
			*/
			Matrix33(double m00, double m01, double m02, double m10, double m11, double m12, double m20, double m21, double m22);
			/*! Constructs a 3x3 matrix form a 2x2 matrix and the additional row and column.
			* \param m a 2x2 matrix with the entries.
			* \param m02 row 0 col 2 entry.
			* \param m12 row 1 col 2 entry.
			* \param m20 row 2 col 0 entry.
			* \param m21 row 2 col 1 entry.
			* \param m22 row 2 col 2 entry.
			*/
			Matrix33(const Matrix22& m, double m02, double m12, double m20, double m21, double m22);
			double getEntry(int i, int j);
			void setEntry(int i, int j, double a);

			/*! Returns the sum of this matrix and another 3x3 matrix.
			* \param m the matrix to add to this matrix.
			* \return the sum of this matrix and matrix m.
			*/
			Matrix33 operator + (const Matrix33& m);
			/*! Adds a 3x3 matrix to this matrix.
			* \param m the matrix to add to this matrix.
			*/
			void operator +=(const Matrix33& m);
			/*! Returns the difference of this matrix and another 3x3 matrix.
			* \param m the matrix to subtract from this matrix.
			* \return the difference of this matrix and matrix m.
			*/
			Matrix33 operator - (const Matrix33& m);
			/*! Subtracts a 3x3 matrix form this matrix.
			* \param m the matrix to subtract from this matrix.
			*/
			void operator -=(const Matrix33& m);
			/*! Returns the product of this matrix and another 3x3 matrix.
			* \param m the matrix by which to multiply this matrix.
			* \return the product of this matrix and matrix m.
			*/
			Matrix33 operator * (const Matrix33& m);
			/*! Multiplies this matrix by matrix m.
			* \param m the matrix by which to multiply this matrix.
			*/
			void operator *= (const Matrix33& m);

			GLfloat* toGLfloat16();

			/*! Returns a zero 3x3 matrix.
			* \return the 3x3 matrix with all zero values.
			*/
			static Matrix33 zero();
			/*! Returns an identity 3x3 matrix.
			* \return the identity 3x3 matrix.
			*/
			static Matrix33 identity();
			/*! Returns a rotarion matrix around the X axis.
			* \param angle the angle of rotation in radians.
			* \return the 3x3 rotation matrix by angle around the x axis.
			*/
			static Matrix33 rotationX(double angle);
			/*! Returns a rotarion matrix around the Y axis.
			* \param angle the angle of rotation in radians.
			* \return the 3x3 rotation matrix by angle around the y axis.
			*/
			static Matrix33 rotationY(double angle);
			/*! Returns a rotarion matrix around the Z axis.
			* \param angle the angle of rotation in radians.
			* \return the 3x3 rotation matrix by angle around the z axis.
			*/
			static Matrix33 rotationZ(double angle);
		};

		//class Matrix44 : public Matrix33
		//{
		//protected:
		//	double a03;
		//	double a13;
		//	double a23;
		//	double a30;
		//	double a31;
		//	double a32;
		//	double a33;
		//public:
		//	Matrix44();
		//	Matrix44(double m00, double m01, double m02, double m03, 
		//		double m10, double m11, double m12, double m13, 
		//		double m20, double m21, double m22, double m23, 
		//		double m30, double m31, double m32, double m33);
		//	Matrix44(const Matrix33& m, double m03, double m13, double m23, double m30, double m31, double m32, double m33);
		//};

		/*! Stream operator for the Matrix22 class.
		* \param os output stream.
		* \param m the matrix to be sent to output.
		*/
		ostream& operator <<(ostream& os, const Matrix22& m);
		/*! Stream operator for the Matrix33 class.
		* \param os output stream
		* \param m the matrix to be sent to output
		*/
		ostream& operator <<(ostream& os, const Matrix33& m);

		/*! Returns a product of matrix m and vector v.
		* \param m the matrix by which to multiply vector v.
		* \param v the vector to be transformed by matrix m.
		* \return the product m * v.
		*/
		Vector3 operator * (Matrix33& m, Vector3& v);
		/*! Returns a product of vector v and matrix m.
		* \param m the matrix by which to multiply vector v.
		* \param v the vector to be transformed by matrix m.
		* \return the product v * m.
		*/
		Vector3 operator * (Vector3& v, Matrix33& m);

		/*! Converts degrees to radians.
		* \param deg the value in degrees.
		* \return the value in radians.
		*/
		double DegToRadian(double deg);
		/*! Converts radians to degrees.
		* \param rad the value in radians.
		* \return the value in degrees.
		*/
		double RadianToDeg(double rad);

		double distancePointLine(Vector3& p, Vector3& lp, Vector3& ln);

		double distancePointPoint(Vector3& p1, Vector3& p2);
	}
}