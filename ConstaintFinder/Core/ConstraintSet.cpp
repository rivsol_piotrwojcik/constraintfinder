#include "ConstraintSet.h"

namespace ConstraintFinder
{
	namespace Core
	{
		ConstraintSet::ConstraintSet()
		{
			this->initialize();
		}

		vector<Constraint*> ConstraintSet::getEmptyInstances()
		{
			return this->instances;
		}

		ConstraintSet* ConstraintSet::defualtConstraintSet()
		{
			return new ConstraintSet();
		}

		bool ConstraintSet::getConstraint(int index)
		{
			return instances[index] != NULL;
		}

		void ConstraintSet::setConstraint(bool enabled, int index, Constraint* nullInstance)
		{
			if (enabled && !instances[index])
				instances[index] = nullInstance;
			if (!enabled && instances[index])
			{
				Constraint* del = instances[index];
				instances[index] = NULL;
				delete del;
			}
		}

		void ConstraintSet::initialize()
		{
			this->instances.push_back(new GlobalXAligned());  //index 0 - global X aligned 
			this->instances.push_back(new GlobalYAligned());  //index 1 - global Y aligned 
			this->instances.push_back(new GlobalZAligned());  //index 2 - global Z aligned 
			this->instances.push_back(new Parallel());        //index 3 - parallel
			this->instances.push_back(new CoAxial());         //index 4 - coaxial
			this->instances.push_back(new GlobalXCentered()); //index 5 - global X centered
			this->instances.push_back(new GlobalYCentered()); //index 6 - global Y centered
			this->instances.push_back(new GlobalZCentered()); //index 7 - global Z centered
			this->instances.push_back(new CoCentral());       //index 8 - cocentral
			this->instances.push_back(new Aligned());         //index 9 - aligned
			this->instances.push_back(new RectDistributed()); //index 10 - rect distributed
		}

		void ConstraintSet::setGlobalXAligned(bool enabled)
		{
			setConstraint(enabled, 0, new GlobalXAligned());
		}
		bool ConstraintSet::getGlobalXAligned()
		{
			return getConstraint(0);
		}

		void ConstraintSet::setGlobalYAligned(bool enabled)
		{
			setConstraint(enabled, 1, new GlobalYAligned());
		}
		bool ConstraintSet::getGlobalYAligned()
		{
			return getConstraint(1);
		}

		void ConstraintSet::setGlobalZAligned(bool enabled)
		{
			setConstraint(enabled, 2, new GlobalZAligned());
		}
		bool ConstraintSet::getGlobalZAligned()
		{
			return getConstraint(2);
		}

		void ConstraintSet::setParallel(bool enabled)
		{
			setConstraint(enabled, 3, new Parallel());
		}
		bool ConstraintSet::getParallel()
		{
			return getConstraint(3);
		}

		void ConstraintSet::setCoAxial(bool enabled)
		{
			setConstraint(enabled, 4, new Parallel());
		}
		bool ConstraintSet::getCoAxial()
		{
			return getConstraint(4);
		}

		void ConstraintSet::setGlobalXCentered(bool enabled)
		{
			setConstraint(enabled, 5, new Parallel());
		}
		bool ConstraintSet::getGlobalXCentered()
		{
			return getConstraint(5);
		}

		void ConstraintSet::setGlobalYCentered(bool enabled)
		{
			setConstraint(enabled, 6, new Parallel());
		}
		bool ConstraintSet::getGlobalYCentered()
		{
			return getConstraint(6);
		}

		void ConstraintSet::setGlobalZCentered(bool enabled)
		{
			setConstraint(enabled, 7, new Parallel());
		}
		bool ConstraintSet::getGlobalZCentered()
		{
			return getConstraint(7);
		}

		void ConstraintSet::setCoCentral(bool enabled)
		{
			setConstraint(enabled, 8, new Parallel());
		}
		bool ConstraintSet::getCoCentral()
		{
			return getConstraint(8);
		}

		void ConstraintSet::setAligned(bool enabled)
		{
			setConstraint(enabled, 9, new Parallel());
		}
		bool ConstraintSet::getAligned()
		{
			return getConstraint(9);
		}

		void ConstraintSet::setRectDistributed(bool enabled)
		{
			setConstraint(enabled, 10, new Parallel());
		}
		bool ConstraintSet::getRectDistributed()
		{
			return getConstraint(10);
		}

		string ConstraintSet::toString()
		{
			string s;
			s += "--Enabled Constraints--\n";
			if (getGlobalXAligned())
				s += "Global X aligned (1)\n";
			if (getGlobalYAligned())
				s += "Global Y aligned (1)\n";
			if (getGlobalZAligned())
				s += "Global Z aligned (1)\n";
			if (getParallel())
				s += "Parallel (2)\n";
			if (getCoAxial())
				s += "Coaxial (2)\n";
			if (getGlobalXCentered())
				s += "Global X centered (1)\n";
			if (getGlobalYCentered())
				s += "Global Y centered (1)\n";
			if (getGlobalZCentered())
				s += "Global Z centered (1)\n";
			if (getCoCentral())
				s += "Cocentral (2)\n";
			if (getAligned())
				s += "Aligned (3+)\n";
			if (getRectDistributed())
				s += "Distributed on rectangle (4)\n";
			s += "---------------------------------------\n";
			return s;
		}
	}
}