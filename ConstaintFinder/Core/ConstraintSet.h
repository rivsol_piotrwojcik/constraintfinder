#include "SingleConstraint.h"
#include "MultiConstraint.h"
#include "GlobalXAligned.h"
#include "GlobalYAligned.h"
#include "GlobalZAligned.h"
#include "Parallel.h"
#include "CoAxial.h"
#include "GlobalXCentered.h"
#include "GlobalYCentered.h"
#include "GlobalZCentered.h"
#include "CoCentral.h"
#include "Aligned.h"
#include "RectDistributed.h"
using namespace ConstraintFinder::Core::Constraints;

namespace ConstraintFinder
{
	namespace Core
	{
		/*! \brief Represents a set of active constraints.
		* The algorithms of constraints can be executed without any knowledged of their type using empty instances.
		* This class is a wrapper around a list of empty instances, used ot execute constraints. If a constraint is active,
		* the getEmptyInstances() function will return it's empty instance at the proper index, 
		* in case a constraint is inactive NULL will be returned at the proper index.
		*/
		class ConstraintSet
		{
		protected:
			/*!The constraint instances vector.*/
			vector<Constraint*> instances;
			/*!Initializes the constraint set with proper values. Used in constructor.*/
			void initialize();
			/*!Gets the state of a constraint by its index
			* \param index the index of the constraint
			* \return the state of the constraint at given index
			*/
			bool getConstraint(int index);
			/*!Setst he state of a constraint by its index
			* \param enabled true if the constraint is to be enabled. False if it is to be disabled
			* \param index the index of the constraint
			* \param the null instance of the constraint in case needed
			*/
			void setConstraint(bool enabled, int index, Constraint* nullInstance);
		public:
			/*!Defualt constructor.*/
			ConstraintSet();
			/*!Returns the vector of empty constraint instances.
			* \return the vector of empty constraint instances
			*/
			vector<Constraint*> getEmptyInstances();
			/*!Returns the default constraint set
			* \return the default constraint set.
			*/
			static ConstraintSet* defualtConstraintSet();
			
			/*!Gets the info on enabled constraints as a string
			* \return info on enabled constranits
			*/
			string toString();

			/*!Enables or disables the global X aligned constraint.
			* \param enabled true if the constraint is to be enabled. False if it is to be disabled
			*/
			void setGlobalXAligned(bool enabled);
			/*!Gets the state of the global X aligned constraint.
			* \return true if the constraint is enabled, false otherwise
			*/
			bool getGlobalXAligned();

			/*!Enables or disables the global Y aligned constraint.
			* \param enabled true if the constraint is to be enabled. False if it is to be disabled
			*/
			void setGlobalYAligned(bool enabled);
			/*!Gets the state of the global Y aligned constraint.
			* \return true if the constraint is enabled, false otherwise
			*/
			bool getGlobalYAligned();

			/*!Enables or disables the global Z aligned constraint.
			* \param enabled true if the constraint is to be enabled. False if it is to be disabled
			*/
			void setGlobalZAligned(bool enabled);
			/*!Gets the state of the global Z aligned constraint.
			* \return true if the constraint is enabled, false otherwise
			*/
			bool getGlobalZAligned();

			/*!Enables or disables the parallel constraint.
			* \param enabled true if the constraint is to be enabled. False if it is to be disabled
			*/
			void setParallel(bool enabled);
			/*!Gets the state of the parallel constraint.
			* \return true if the constraint is enabled, false otherwise
			*/
			bool getParallel();

			/*!Enables or disables the coaxial constraint.
			* \param enabled true if the constraint is to be enabled. False if it is to be disabled
			*/
			void setCoAxial(bool enabled);
			/*!Gets the state of the coaxial constraint.
			* \return true if the constraint is enabled, false otherwise
			*/
			bool getCoAxial();

			/*!Enables or disables the global X centered constraint.
			* \param enabled true if the constraint is to be enabled. False if it is to be disabled
			*/
			void setGlobalXCentered(bool enabled);
			/*!Gets the state of the global X centered constraint.
			* \return true if the constraint is enabled, false otherwise
			*/
			bool getGlobalXCentered();

			/*!Enables or disables the global Y centered constraint.
			* \param enabled true if the constraint is to be enabled. False if it is to be disabled
			*/
			void setGlobalYCentered(bool enabled);
			/*!Gets the state of the global Y centered constraint.
			* \return true if the constraint is enabled, false otherwise
			*/
			bool getGlobalYCentered();

			/*!Enables or disables the global Z centered constraint.
			* \param enabled true if the constraint is to be enabled. False if it is to be disabled
			*/
			void setGlobalZCentered(bool enabled);
			/*!Gets the state of the global Z centered constraint.
			* \return true if the constraint is enabled, false otherwise
			*/
			bool getGlobalZCentered();

			/*!Enables or disables the cocentral constraint.
			* \param enabled true if the constraint is to be enabled. False if it is to be disabled
			*/
			void setCoCentral(bool enabled);
			/*!Gets the state of the cocentral constraint.
			* \return true if the constraint is enabled, false otherwise
			*/
			bool getCoCentral();

			/*!Enables or disables the aligned constraint.
			* \param enabled true if the constraint is to be enabled. False if it is to be disabled
			*/
			void setAligned(bool enabled);
			/*!Gets the state of the aligned constraint.
			* \return true if the constraint is enabled, false otherwise
			*/
			bool getAligned();

			/*!Enables or disables the distributed on rectangle constraint.
			* \param enabled true if the constraint is to be enabled. False if it is to be disabled
			*/
			void setRectDistributed(bool enabled);
			/*!Gets the state of the distributed on rectangle constraint.
			* \return true if the constraint is enabled, false otherwise
			*/
			bool getRectDistributed();
		};
	}
}