#include "RectDistributed.h"

namespace ConstraintFinder
{
	namespace Core
	{
		namespace Constraints
		{
			RectDistributed::RectDistributed()
				:MultiConstraint(-1)
			{
			}

			RectDistributed::RectDistributed(unsigned int id, vector<Entity*> ents)
				:MultiConstraint(id, ents)
			{
			}

			string RectDistributed::toString()
			{
				string s = "[Distributed on rectangle] for entities: ";
				for (vector<Entity*>::iterator it = entities.begin(); it != entities.end(); ++it)
				{
					s += (*it)->getType();
					s += " ";
				}
				s += "with ids: ";
				char* buf = new char[32];
				for (vector<Entity*>::iterator it = entities.begin(); it != entities.end(); ++it)
				{
					_itoa_s((*it)->getId(), buf, 32, 10);
					s += string(buf);
					s += " ";
				}
				s += "; Group  id: ";
				_itoa_s(getId(), buf, 32, 10);
				s += string(buf);
				delete[] buf;
				return s;
			}
			
			vector<Constraint*> RectDistributed::findInTree(Entity* root, ToleranceSet* ts, SearchPattern sp)
			{
				vector<Constraint*> result;
				vector<Entity*> entityList;
				vector<Entity*> temp;
				string rootType = root->getType();

				entityList = root->toEntityList(sp);
				for(vector<Entity*>::iterator s1 = entityList.begin(); s1 != entityList.end(); ++s1)
				{
					for(vector<Entity*>::iterator s2 = entityList.begin(); s2 != entityList.end(); ++s2)
					{
						if (*s1 == *s2 || (*s2)->getId() == DUMMYGROUPID)
							continue;
						temp.clear();
						temp.push_back(*s1);
						temp.push_back(*s2);
						Vector3 l1 = (*s2)->getCenter() - (*s1)->getCenter();
						vector<Entity*> candidates;
						for(vector<Entity*>::iterator c = entityList.begin(); c != entityList.end(); ++c)
						{
							if (*c == *s1 || *c == *s2 || (*c)->getId() == DUMMYGROUPID)
								continue;
							if (Vector3::Dot( (*c)->getCenter() - (*s1)->getCenter(), l1 ) <= ts->orthogonalityDeviation)
								candidates.push_back(*c);
						}
						for(vector<Entity*>::iterator c1 = candidates.begin(); c1 != candidates.end(); ++c1)
						{
							Vector3 l2 = (*c1)->getCenter() - (*s1)->getCenter();
							for(vector<Entity*>::iterator c2 = entityList.begin(); c2 != entityList.end(); ++c2)
							{
								if (*c2 == *s1 || *c2 == *s2 || *c2 == *c1 || (*c2)->getId() == DUMMYGROUPID)
									continue;
								if (abs(Vector3::Dot( (*c2)->getCenter() - (*s2)->getCenter(), l1 )) <= ts->orthogonalityDeviation
									&& abs(Vector3::Dot( (*c2)->getCenter() - (*c1)->getCenter(), l2 )) <= ts->orthogonalityDeviation)
								{
									vector<Entity*> final;
									final.push_back(*s1);
									final.push_back(*s2);
									final.push_back(*c1);
									final.push_back(*c2);
									result.push_back(new RectDistributed(getNextId(), final));
								}
							}
						}
					}
				}
				return result;
			}
		}
	}
}