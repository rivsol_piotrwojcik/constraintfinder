#include "MultiConstraint.h"

namespace ConstraintFinder
{
	namespace Core
	{
		namespace Constraints
		{
			class CoAxial : public MultiConstraint
			{
			public:
				CoAxial();
				CoAxial(unsigned int id, Entity* e1, Entity* e2);
				string getType() { return "CON_COAXIAL"; };
				vector<Constraint*> findInTree(Entity* root, ToleranceSet* ts, SearchPattern sp);
				string toString();
			};
		}
	}
}