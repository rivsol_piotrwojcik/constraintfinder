#include "Part.h"

namespace ConstraintFinder
{
	namespace Core
	{
		Part::Part(unsigned int id, Entity** objects, unsigned int obj_count)
			:EntityGroup(id)
		{
			for (int i=0; i<(int)obj_count; ++i)
			{
				string type = objects[i]->getType();
				if (type == "ET_PART")
					throw exception("Part::constructor -> Part objects are not allowed as a subset of Part.");
				else if (type.find("ET_CON_") == 0)
					throw exception("Part::constructor -> Constraint objects are not allowed as a subset of Part.");
				else if (type == "ET_CSG")
					loadCsgTree(dynamic_cast<CsgNode*>(objects[i]));
				else if (type == "ET_GROUP")
					loadEntityGroup(dynamic_cast<EntityGroup*>(objects[i]), NULL);
				else
					loadBaseShape((Entity*)objects[i]);
			}
			computeLocalFrame();
		}

		Part::Part(unsigned int id, vector<Entity*> objects)
			:EntityGroup(id)
		{
			for (vector<Entity*>::iterator it = objects.begin(); it != objects.end(); ++it)
			{
				string type = (*it)->getType();
				if (type == "ET_PART")
					throw exception("Part::constructor -> Part objects are not allowed as a subset of Part.");
				else if (type.find("ET_CON_") == 0)
					throw exception("Part::constructor -> Constraint objects are not allowed as a subset of Part.");
				else if (type == "ET_CSG")
					loadCsgTree(dynamic_cast<CsgNode*>(*it));
				else if (type == "ET_GROUP")
					loadEntityGroup(dynamic_cast<EntityGroup*>(*it), NULL);
				else
					loadBaseShape(*it);
			}
			computeLocalFrame();
		}

		Part::~Part()
		{
			//TODO:
		}

		void Part::computeLocalFrame()
		{
			EntityGroup::computeLocalFrame();
		}

		void Part::loadCsgTree(CsgNode* root)
		{
			entities.push_back((Entity*)root);
			computeLocalFrame();
		}

		void Part::loadBaseShape(Entity* bs)
		{
			entities.push_back(bs);
			computeLocalFrame();
		}

		void Part::loadEntityGroup(EntityGroup* g, EntityGroup* parentGroup)
		{
			if (!parentGroup)  
			{
				entities.push_back(g);
				computeLocalFrame();
			}
			for (vector<Entity*>::iterator it = g->getEntities().begin(); it != g->getEntities().end(); ++it)
			{
				string type = (*it)->getType();
				if (type == "ET_PART")
					throw exception("Part::constructor -> Part objects are not allowed as a subset of Part.");
				else if (type.find("ET_CON_") == 0)
					throw exception("Part::constructor -> Constraint objects are not allowed as a subset of Part.");
				else if (type == "ET_CSG")
					loadCsgTree(dynamic_cast<CsgNode*>(*it));
				else if (type == "ET_GROUP")
					loadEntityGroup(dynamic_cast<EntityGroup*>(*it), g);
				else
					loadBaseShape(*it);
			}
		}

		void Part::detectConstraints()
		{
			cout<<"Clearing previous constraints... ";
			this->constraints.clear();
			cout<<"done.\n";
			this->constraints = ConstraintProcessor::detectConstraints(this);
			for (int i=0; i<(int)constraints.size(); ++i)
			{
				cout<<constraints[i]<<"\n";
			}
		}
	}
}