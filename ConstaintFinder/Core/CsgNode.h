#pragma once
#include "Entity.h"
#define CSGNODE_ORTHOGONAL_TOLERANCE 0.0001

namespace ConstraintFinder
{
	namespace Core
	{
		/*! Represents the possible operations between nodes of the CsgNode class.
		*\sa CsgNode
		*/
		typedef enum CSG_Operation { CSG_Union, CSG_Intersection, CSG_Difference, CSG_None };

		/*!
		* \brief This class represents the node of a CSG tree.
		*
		* The class extends the bacis Entity class for all displayable entities.
		* The allowed operations for CSG nodes are: Union Intersection Difference
		* \sa Entity, CSG_Operation
		*/
		class CsgNode : public Entity
		{
		protected:
			/*! Left child of the CSG node*/
			Entity* left;
			/*! Right child of the CSG node*/
			Entity* right;
			/*! Operation corresponding to this CSG node*/
			CSG_Operation op;

			void computeLocalFrame();
		public:
			/*! Default constructor. 
			* \param id a unique identifier assigned to all entities.
			*/
			CsgNode(unsigned int id);
			/*! Constructor
			* \param id a unique identifier assigned to all entities.
			* \param left the left child of the new CSG node.
			* \param right the right child of the new CSG node.
			* \param op the operation corresponding to the new CSG node.
			*/
			CsgNode(unsigned int id, Entity* left, Entity* right, CSG_Operation op);
			/*! Constructor
			* \param id a unique identifier assigned to all entities.
			* \param left the left child of the new CSG node.
			* \param right the right child of the new CSG node.
			* \param op the operation corresponding to the new CSG node.
			* \param center the center of the solid represented by the new CSG node.
			* \param localframe the local coordinate frame of the solid represented by the new CSG node.
			*/
			CsgNode(unsigned int id, Entity* left, Entity* right, CSG_Operation op, const Vector3& center, const Frame& localframe);
			/*! Destructor.*/
			~CsgNode();
			string getType() { return "ET_CSG"; };
			/*! Gets the left child of the CSG node. 
			* \return the left child of the CSG node.
			*/
			Entity* getLeftChild();
			/*! Gets the right child of the CSG node.
			* \return the right child of the CSG node.
			*/
			Entity* getRightChild();
			/*! Gets the operation represented by this node.
			* \return the operation represented by this node.
			*/
			CSG_Operation getOperation();

			string toString();

			vector<Entity*> toEntityList(SearchPattern sp);
		};
	}
}