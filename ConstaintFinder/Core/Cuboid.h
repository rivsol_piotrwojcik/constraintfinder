#pragma once
#include "Entity.h"

namespace ConstraintFinder
{
	namespace Core
	{
		/*! \biref A local axis aligned cuboid.
		* 
		* Represents a cuboid base shape with extents eX, eY, eZ along its local coordinate axes.
		* \sa Entity
		*/
		class Cuboid : public Entity
		{
		protected:
			/*! Extent along the local X axis.*/
			double eX;
			/*! Extent along the local Y axis.*/
			double eY;
			/*! Extent along the local Z axis.*/
			double eZ;
		public:
			/*! Default constructor.
			* \param id the unique id assigend to each entity
			* \param eX extent along the local X axis.
			* \param eY extent along the local Y axis.
			* \param eZ extent along the local Z axis.
			*/
			Cuboid(unsigned int id, double eX, double eY, double eZ);
			string getType() { return "ET_CUBOID"; };
			/*! Returns the x extent.
			* \return X extent
			*/
			double getEX();
			/*! Returns the y extent.
			* \return Y extent
			*/
			double getEY();
			/*! Returns the z extent.
			* \return Z extent
			*/
			double getEZ();

			vector<Entity*> toEntityList(SearchPattern sp);
		};
	}
}