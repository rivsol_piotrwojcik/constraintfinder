#include "MultiConstraint.h"

namespace ConstraintFinder
{
	namespace Core
	{
		namespace Constraints
		{
			class RectDistributed : public MultiConstraint
			{
			public:
				RectDistributed();
				RectDistributed(unsigned int id, vector<Entity*> ents);
				string getType() { return "CON_RECTDISTRIBUTED"; };
				vector<Constraint*> findInTree(Entity* root, ToleranceSet* ts, SearchPattern sp);
				string toString();
			};
		}
	}
}