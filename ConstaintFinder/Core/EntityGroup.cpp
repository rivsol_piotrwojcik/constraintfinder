#include "EntityGroup.h"

namespace ConstraintFinder
{
	namespace Core
	{
		EntityGroup::EntityGroup(unsigned int id)
			:Entity(id)
		{
			entities = vector<Entity*>();
		}

		EntityGroup::EntityGroup(unsigned int id, vector<Entity*> entities)
			:Entity(id)
		{
			this->entities = entities;
			this->computeLocalFrame();
		}

		EntityGroup::EntityGroup(unsigned int id, 
			vector<Entity*> entities,
			const Vector3& translation,
			const Matrix33& rotation,
			const Vector3& scaling)
			:Entity(id, translation, rotation, scaling)
		{
			this->entities = entities;
			this->computeLocalFrame();
		}

		EntityGroup::EntityGroup(unsigned int id, 
			vector<Entity*> entities, 
			const Vector3& translation,
			const Matrix33& rotation,
			const Vector3& scaling,
			const Vector3& center, 
			const Frame& lcoalFrame)
			:Entity(id, translation, rotation, scaling)
		{
			this->entities = entities;
			this->center = center;
			this->localFrame = localFrame;
		}

		Entity* EntityGroup::operator [](unsigned int i)
		{
			return entities[i];
		}

		vector<Entity*> EntityGroup::getEntities()
		{
			return entities;
		}

		int EntityGroup::size()
		{
			return static_cast<int>(entities.size());
		}

		void EntityGroup::computeLocalFrame()
		{
			Vector3 sum = Vector3::zero();
			for (vector<Entity*>::iterator it = entities.begin(); it != entities.end(); ++it)
				sum += (*it)->getCenter();
			sum /= entities.size();
			this->center = sum + this->translation;

			//TODO compute local coord frame
		}

		vector<Entity*> EntityGroup::getCsgMembers()
		{
			vector<Entity*> result;
			for (vector<Entity*>::iterator it = entities.begin(); it != entities.end(); ++it)
			{
				if ((*it)->getType() == "ET_GROUP")
				{
					vector<Entity*> v = dynamic_cast<EntityGroup*>(*it)->getCsgMembers();
					result.insert(result.begin(), v.begin(), v.end());
				}
				else if ((*it)->getType() == "ET_CSG")
					result.push_back(*it);
			}
			return result;
		}

		string EntityGroup::toString()
		{
			string s;
			s = Entity::toString();
			s += "---Members---\n";
			for (vector<Entity*>::iterator it = entities.begin(); it != entities.end(); ++it)
			{
				s += "\t";
				s += (*it)->toString();
				s += "\n";
			}
			return s;
		}

		vector<Entity*> EntityGroup::toEntityList(SearchPattern sp)
		{
			vector<Entity*> result;
			vector<Entity*> temp;
			result.push_back(this);
			if (sp == SP_PYRAMID && getId() != DUMMYGROUPID)
				return result;
			for (vector<Entity*>::iterator it = entities.begin(); it != entities.end(); ++it)
			{
				temp = (*it)->toEntityList(sp);
				result.insert(result.end(), temp.begin(), temp.end());
			}
			return result;
		}

		void EntityGroup::append(Entity* e)
		{
			this->entities.push_back(e);
			computeLocalFrame();
		}

		void EntityGroup::append(vector<Entity*> ents)
		{
			this->entities.insert(entities.end(), ents.begin(), ents.end());
		}
	}
}