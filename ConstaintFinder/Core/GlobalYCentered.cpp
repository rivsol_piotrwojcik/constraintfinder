#include "GlobalYCentered.h"

namespace ConstraintFinder
{
	namespace Core
	{
		namespace Constraints
		{
			GlobalYCentered::GlobalYCentered()
				:SingleConstraint(NULL)
			{
			}

			GlobalYCentered::GlobalYCentered(Entity* e)
				:SingleConstraint(e)
			{
			}

			string GlobalYCentered::toString()
			{
				string s = "[Centered on the Y axis] for entity: ";
				s += e->getType();
				s += " with id: ";
				char* buf = new char[32];
				_itoa_s(e->getId(), buf, 32, 10);
				s += string(buf);
				delete[] buf;
				return s;
			}

			bool isCenteredY(Vector3& c, double zdDev)
			{
				return abs(c.getY()) <= zdDev;
			}

			vector<Constraint*> GlobalYCentered::findInTree(Entity* root, ToleranceSet* ts, SearchPattern sp)
			{
				vector<Constraint*> result;
				vector<Entity*> entityList;
				entityList = root->toEntityList(sp);
				for(vector<Entity*>::iterator it = entityList.begin(); it != entityList.end(); ++it)
				{
					if (isCenteredY((*it)->getCenter(), ts->zeroDistanceDeviation))
						result.push_back(new GlobalYCentered(*it));
				}
				return result;
			}
		}
	}
}