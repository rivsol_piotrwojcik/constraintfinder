#include "CsgNode.h"
#include "ConstraintSet.h"

namespace ConstraintFinder
{
	namespace Core
	{
		/*! \biref Controls the constraint detection process.
		* This class serves as the wrapper around the constraint detection algorithm with all of its parameters.
		*/
		class ConstraintProcessor
		{
		protected:
			/*!The tolerance set used for detection.*/
			static ToleranceSet* tolSet;
			/*!The set of constraints detected.*/
			static ConstraintSet* conSet;
			/*!Number of iterations of the detection algoithm.*/
			static int numOfIterations;
			static SearchPattern searchPattern;
			/*!Performs a single iteration of the detection algorithm.
			* \param root the vector of entities serving as input for this iteration.
			* \param detachCSG the detach CSG flag.
			* \return the constraints detected during this iteration.
			*/
			static vector<Constraint*> detectConstranitsIter(EntityGroup* root, SearchPattern sp);
			/*!Returns a concatenation of target and v with the redundant constraints filtered out.
			* \param target the starting vector.
			* \param v the vector to be concatenated. It's elements will be filtered.
			* \return the non-redundant concatenation of target and v.
			*/
			static vector<Constraint*> insertNoRedundant(vector<Constraint*> target, vector<Constraint*> v);
			/*!Fileres out multiple constraints form a vector.
			* \param v the vector to be filtered.
			* \return the mulitple constraints form v.
			*/
			static vector<Entity*> multiConstarintsOnly(vector<Constraint*> v);
			/*!Compares two geometric constarints for equality.
			* Constraints are considered equal iff they are of the same type and their subject entity sets are identical.
			* \param c1 left constraint
			* \param c2 right constraint
			* \return true if c1 = c2
			*/
			static bool Equal(Constraint* c1, Constraint* c2);
			/*! Filters out constraints detected for the dummy group with id -10
			* \param v the vector to be filtered.
			* \return the filtered vector
			*/
			static vector<Constraint*> filterOutDummyGroup(vector<Constraint*> v);
		public:
			/*!Performs initialization of the constraint detection algorithm params.*/
			static void intializeConstarintProcessor();
			/*!Gets the tolerance set used during detection.
			* \return the tolerance set used during detection.
			*/
			static ToleranceSet* getToleranceSet();
			/*!Sets the tolerance set used during detection.
			* \param ts the new tolerance set used during detection.
			*/
			static void setToleranceSet(ToleranceSet* ts);
			/*!Gets the constraint set used during detection.
			* \return the constraint set used during detection.
			*/
			static ConstraintSet* getConstraintSet();
			/*!Sets the constraint set used during detection.
			* \param cs the new constraint set used during detection.
			*/
			static void setConstraintSet(ConstraintSet* cs);
			/*!Gets the number of iteartions of the detection algorithm.
			* \return the number of iteartions of the detection algorithm.
			*/
			static int getNumberOfInterations();
			/*!Sets the number of iteartions of the detection algorithm.
			* 0 means an unbounded number of iterations, in fact the number is bounded by 1024 to avoid infinite loops.
			* \param n the new number of iteartions of the detection algorithm.
			*/
			static void setNumberOfIterations(int n);
			
			static SearchPattern getSearchPattern();
			static void setSearchPattern(SearchPattern sp);

			/*!Detects geometrical constraints within a given entity.
			* \param root the starting point of the search.
			* \return the constraints found in entity root.
			*/
			static vector<Constraint*> detectConstraints(EntityGroup* root);
		};
	}
}