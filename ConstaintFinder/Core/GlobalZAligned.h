#include "SingleConstraint.h"

namespace ConstraintFinder
{
	namespace Core
	{
		namespace Constraints
		{
			/*! \biref Represent alignment with the global X axis.
			* 
			* An entity subject to this constraint will have one of its local axes aligned with the global X axis.
			* The constraint groups all such entities.
			* \sa Entity, Constraint
			*/
			class GlobalZAligned : public SingleConstraint
			{
			public:
				/*!Default constructor*/
				GlobalZAligned();
				/*!Detailed constructor
				* \param e the entitiy subject to this constraint
				*/
				GlobalZAligned(Entity* e);
				string getType() { return "CON_GLOBALZALIGNED"; };
				vector<Constraint*> findInTree(Entity* root, ToleranceSet* ts, SearchPattern sp);
				string toString();
			};
		}
	}
}