#include "InputProcessor.h"

namespace ConstraintFinder
{
	namespace UserInterface
	{
		InputProcessor::InputProcessor()
		{
			oldX = -1;
			oldY = -1;
			LMBpressed = false;
			MMBpressed = false;
			RMBpressed = false;
			command = ">";
		}

		void InputProcessor::mouseDown(int x, int y, unsigned short button)
		{
			switch (button)
			{
			case 0:
				LMBpressed = true;
				oldX = x;
				oldY = y;
				break;
			case 1:
				MMBpressed = true;
				oldX = x;
				oldY = y;
				break;
			case 2:
				RMBpressed = true;
				oldX = x;
				oldY = y;
				break;
			};
		}

		void InputProcessor::mouseUp(int x, int y, unsigned short button)
		{
			switch (button)
			{
			case 0:
				LMBpressed = false;
				break;
			case 1:
				MMBpressed = false;
				break;
			case 2:
				RMBpressed = false;
				break;
			};
		}

		void InputProcessor::mouseMoved(int x, int y)
		{
			deltaX = x - oldX;
			deltaY = y - oldY;
			oldX = x;
			oldY = y;
		}

		void InputProcessor::keyDown(unsigned char k)
		{
			if (static_cast<int>(k) == 13)
			{
				this->executeCommand(command);
				this->command = ">";
			}
			else if (static_cast<int>(k) == 8)
			{
				if (this->command != ">")
					this->command = this->command.substr(0, command.size() - 1);
			}
			else
			{
				command += k;
			}
		}

		void InputProcessor::keyUp(unsigned char k)
		{
		}

		void InputProcessor::keyPress(unsigned char k)
		{
		}

		string InputProcessor::getCommandStr()
		{
			return this->command;
		}

		void InputProcessor::executeCommand(string command)
		{
		}

		bool InputProcessor::getLMBpressed()
		{
			return LMBpressed;
		}

		bool InputProcessor::getMMBpressed()
		{
			return MMBpressed;
		}

		bool InputProcessor::getRMBpressed()
		{
			return RMBpressed;
		}
	}
}