#include <GL/glew.h>
#include <GL/glut.h>
#include "OpenCSGtest.h"
using namespace ConstraintFinder::UserInterface;

OpenCSG::DisplayListPrimitive::DisplayListPrimitive(unsigned int i, OpenCSG::Operation o, unsigned int c)
: OpenCSG::Primitive(o, c)
{
	mDisplayListId = i;
}

void OpenCSG::DisplayListPrimitive::setDisplayListId(unsigned int i) {
	mDisplayListId = i;
}

unsigned int OpenCSG::DisplayListPrimitive::getDisplayListId() const {
	return mDisplayListId;
}

void OpenCSG::DisplayListPrimitive::render() {
	glCallList(mDisplayListId);
}

void ConstraintFinder::UserInterface::setBasicShape(vector<OpenCSG::Primitive*>* primitives) {

    GLuint id1 = glGenLists(1);
    glNewList(id1, GL_COMPILE);
    glPushMatrix();
    glTranslatef(-0.25, 0.0, 0.0);
    glutSolidSphere(1.0, 20, 20);
    glPopMatrix();
    glEndList();

    GLuint id2 = glGenLists(1);
    glNewList(id2, GL_COMPILE);
    glPushMatrix();
    glTranslatef(0.25, 0.0, 0.0);
    glutSolidSphere(1.0, 20, 20);
    glPopMatrix();
    glEndList();

    GLuint id3 = glGenLists(1);
    glNewList(id3, GL_COMPILE);
    glPushMatrix();
    glTranslatef(0.0, 0.0, 0.5);
    glScalef(0.5, 0.5, 2.0);
    glutSolidSphere(1.0, 20, 20);
    glPopMatrix();
    glEndList();

    primitives->push_back(new OpenCSG::DisplayListPrimitive(id1, OpenCSG::Intersection, 1));
    primitives->push_back(new OpenCSG::DisplayListPrimitive(id2, OpenCSG::Intersection, 1));
    primitives->push_back(new OpenCSG::DisplayListPrimitive(id3, OpenCSG::Subtraction, 1));
}