#include "DataFileProcessor.h"

namespace ConstraintFinder
{
	namespace UserInterface
	{
		unsigned int DataFileProcessor::getNextId()
		{
			return IdMaster::getNextId();
		}

		void DataFileProcessor::validateDataFile(TiXmlDocument* datafile)
		{
			TiXmlHandle hDoc(datafile);
			TiXmlHandle hRoot(0);
			TiXmlElement* temp;
			string strTemp;
			vector<string> entityIds;
			//validate Part node
			temp = hDoc.FirstChildElement().Element();
			if (!temp)
				throw std::exception("DataFileProcessor::validateDataFile -> No <Part> element on top found");
			strTemp = temp->ValueStr();
			if ( strTemp != "Part" )
				throw std::exception("DataFileProcessor::validateDataFile -> Top element is not <Part>");
			hRoot = TiXmlHandle(temp);

			//validate Entity nodes
			for (temp = hRoot.FirstChildElement().Element(); temp != NULL; temp = temp->NextSiblingElement())
			{
				strTemp = temp->ValueStr();
				if (strTemp != "Entity")
					throw std::exception("DataFileProcessor::validateDataFile -> Part subnode other than <Entity>");
				if (!hasUniqueAttribute(temp, "id"))
					throw std::exception("DataFileProcessor::validateDataFile -> Entity should contain unique id");
				if (idOnList(string(temp->Attribute("id")), entityIds))
					throw std::exception("DataFileProcessor::validateDataFile -> Entity id ambiguous");
				entityIds.push_back(temp->Attribute("id"));
				if (!hasUniqueAttribute(temp, "topLevel"))
					throw std::exception("DataFileProcessor::validateDataFile -> Entity should contain unique topLevel flag");
				strTemp = temp->Attribute("topLevel");
				if (!(strTemp == "true" || strTemp == "false"))
					throw std::exception("DataFileProcessor::validateDataFile -> topLevel attribute invalid");
				if (!hasUniqueChild(temp, "definition"))
					throw std::exception("DataFileProcessor::validateDataFile -> Entity should contain a unique definition");
				if (!hasUniqueChild(temp, "translation"))
					throw std::exception("DataFileProcessor::validateDataFile -> Entity should contain a unique translation");
				if (!hasUniqueChild(temp, "rotation"))
					throw std::exception("DataFileProcessor::validateDataFile -> Entity should contain a unique rotation");
				if (!hasUniqueChild(temp, "scaling"))
					throw std::exception("DataFileProcessor::validateDataFile -> Entity should contain a unique scaling");
			}

			for (temp = hRoot.FirstChildElement().Element(); temp != NULL; temp = temp->NextSiblingElement())
			{
				validateDefinition(temp->FirstChildElement("definition"), entityIds);
				validateTransform(temp->FirstChildElement("translation"), 
					temp->FirstChildElement("rotation"),
					temp->FirstChildElement("scaling"));
				if (isGroup(temp))
				{
					for(TiXmlElement* m = temp->FirstChildElement("member"); m != NULL; m = m->NextSiblingElement("member"))
					{
						if (!hasUniqueAttribute(m, "id"))
							throw std::exception("DataFileProcessor::validateDataFile -> group member should have unique id");
						if (!idOnList(string(m->Attribute("id")), entityIds))
							throw std::exception("DataFileProcessor::validateDataFile -> group member - non existing id");
					}
				}
			}
		}

		bool DataFileProcessor::idOnList(string id, vector<string>& ids)
		{
			for (vector<string>::iterator it = ids.begin(); it != ids.end(); ++it)
				if (*it == id) 
					return true;
			return false;
		}

		bool DataFileProcessor::hasChild(TiXmlElement* e, string name)
		{
			return e->FirstChild(name.c_str()) != NULL;
		}

		bool DataFileProcessor::hasUniqueChild(TiXmlElement* e, string name)
		{
			TiXmlNode* n = e->FirstChild(name.c_str());
			if (!n) return false;
			return n->NextSibling(name.c_str()) == NULL;
		}

		bool DataFileProcessor::hasAttribute(TiXmlElement* e, string name)
		{
			for (TiXmlAttribute* a=e->FirstAttribute(); a!= NULL; a=a->Next())
				if (strcmp(a->Name(), name.c_str()) == 0) return true;
			return false;
		}

		bool DataFileProcessor::hasUniqueAttribute(TiXmlElement* e, string name)
		{
			bool found = false;
			for (TiXmlAttribute* a=e->FirstAttribute(); a!= NULL; a=a->Next())
			{
				if (strcmp(a->Name(), name.c_str()) == 0)
				{
					if (found == false)
						found = true;
					else 
						return false;
				}
			}
			return found;
		}

		bool DataFileProcessor::isCuboid(TiXmlElement* e)
		{
			return string(e->FirstChildElement("definition")->Attribute("type")) == "ET_Cuboid";
		}

		bool DataFileProcessor::isEllipsoid(TiXmlElement* e)
		{
			return string(e->FirstChildElement("definition")->Attribute("type")) == "ET_Ellipsoid";
		}

		bool DataFileProcessor::isCylinder(TiXmlElement* e)
		{
			return string(e->FirstChildElement("definition")->Attribute("type")) == "ET_Cylinder";
		}

		bool DataFileProcessor::isCSG(TiXmlElement* e)
		{
			return string(e->FirstChildElement("definition")->Attribute("type")) == "ET_CSG";
		}

		bool DataFileProcessor::isGroup(TiXmlElement* e)
		{
			return string(e->FirstChildElement("definition")->Attribute("type")) == "ET_Group";
		}

		bool DataFileProcessor::isTopLevel(TiXmlElement* e)
		{ 
			return  string(e->Attribute("topLevel")) == "true";
		}

		void DataFileProcessor::validateDefinition(TiXmlElement *definition, vector<string>& ids)
		{
			if (!hasUniqueAttribute(definition, "type"))
				throw std::exception("DataFileProcessor::validateDefinition -> definition should contain a unique object type");
			string type = definition->Attribute("type");
			if (type == "ET_Cuboid")
			{
				if (!hasUniqueAttribute(definition, "eX"))
					throw std::exception("DataFileProcessor::validateDefinition -> cuboid definition should contain a unique X extent");
				if (!hasUniqueAttribute(definition, "eY"))
					throw std::exception("DataFileProcessor::validateDefinition -> cuboid definition should contain a unique Y extent");
				if (!hasUniqueAttribute(definition, "eZ"))
					throw std::exception("DataFileProcessor::validateDefinition -> cuboid definition should contain a unique Z extent");
			}
			else if (type == "ET_Ellipsoid")
			{
				if (!hasUniqueAttribute(definition, "eX"))
					throw std::exception("DataFileProcessor::validateDefinition -> ellipsoid definition should contain a unique X extent");
				if (!hasUniqueAttribute(definition, "eY"))
					throw std::exception("DataFileProcessor::validateDefinition -> ellipsoid definition should contain a unique Y extent");
				if (!hasUniqueAttribute(definition, "eZ"))
					throw std::exception("DataFileProcessor::validateDefinition -> ellipsoid definition should contain a unique Z extent");
			}
			else if (type == "ET_Cylinder")
			{
				if (!hasUniqueAttribute(definition, "radius"))
					throw std::exception("DataFileProcessor::validateDefinition -> cylinder definition should contain a unique radius");
				if (!hasUniqueAttribute(definition, "length"))
					throw std::exception("DataFileProcessor::validateDefinition -> cylinder definition should contain a unique length");
			}
			else if (type == "ET_CSG")
			{
				if (!hasUniqueAttribute(definition, "left"))
					throw std::exception("DataFileProcessor::validateDefinition -> CSG node definition should contain a unique left child");
				if (!hasUniqueAttribute(definition, "right"))
					throw std::exception("DataFileProcessor::validateDefinition -> CSG node definition should contain a unique right child");
				if (!hasUniqueAttribute(definition, "operation"))
					throw std::exception("DataFileProcessor::validateDefinition -> CSG node definition should contain a unique operation");
				string op = definition->Attribute("operation");
				if (!(op == "Intersection" || op == "Difference" || op == "Union"))
					throw std::exception("DataFileProcessor::validateDefinition -> operation invalid");
				string l = definition->Attribute("left");
				if (!idOnList(l, ids))
					throw std::exception("DataFileProcessor::validateDefinition -> CSG node left child - id invalid");
				string r = definition->Attribute("right");
				if (!idOnList(r, ids))
					throw std::exception("DataFileProcessor::validateDefinition -> CSG node left right - id invalid");
			}
			else if (type == "ET_Group")
			{
			}
			else
			{
				throw std::exception("DataFileProcessor::validateDefinition -> invalid object type");
			}
		}

		void DataFileProcessor::validateTransform(TiXmlElement* translate, TiXmlElement* rotate, TiXmlElement* scale)
		{
			if (!hasUniqueAttribute(translate, "tX"))
				throw std::exception("DataFileProcessor::validateTransform -> translation should containt unique X component");
			if (!hasUniqueAttribute(translate, "tY"))
				throw std::exception("DataFileProcessor::validateTransform -> translation should containt unique Y component");
			if (!hasUniqueAttribute(translate, "tZ"))
				throw std::exception("DataFileProcessor::validateTransform -> translation should containt unique Z component");
			if (!hasUniqueAttribute(rotate, "rX"))
				throw std::exception("DataFileProcessor::validateTransform -> rotation should containt unique X component");
			if (!hasUniqueAttribute(rotate, "rY"))
				throw std::exception("DataFileProcessor::validateTransform -> rotation should containt unique Y component");
			if (!hasUniqueAttribute(rotate, "rZ"))
				throw std::exception("DataFileProcessor::validateTransform -> rotation should containt unique Z component");
			if (!hasUniqueAttribute(scale, "sX"))
				throw std::exception("DataFileProcessor::validateTransform -> scaling should containt unique X component");
			if (!hasUniqueAttribute(scale, "sY"))
				throw std::exception("DataFileProcessor::validateTransform -> scaling should containt unique Y component");
			if (!hasUniqueAttribute(scale, "sZ"))
				throw std::exception("DataFileProcessor::validateTransform -> scaling should containt unique Z component");
		}

		ConstraintFinder::Graphics::GLPart* DataFileProcessor::constructPart(const TiXmlHandle& root)
		{
			GLPart* p = new GLPart(getNextId());
			vector<TiXmlElement*> entities;
			for (TiXmlElement* entity = root.FirstChildElement().Element(); entity != NULL; entity = entity->NextSiblingElement())
				entities.push_back(entity);
			for (vector<TiXmlElement*>::iterator it = entities.begin(); it != entities.end(); ++it)
			{
				if (isTopLevel(*it))
				{
					if (isCuboid(*it))
						p->addEntity(loadCuboid(*it));
					else if (isEllipsoid(*it))
						p->addEntity(loadEllipsoid(*it));
					else if (isCylinder(*it))
						p->addEntity(loadCylinder(*it));
					else if (isCSG(*it))
					{
						p->addEntity(loadCSG(*it, entities));
					}
					else if (isGroup(*it))
						p->addEntity(loadGroup(*it, entities));
				}
			}
			return p;
		}

		GLCuboid* DataFileProcessor::loadCuboid(TiXmlElement *e)
		{
			double eX, eY, eZ;
			double tX, tY, tZ, rX, rY, rZ, sX, sY, sZ;
			e->FirstChildElement("definition")->QueryDoubleAttribute("eX", &eX);
			e->FirstChildElement("definition")->QueryDoubleAttribute("eY", &eY);
			e->FirstChildElement("definition")->QueryDoubleAttribute("eZ", &eZ);
			e->FirstChildElement("translation")->QueryDoubleAttribute("tX", &tX);
			e->FirstChildElement("translation")->QueryDoubleAttribute("tY", &tY);
			e->FirstChildElement("translation")->QueryDoubleAttribute("tZ", &tZ);
			e->FirstChildElement("rotation")->QueryDoubleAttribute("rX", &rX);
			e->FirstChildElement("rotation")->QueryDoubleAttribute("rY", &rY);
			e->FirstChildElement("rotation")->QueryDoubleAttribute("rZ", &rZ);
			e->FirstChildElement("scaling")->QueryDoubleAttribute("sX", &sX);
			e->FirstChildElement("scaling")->QueryDoubleAttribute("sY", &sY);
			e->FirstChildElement("scaling")->QueryDoubleAttribute("sZ", &sZ);
			GLCuboid* c = new GLCuboid(getNextId(), eX, eY, eZ);
			c->translate(tX, tY, tZ);
			c->rotateX(DegToRadian(rX));
			c->rotateY(DegToRadian(rY));
			c->rotateZ(DegToRadian(rZ));
			c->scale(sX, sY, sZ);
			return c;
		}

		GLEllipsoid* DataFileProcessor::loadEllipsoid(TiXmlElement *e)
		{
			double eX, eY, eZ;
			double tX, tY, tZ, rX, rY, rZ, sX, sY, sZ;
			e->FirstChildElement("definition")->QueryDoubleAttribute("eX", &eX);
			e->FirstChildElement("definition")->QueryDoubleAttribute("eY", &eY);
			e->FirstChildElement("definition")->QueryDoubleAttribute("eZ", &eZ);
			e->FirstChildElement("translation")->QueryDoubleAttribute("tX", &tX);
			e->FirstChildElement("translation")->QueryDoubleAttribute("tY", &tY);
			e->FirstChildElement("translation")->QueryDoubleAttribute("tZ", &tZ);
			e->FirstChildElement("rotation")->QueryDoubleAttribute("rX", &rX);
			e->FirstChildElement("rotation")->QueryDoubleAttribute("rY", &rY);
			e->FirstChildElement("rotation")->QueryDoubleAttribute("rZ", &rZ);
			e->FirstChildElement("scaling")->QueryDoubleAttribute("sX", &sX);
			e->FirstChildElement("scaling")->QueryDoubleAttribute("sY", &sY);
			e->FirstChildElement("scaling")->QueryDoubleAttribute("sZ", &sZ);
			GLEllipsoid* el = new GLEllipsoid(getNextId(), eX, eY, eZ);
			el->translate(tX, tY, tZ);
			el->rotateX(rX);
			el->rotateY(rY);
			el->rotateZ(rZ);
			el->scale(sX, sY, sZ);
			return el;
		}

		GLCylinder* DataFileProcessor::loadCylinder(TiXmlElement *e)
		{
			double r, l;
			double tX, tY, tZ, rX, rY, rZ, sX, sY, sZ;
			e->FirstChildElement("definition")->QueryDoubleAttribute("radius", &r);
			e->FirstChildElement("definition")->QueryDoubleAttribute("length", &l);
			e->FirstChildElement("translation")->QueryDoubleAttribute("tX", &tX);
			e->FirstChildElement("translation")->QueryDoubleAttribute("tY", &tY);
			e->FirstChildElement("translation")->QueryDoubleAttribute("tZ", &tZ);
			e->FirstChildElement("rotation")->QueryDoubleAttribute("rX", &rX);
			e->FirstChildElement("rotation")->QueryDoubleAttribute("rY", &rY);
			e->FirstChildElement("rotation")->QueryDoubleAttribute("rZ", &rZ);
			e->FirstChildElement("scaling")->QueryDoubleAttribute("sX", &sX);
			e->FirstChildElement("scaling")->QueryDoubleAttribute("sY", &sY);
			e->FirstChildElement("scaling")->QueryDoubleAttribute("sZ", &sZ);
			GLCylinder* c = new GLCylinder(getNextId(), r, l);
			c->translate(tX, tY, tZ);
			c->rotateX(rX);
			c->rotateY(rY);
			c->rotateZ(rZ);
			c->scale(sX, sY, sZ);
			return c;
		}

		GLCsgNode* DataFileProcessor::loadCSG(TiXmlElement* e, vector<TiXmlElement*> entities)
		{
			string l, r, sop;
			CSG_Operation operation;
			GLCsgNode *left, *right;
			l = e->FirstChildElement("definition")->Attribute("left");
			r = e->FirstChildElement("definition")->Attribute("right");
			sop = e->FirstChildElement("definition")->Attribute("operation");
			if (sop == "Union")
				operation = CSG_Union;
			else if (sop == "Intersection")
				operation = CSG_Intersection;
			else if (sop == "Difference")
				operation = CSG_Difference;
			for (vector<TiXmlElement*>::iterator it = entities.begin(); it != entities.end(); ++it)
			{
				if ((*it)->Attribute("id") == l)
				{
					if (isCuboid(*it))
						left = new GLCsgNode(loadCuboid(*it));
					else if (isEllipsoid(*it))
						left  = new GLCsgNode(loadEllipsoid(*it));
					else if (isCylinder(*it))
						left  = new GLCsgNode(loadCylinder(*it));
					else if (isCSG(*it))
						left = loadCSG(*it, entities);
					else
						throw std::exception("DataFileProcessor::loadCSG -> only basic shapes or CSG nodes allowed as children");
				}
				if ((*it)->Attribute("id") == r)
				{
					if (isCuboid(*it))
						right = new GLCsgNode(loadCuboid(*it));
					else if (isEllipsoid(*it))
						right  = new GLCsgNode(loadEllipsoid(*it));
					else if (isCylinder(*it))
						right  = new GLCsgNode(loadCylinder(*it));
					else if (isCSG(*it))
						right = loadCSG(*it, entities);
					else
						throw std::exception("DataFileProcessor::loadCSG -> only basic shapes or CSG nodes allowed as children");
				}
			}
			return new GLCsgNode(IdMaster::getNextId(), left, right, operation);
		}

		GLEntityGroup* DataFileProcessor::loadGroup(TiXmlElement* e, vector<TiXmlElement*> entities)
		{
			GLEntityGroup* g = new GLEntityGroup(getNextId());
			string mid;
			for (TiXmlElement* m = e->FirstChildElement("member"); m != NULL; m = m->NextSiblingElement("member"))
			{
				mid = m->Attribute("id");
				for (vector<TiXmlElement*>::iterator it = entities.begin(); it != entities.end(); ++it)
				{
					if ((*it)->Attribute("id") == mid)
					{
						if (isCuboid(*it))
							g->addEntity(loadCuboid(*it));
						else if (isEllipsoid(*it))
							g->addEntity(loadEllipsoid(*it));
						else if (isCylinder(*it))
							g->addEntity(loadCylinder(*it));
						else if (isCSG(*it))
							g->addEntity(loadCSG(*it, entities));
						else
							g->addEntity(loadGroup(*it, entities));
					}
				}
			}
			return g;
		}

		ConstraintFinder::Graphics::GLPart* DataFileProcessor::loadPartFromFile(string filename)
		{
			TiXmlDocument dataFile( filename.c_str());
			bool loadSuccess = dataFile.LoadFile();
			if (!loadSuccess)
				throw std::exception("DataInputProcessor::loadPartFromFile -> error while opening data file.");
			try
			{
				validateDataFile(&dataFile);
				TiXmlHandle hDoc = TiXmlHandle(&dataFile);
				TiXmlHandle hRoot = TiXmlHandle(hDoc.FirstChildElement().Element());
				return constructPart(hRoot);
			}
			catch (std::exception e)
			{
				cout<<e.what()<<"\n";
				throw std::exception("DataInputProcessor::validateDataFile -> part data file invalid.");
			}
			return NULL;
		}
	}
}