# include "Engine.h"
using namespace ConstraintFinder::UserInterface;

Engine* Engine::pinstance = NULL;
GLPart* Engine::part = NULL;
SphericalCamera Engine::camera = SphericalCamera();
CFInputProcessor* Engine::inputProc = NULL;

unsigned int Engine::getNextId()
{
	return IdMaster::getNextId();
}

Engine* Engine::instance()
{
	if (pinstance == NULL)
		pinstance = new Engine();
	return pinstance;
}

Engine::Engine()
{
	scrWidth = scrHeight = 0;
	aspect = 0;
}

GLCsgNode* sampleCSG()
{
	//GLEllipsoid* e1 = new GLEllipsoid(Engine::getNextId(), 1.5, 1.5, 1.5);
	//e1->translate(-1, 0, -2);
	//GLEllipsoid* e2 = new GLEllipsoid(Engine::getNextId(), 1.5, 1.5, 1.5);
	//e2->translate(1, 0, -2);
	//e2->rotateX(PI/4);
	//e2->rotateY(PI/8);
	//GLCsgNode* n1 = new GLCsgNode(new GLCsgNode(e1), new GLCsgNode(e2), CSG_Union);
	//GLEllipsoid* e3 = new GLEllipsoid(Engine::getNextId(), 1.5, 1.5, 1.5);
	//e3->translate(0, 1, -2);
	//return new GLCsgNode(n1, new GLCsgNode(e3), CSG_Difference);
}

void Engine::initialize(int argc, char **argv, int scrWidth, int scrHeight)
{
	cout<<"Initializing... ";
	glutInit(&argc, argv);
    glutInitWindowSize(scrWidth, scrHeight);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH | GLUT_STENCIL);
    glutCreateWindow("CSG Constraint finder");

	int err = glewInit();
    if (GLEW_OK != err) {
        std::cerr << "GLEW Error: " << glewGetErrorString(err) << std::endl;
    } 

	this->scrWidth = scrWidth;
	this->scrHeight = scrHeight;
	this->aspect = (GLdouble)scrWidth / (GLdouble)scrHeight;

	Engine::inputProc = new CFInputProcessor(this);

    glutDisplayFunc(render);
	glutIdleFunc(idle);
	glutKeyboardFunc(keyboardCallback);
	glutMouseFunc(mouseCallback);
	glutMotionFunc(motionCallback);
	glutPassiveMotionFunc(motionCallback);
	glClearColor(0.0f, 0.7f, 1.0f, 1.0f);

	// Enable two OpenGL lights
    GLfloat light_diffuse[] = {0.0, 0.5, 1.0, 1.0};       // diffuse light
    GLfloat light_position0[] = {-1.0, -1.0, -1.0, 0.0};  // Infinite light location
    GLfloat light_position1[] = {1.0, 1.0, 1.0, 0.0};     // Infinite light location

    glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse);
    glLightfv(GL_LIGHT0, GL_POSITION, light_position0);
    glEnable(GL_LIGHT0);  
    glLightfv(GL_LIGHT1, GL_DIFFUSE, light_diffuse);
    glLightfv(GL_LIGHT1, GL_POSITION, light_position1);
    glEnable(GL_LIGHT1);
    glEnable(GL_LIGHTING);
    glEnable(GL_NORMALIZE);

    // Use depth buffering for hidden surface elimination
    glEnable(GL_DEPTH_TEST);

    glMatrixMode(GL_PROJECTION);
    gluPerspective(40.0, aspect, 1.0, 100.0);
    glMatrixMode(GL_MODELVIEW);
	glViewport(0,0,scrWidth, scrHeight);

	IdMaster::initializeIdMaster();
	ConstraintProcessor::intializeConstarintProcessor();
	cout<<" done\nWelcome to ConstraintFinder.\nUse the command prompt in the main window.\n";
}

void Engine::idle()
{
	render();
}

void Engine::mainloop()
{
	glutMainLoop();
}

void Engine::render()
{
	glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
	camera.applyView();
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
	if (part)
		part->renderGL();
	renderLabel();
	glutSwapBuffers();
}

void Engine::renderLabel() {
    glDisable(GL_DEPTH_TEST);
    glLoadIdentity();
    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();
    glColor3f(0.0, 0.0, 0.0);
    glRasterPos2f(-1.0, -1.0);
    glDisable(GL_LIGHTING);
	std::string s = inputProc->getCommandStr();
    for (unsigned int i=0; i<s.size(); ++i) {
        glutBitmapCharacter(GLUT_BITMAP_8_BY_13, s[i]);
    }
    glEnable(GL_LIGHTING);
    glPopMatrix();
    glMatrixMode(GL_MODELVIEW);
    glEnable(GL_DEPTH_TEST);
}

void Engine::keyboardCallback(unsigned char key, int x, int y)
{
	inputProc->keyDown(key);
}

void Engine::mouseCallback(int button, int state, int x, int y)
{
	switch (button)
	{
	case GLUT_LEFT_BUTTON:
		switch (state)
		{
		case GLUT_DOWN:
			Engine::inputProc->mouseDown(x,y,0);
			break;
		case GLUT_UP:
			Engine::inputProc->mouseUp(x,y,0);
			break;
		}
		break;
	case GLUT_MIDDLE_BUTTON:
		switch (state)
		{
		case GLUT_DOWN:
			Engine::inputProc->mouseDown(x,y,1);
			break;
		case GLUT_UP:
			Engine::inputProc->mouseUp(x,y,1);
			break;
		}
		break;
	case GLUT_RIGHT_BUTTON:
		switch (state)
		{
		case GLUT_DOWN:
			Engine::inputProc->mouseDown(x,y,2);
			break;
		case GLUT_UP:
			Engine::inputProc->mouseUp(x,y,2);
			break;
		}
		break;
	}
}

void Engine::motionCallback(int x, int y)
{
	Engine::inputProc->mouseMoved(x,y);
}

CFInputProcessor::CFInputProcessor(Engine* parent) : InputProcessor()
{
	this->parent = parent;
}

void CFInputProcessor::mouseDown(int x, int y, unsigned short button)
{
	InputProcessor::mouseDown(x,y,button);
}

void CFInputProcessor::mouseUp(int x, int y, unsigned short button)
{
	InputProcessor::mouseUp(x,y,button);
}

void CFInputProcessor::mouseMoved(int x, int y)
{
	InputProcessor::mouseMoved(x,y);
	if (LMBpressed)
	{
		Engine::camera.rotate(0, DegToRadian(deltaY), DegToRadian(-deltaX));
	}
	if (MMBpressed)
	{
		Engine::camera.rotate(deltaY, 0, 0);
	}
}

void CFInputProcessor::executeCommand(string command)
{
	if (command.find(">loadDataFile ") == 0)
	{
		string path = command.substr(14, command.size()-14);
		cout<<"Execute: load Data File at ";
		cout<<path<<"\n";
		parent->loadPartFormFile(path);
	}
	else if(command == ">detect")
	{
		cout<<"Execute: Detect Geometrical Constraints for this part\n";
		parent->detectConstraints();
	}
	else if(command == ">exit")
	{
		//TODO
	}
	else if(command.find(">setIterations ") == 0)
	{
		cout<<"Execute: Set number of iterations\n";
		string iters = command.substr(15, command.size() - 15);
		int n = atoi(iters.c_str());
		ConstraintProcessor::setNumberOfIterations(n);
		cout<<"New number of iterations is: "<<n<<"\n";
	}
	else if(command == ">listDetectionProps")
	{
		cout<<"----Constraint detection properties----\n";
		int n = ConstraintProcessor::getNumberOfInterations();
		if (n!=0)
			cout<<"Number of iterations: "<<n<<"\n";
		else
			cout<<"Number of iterations: unbounded\n";
		cout<<"Search Pattern: "<<ConstraintProcessor::getSearchPattern()<<"\n";
		cout<<ConstraintProcessor::getConstraintSet()->toString();
	}
	else if (command == ">listTolerances")
	{
		cout<<"----Toleraces----\n";
		cout<<"Length deviation: "<<ConstraintProcessor::getToleranceSet()->lengthDeviation<<"\n";
		cout<<"Angular deviation: "<<ConstraintProcessor::getToleranceSet()->angularDeviation<<"\n";
		cout<<"Vector deviation: "<<ConstraintProcessor::getToleranceSet()->vector3DDeviation<<"\n";
		cout<<"Orthgonality deviation: "<<ConstraintProcessor::getToleranceSet()->orthogonalityDeviation<<"\n";
		cout<<"Zero distance deviation: "<<ConstraintProcessor::getToleranceSet()->zeroDistanceDeviation<<"\n";
		cout<<"-----------------\n";
	}
	else if (command == ">listConstraints")
	{
		parent->listConstraints();
	}
	else if (command == ">listEntities")
	{
		parent->listEntities();
	}
	else if (command.find(">selectEntity ") == 0)
	{
		string d = command.substr(14, command.size()-14);
		int dev = atoi(d.c_str());
		cout<<"Execute: Select Entitiy with id: "<<dev<<"\n";
		parent->selectEntity(dev);
	}
	else if (command.find(">selectConstraint ") == 0)
	{
		string d = command.substr(18, command.size()-18);
		int dev = atoi(d.c_str());
		cout<<"Execute: Select Constraint with index: "<<dev<<"\n";
		parent->selectConstraint(dev);
	}
	else if (command == ">clearSelection")
	{
		cout<<"Execute: Clear selection\n";
		parent->clearSelect();
	}
	else if (command.find(">lengthDev ") == 0)
	{
		cout<<"Execute: Set length deviation tolerance\n";
		string d = command.substr(11, command.size()-11);
		double dev = atof(d.c_str());
		ConstraintProcessor::getToleranceSet()->lengthDeviation = dev;
		cout<<"New length deviation tolerance is: "<<dev<<"\n";
	}
	else if (command.find(">angularDev ") == 0)
	{
		cout<<"Execute: Set angular deviation tolerance\n";
		string d = command.substr(12, command.size()-12);
		double dev = atof(d.c_str());
		ConstraintProcessor::getToleranceSet()->angularDeviation = dev;
		cout<<"New angular deviation tolerance is: "<<dev<<"\n";
	}
	else if (command.find(">vectorDev ") == 0)
	{
		cout<<"Execute: Set vector deviation tolerance\n";
		string d = command.substr(11, command.size()-11);
		double dev = atof(d.c_str());
		ConstraintProcessor::getToleranceSet()->vector3DDeviation = dev;
		cout<<"New vector deviation tolerance is: "<<dev<<"\n";
	}
	else if (command.find(">orthoDev ") == 0)
	{
		cout<<"Execute: Set orthonormality deviation tolerance\n";
		string d = command.substr(10, command.size()-10);
		double dev = atof(d.c_str());
		ConstraintProcessor::getToleranceSet()->orthogonalityDeviation = dev;
		cout<<"New orthonormality deviation tolerance is: "<<dev<<"\n";
	}
	else if (command.find(">zeroDistanceDev ") == 0)
	{
		cout<<"Execute: Set zero distance deviation tolerance\n";
		string d = command.substr(17, command.size()-17);
		double dev = atof(d.c_str());
		ConstraintProcessor::getToleranceSet()->zeroDistanceDeviation = dev;
		cout<<"New zero distance deviation tolerance is: "<<dev<<"\n";
	}
	else if (command.find(">globalXAligned ") == 0)
	{
		string s = command.substr(16, command.size()-16);
		if (s == "enable")
		{
			cout<<"Enabling global X aligned constraint\n";
			ConstraintProcessor::getConstraintSet()->setGlobalXAligned(true);
		}
		else if (s == "disable")
		{
			cout<<"Disabling global X aligned constraint\n";
			ConstraintProcessor::getConstraintSet()->setGlobalXAligned(false);
		}
		else
		{
			cout<<"Please use: >globalXAligned [enable|disable]\n";
		}
	}
	else if (command.find(">globalYAligned ") == 0)
	{
		string s = command.substr(16, command.size()-16);
		if (s == "enable")
		{
			cout<<"Enabling global Y aligned constraint\n";
			ConstraintProcessor::getConstraintSet()->setGlobalYAligned(true);
		}
		else if (s == "disable")
		{
			cout<<"Disabling global Y aligned constraint\n";
			ConstraintProcessor::getConstraintSet()->setGlobalYAligned(false);
		}
		else
		{
			cout<<"Please use: >globalYAligned [enable|disable]\n";
		}
	}
	else if (command.find(">globalZAligned ") == 0)
	{
		string s = command.substr(16, command.size()-16);
		if (s == "enable")
		{
			cout<<"Enabling global Z aligned constraint\n";
			ConstraintProcessor::getConstraintSet()->setGlobalZAligned(true);
		}
		else if (s == "disable")
		{
			cout<<"Disabling global Z aligned constraint\n";
			ConstraintProcessor::getConstraintSet()->setGlobalZAligned(false);
		}
		else
		{
			cout<<"Please use: >globalZAligned [enable|disable]\n";
		}
	}
	else if (command.find(">searchPattern ") == 0)
	{
		string s = command.substr(15, command.size()-15);
		if (s == "bruteforce")
		{
			cout<<"Execute: Set search pattern to brute force\n";
			ConstraintProcessor::setSearchPattern(SP_BRUTEFORCE);
		}
		else if (s == "detacgCsg")
		{
			cout<<"Execute: Set search pattern to detach CSG\n";
			ConstraintProcessor::setSearchPattern(SP_DETACHCSG);
		}
		else if (s == "pyramid")
		{
			cout<<"Execute: Set search pattern to pyramid\n";
			ConstraintProcessor::setSearchPattern(SP_PYRAMID);
		}
	}
	else if (command == ">help")
	{
		cout<<"----Commands----\n";
		cout<<"loadDataFile [path] - load a part form xml file\n";
		cout<<"detect - detect geometrical constraints in this part\n";
		cout<<"listDetectionProps - lists the constraint detection algorithm parameters\n";
		cout<<"listTolerances - lists values of all tolerances\n";
		cout<<"listEntities - lists all entities in current part\n";
		cout<<"listConstraints - lists all constraints detected in current part\n";
		cout<<"selectEntity [int+] - selects the desired entity to be the only one displayed\n";
		cout<<"selectConstraint [index] - selects the desired constraint. Only it's subject entities will be displayed.\n";
		cout<<"clearSelection - clear the current selection (displays the entire part)\n";
		cout<<"setIterations [0 - 1024] - set the number of iterations of the constraint detection algorithm\n";
		cout<<"detachCSG [enable|disable] - enable or disable seperate processing of CSG internal structure\n";
		cout<<"lengthDev [double] - sets the length deviation tolerance\n";
		cout<<"angularDev [double] - sets the angular deviation tolerance\n";
		cout<<"vectorDev [double] - sets the vector deviation tolerance\n";
		cout<<"orthoDev [double] - sets the orthogonality deviation tolerance\n";
		cout<<"zeroDistanceDev [double] - sets the zero distance deviation tolerance\n";
		cout<<"globalXAligned [enable|disable] - enable or disable detection of the global x aligned constraint\n";
		cout<<"globalYAligned [enable|disable] - enable or disable detection of the global y aligned constraint\n";
		cout<<"globalZAligned [enable|disable] - enable or disable detection of the global z aligned constraint\n";
		cout<<"echo [text] - display text in console window\n";
		cout<<"help - display this info\n";
		cout<<"about - display app info\n";
		cout<<"exit - quits the application\n";
		cout<<"----------------\n";
	}
	else if (command == ">about")
	{
		cout<<"------------------------------\n";
		cout<<"Constarint Finder v 1.0\n";
		cout<<"Copyright Peter Wojcik 2009\n";
		cout<<"Msc. thesis application.\n";
		cout<<"------------------------------\n";
	}
	else if (command.find(">echo ") == 0)
	{
		string s = command.substr(6, command.size()-6);
		cout<<s<<"\n";
	}
	else
	{
		cout<<"Invalid command! Type help for list\n";
	}

}

void Engine::loadPartFormFile(string path)
{
	try
	{
		GLPart* p2 = DataFileProcessor::loadPartFromFile(path);
		if (part)
			delete part;
		part = p2;
		cout<<"Load successfull!\n";
	}
	catch (std::exception e)
	{
		cout<<e.what();
	}
}

void Engine::detectConstraints()
{
	if (!part)
	{
		cout<<"No part Loaded. Canceled\n";
		return;
	}
	part->detectConstraints();
}

void Engine::listConstraints()
{
	if (!part)
			cout<<"No part loaded. Canceled\n";
	else
		part->listConstraints();
}

void Engine::listEntities()
{
	if (!part)
			cout<<"No part loaded. Canceled\n";
	else
		part->listEntities();
}

void Engine::selectEntity(int id)
{
	if (!part)
		cout<<"No part loaded. Canceled\n";
	else
		part->selectEntity(static_cast<unsigned int>(id));
}

void Engine::selectConstraint(int index)
{
	if (!part)
		cout<<"No part loaded. Canceled\n";
	else
		part->selectConstraint(index);
}

void Engine::clearSelect()
{
	if (!part)
		cout<<"No part loaded. Canceled\n";
	else
		part->clearSelect();
}