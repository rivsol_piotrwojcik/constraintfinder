#pragma once
#include "Matrix.h"
#include <GL/glu.h>
using namespace ConstraintFinder::Core;

namespace ConstraintFinder
{
	namespace UserInterface
	{
		class Camera
		{
		protected:
			Vector3 position;
			Vector3 lookAt;
			Vector3 up;		
		public:
			Camera();
			Camera(const Vector3& pos, const Vector3& lookAt, const Vector3& up);
			virtual void applyView();
			Vector3 getPosition();
			void setPosition(const Vector3& v);
			Vector3 getLookAt();
			void setLookAt(const Vector3& v);
			Vector3 getUpVector();
			void setUpVector(const Vector3& v);

			virtual void move(Vector3& t);
			virtual void rotate(double rX, double rY, double rZ);

			void setTopView();
			void setFrontView();
			void setSideView();
			void set3DView();
		};

		class SphericalCamera : public Camera
		{
		protected:
			double radius;
			double alpha;
			double beta;
		public:
			SphericalCamera();
			void move(Vector3& t);
			void rotate(double rX, double rY, double rZ);
		};
	}
}