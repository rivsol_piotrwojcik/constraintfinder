#pragma once
#include "GLBasicShape.h"
#include "Cuboid.h"
using namespace ConstraintFinder::Core;

namespace ConstraintFinder
{
	namespace Graphics
	{
		/*! \biref A local axis aligned cuboid displayable using OpenGL.
		* 
		* Extends the Cuboid and GLDisplayable classes.
		* \sa Cuboid, GLDisplayable
		*/
		class GLCuboid : public Cuboid, public GLBasicShape
		{
		protected:
			void createDisplayList();
		public:
			GLCuboid(unsigned int id, double eX, double eY, double eZ);
			void renderGL();
			void callMyList();
			Entity* toEntity();
			GLEntity* queryId(unsigned int id);
		};
	}
}