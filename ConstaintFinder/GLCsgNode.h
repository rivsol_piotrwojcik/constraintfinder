#pragma once
#include "GLBasicShape.h"
#include "CsgNode.h"
#include "IdMaster.h"
using namespace ConstraintFinder::Core;

namespace ConstraintFinder
{
	namespace Graphics
	{
		class GLCsgNode : public GLEntity
		{
		protected:
			GLCsgNode* left;
			GLCsgNode* right;
			CSG_Operation operation;
			GLBasicShape* primitive;

			CsgNode* core;
			vector<vector<Primitive*>> CsgPrimitives;

		public:
			GLCsgNode(unsigned int id, GLCsgNode* left, GLCsgNode* right, CSG_Operation op);
			GLCsgNode(GLBasicShape* primitive);
			GLCsgNode(GLCsgNode* n);
			GLCsgNode* getLeftChild();
			GLCsgNode* getRightChild();
			CSG_Operation getOperation();
			void setOperation(CSG_Operation op);
			void setLeftChild(GLCsgNode* newLeft);
			void setRightChild(GLCsgNode* newRight);
			GLBasicShape* getPrimitive();
			bool isPrimitive();
			int treeSize();

			void renderGL();
			void createDisplayList();
			void callMyList();
			Entity* toEntity();
			GLEntity* queryId(unsigned int id);
		};

		/*!
		* \brief This class contains methods for normalizing CSG geometry.
		* The class is a wrapper around the normalization algorithm for CSG trees.
		* \sa GLCsgNode, OpenCSG
		*/
		class GLCsgNormalizer
		{
		private:
			/*! Tests whether the node matches the left side of any of the equivalences in the CSG tree normalization algorithm. If a match is found the equivalence is applied.
			* \param n the node to test.
			* \return 0 if the node does not match any of the equivalences. Otherwise the number of the eqivalence is returned (1-8).
			*/
			static int detectEquivalence(GLCsgNode* n);
			/*! Applies equivalence 1 to the specified node. Helper method for tree normalization.
			* \param the node to transform.
			*/
			static void applyEq1(GLCsgNode* n);
			/*! Applies equivalence 2 to the specified node. Helper method for tree normalization.
			* \param the node to transform.
			*/
			static void applyEq2(GLCsgNode* n);
			/*! Applies equivalence 3 to the specified node. Helper method for tree normalization.
			* \param the node to transform.
			*/
			static void applyEq3(GLCsgNode* n);
			/*! Applies equivalence 4 to the specified node. Helper method for tree normalization.
			* \param the node to transform.
			*/
			static void applyEq4(GLCsgNode* n);
			/*! Applies equivalence 5 to the specified node. Helper method for tree normalization.
			* \param the node to transform.
			*/
			static void applyEq5(GLCsgNode* n);
			/*! Applies equivalence 6 to the specified node. Helper method for tree normalization.
			* \param the node to transform.
			*/
			static void applyEq6(GLCsgNode* n);
			/*! Applies equivalence 7 to the specified node. Helper method for tree normalization.
			* \param the node to transform.
			*/
			static void applyEq7(GLCsgNode* n);
			/*! Applies equivalence 8 to the specified node. Helper method for tree normalization.
			* \param the node to transform.
			*/
			static void applyEq8(GLCsgNode* n);
		public:
			/*! Normlizes a CSG tree. Normalizaion is defined by tranforming the tree into a form where the union operators are topmost.
			*  \param t the root node of the CSG tree to normalize.
			*/
			static void normalizeTree(GLCsgNode* t);
			/*! Splits a normalized CSG tree into a list of non-union nodes. Helper method for CSG tree decomposition.
			* \param t the tree to be split
			* \param list of the non-union nodes.
			*/
			static void splitToProductNodes(GLCsgNode* t, vector<GLCsgNode*> *baseNodes);
			/*! Decomposes a normalized CSG tree into a form processible by the OpenCSG rendering library.
			*  \param nt the root node of the CSG tree to decompose.
			*  \exception std::exception The CSG tree is not normalized.
			*  \return a vector of vectors of OpenCSG::Primitive objects representing parts of the tree to render.
			*/
			static vector<vector<Primitive*>> docomposeTree(GLCsgNode* nt);
		};
	}
}