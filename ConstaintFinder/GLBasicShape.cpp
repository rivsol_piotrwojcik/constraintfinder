#include "GLBasicShape.h"

namespace ConstraintFinder
{
	namespace Graphics
	{
		GLBasicShape::GLBasicShape()
			:Primitive(Intersection, 1)
		{
		}

		void GLBasicShape::render()
		{
			this->renderGL();
		}

		unsigned int UnitShapes::unitCube = 0;
		unsigned int UnitShapes::unitSphere = 0;
		unsigned int UnitShapes::unitCylinder = 0;

		unsigned int UnitShapes::initUnitCube()
		{
			try
			{
				GLuint id = glGenLists(1);
				glNewList(id, GL_COMPILE);
				glutSolidCube( 1.0 );
				glEndList();
				return (unsigned int)id;
			}
			catch (exception e)
			{
				std::cout<<"UnitShapes::initUnitCube -> failed to create display list";
				throw;
			}
		}

		unsigned int UnitShapes::initUnitSphere()
		{
			try
			{
				GLuint id = glGenLists(1);
				glNewList(id, GL_COMPILE);
				glutSolidSphere( 1.0, GEOMETRY_SLICES, GEOMETRY_STACKS );
				glEndList();
				return (unsigned int)id;
			}
			catch (exception e)
			{
				std::cout<<"UnitShapes::initUnitSphere -> failed to create display list";
				throw;
			}
		}

		unsigned int UnitShapes::initUnitCylinder()
		{
			try
			{
				GLuint id = glGenLists(1);
				glNewList(id, GL_COMPILE);

				GLUquadricObj* qobj = gluNewQuadric();
				glPushMatrix();
				gluCylinder(qobj, 1.0, 1.0, 1.0, GEOMETRY_SLICES, GEOMETRY_STACKS);
				glPushMatrix();
				glTranslatef(0.0, 0.0, 1.0);
				gluDisk(qobj, 0.0, 1.0, GEOMETRY_SLICES, GEOMETRY_STACKS);
				glPopMatrix();
				glRotatef(180.0f, 0.0f, 1.0f, 0.0f);
				gluDisk(qobj, 0.0, 1.0, GEOMETRY_SLICES, GEOMETRY_STACKS);
				gluDeleteQuadric(qobj);    
				glPopMatrix();

				glEndList();
				return (unsigned int)id;
			}
			catch (exception e)
			{
				std::cout<<"UnitShapes::initUnitCylinder -> failed to create display list";
				throw;
			}
		}
	}
}