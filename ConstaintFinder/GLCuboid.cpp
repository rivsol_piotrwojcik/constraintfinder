#include "GLCuboid.h"

namespace ConstraintFinder
{
	namespace Graphics
	{
		GLCuboid::GLCuboid(unsigned int id, double eX, double eY, double eZ)
			:Cuboid(id, eX, eY, eZ), GLBasicShape()
		{
		}

		void GLCuboid::createDisplayList()
		{
		}

		void GLCuboid::renderGL()
		{
			glPushMatrix();
			glTranslatef((GLfloat)translation.getX(), (GLfloat)translation.getY(), (GLfloat)translation.getZ());
			GLfloat* rot = rotation.toGLfloat16();
			glMultMatrixf(rot);
			delete [] rot;
			glScalef((GLfloat)scaling.getX(), (GLfloat)scaling.getY(), (GLfloat)scaling.getZ());
			this->callMyList();
			glPopMatrix();
		}

		void GLCuboid::callMyList()
		{
			if (UnitShapes::unitCube == 0)
				UnitShapes::unitCube = UnitShapes::initUnitCube();
			glScalef( this->eX, this->eY, this->eZ );
			glCallList(UnitShapes::unitCube);
		}

		Entity* GLCuboid::toEntity()
		{
			return dynamic_cast<Entity*>(this);
		}

		GLEntity* GLCuboid::queryId(unsigned int id)
		{
			if (id == toEntity()->getId())
				return this;
			return NULL;
		}
	}
}