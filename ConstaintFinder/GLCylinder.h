#pragma once
#include "GLBasicShape.h"
#include "Cylinder.h"
using namespace ConstraintFinder::Core;

namespace ConstraintFinder
{
	namespace Graphics
	{
		/*! \biref A local axis aligned cylinder displayable by OpenGL.
		* 
		* Extends the GLDisplayable and Cylinder classes.
		* \sa GLDisplayable, Cylinder
		*/
		class GLCylinder : public GLBasicShape, public Cylinder
		{
		protected:
			void createDisplayList();
		public:
			/*! Constructor. 
			* \param id a unique id assigned to each entity.
			* \param radius radius of the base of the clinder.
			* \param length length of the cluinder along the local z axis
			*/
			GLCylinder(unsigned int id, double radius, double length);
			void renderGL();
			void callMyList();
			Entity* toEntity();
			GLEntity* queryId(unsigned int id);
		};
	}
}