#include "GLCsgNode.h"

namespace ConstraintFinder
{
	namespace Graphics
	{
		GLCsgNode::GLCsgNode(unsigned int id, GLCsgNode *left, GLCsgNode *right, CSG_Operation op)
		{
			this->left = left;
			this->right = right;
			this->operation = op;
			this->primitive = NULL;
			this->core = new CsgNode(id, left->toEntity(), right->toEntity(), op);
		}

		GLCsgNode::GLCsgNode(GLBasicShape *primitive)
		{
			this->left = NULL;
			this->right = NULL;
			this->operation = CSG_None;
			this->primitive = primitive;
			this->core = NULL;
		}

		GLCsgNode::GLCsgNode(GLCsgNode* n)
		{
			if (n->left)
				this->left = new GLCsgNode(n->left);
			else
				this->left = NULL;
			if (n->right)
				this->right = new GLCsgNode(n->right);
			else
				this->right = NULL;
			this->operation = n->operation;
			this->primitive = n->primitive;
			this->core = n->core;
		}

		GLEntity* GLCsgNode::queryId(unsigned int id)
		{
			if (id == toEntity()->getId())
				return this;
			if (!isPrimitive())
			{
				GLEntity* t;
				t = left->queryId(id);
				if (t)
					return t;
				t = right->queryId(id);
				if (t)
					return t;
			}
			return NULL;
		}

		GLCsgNode* GLCsgNode::getLeftChild()
		{
			return this->left;
		}

		GLCsgNode* GLCsgNode::getRightChild()
		{
			return this->right;
		}

		void GLCsgNode::setLeftChild(GLCsgNode *newLeft)
		{
			if (isPrimitive())
				throw exception("GLCsgNode::setLeftChild -> cannot set left child for leaf node.");
			this->left = newLeft;
		}

		void GLCsgNode::setRightChild(GLCsgNode *newRight)
		{
			if (isPrimitive())
				throw exception("GLCsgNode::setLeftChild -> cannot set right child for leaf node.");
			this->right = newRight;
		}

		CSG_Operation GLCsgNode::getOperation()
		{
			return this->operation;
		}

		void GLCsgNode::setOperation(CSG_Operation op)
		{
			if (isPrimitive())
				throw exception("GLCsgNode::setOperation -> cannot set operation for leaf node.");
			this->operation = op;
		}

		GLBasicShape* GLCsgNode::getPrimitive()
		{
			return this->primitive;
		}

		bool GLCsgNode::isPrimitive()
		{
			return this->primitive != NULL;
		}

		int GLCsgNode::treeSize()
		{
			if (isPrimitive())
				return 1;
			return left->treeSize() + right->treeSize() + 1;
		}

		void GLCsgNode::renderGL()
		{
			if (isPrimitive())
			{
				this->primitive->renderGL();
			}
			else
			{
				if (CsgPrimitives.size() == 0)
					createDisplayList();
				OpenCSG::Algorithm algo = OpenCSG::SCS;
				OpenCSG::DepthComplexityAlgorithm depthalgo = OpenCSG::NoDepthComplexitySampling;
				for (vector<vector<Primitive*>>::iterator it = CsgPrimitives.begin(); it != CsgPrimitives.end(); ++it)
				{
					OpenCSG::render(*it, algo, depthalgo);
					glDepthFunc(GL_EQUAL);
					for (vector<Primitive*>::iterator subit = (*it).begin(); subit != (*it).end(); ++subit)
						(*subit)->render();
					glDepthFunc(GL_LESS);
				}
			}
		}

		void GLCsgNode::callMyList()
		{
		}

		Entity* GLCsgNode::toEntity()
		{
			if (isPrimitive())
				return this->primitive->toEntity();
			else
				return core;
		}

		void GLCsgNode::createDisplayList()
		{
			if (!isPrimitive())
			{
				GLCsgNode* root = new GLCsgNode(this);
				GLCsgNormalizer::normalizeTree(root);
				CsgPrimitives = GLCsgNormalizer::docomposeTree(root);
				this->left->createDisplayList();
				this->right->createDisplayList();
			}
		}


		int GLCsgNormalizer::detectEquivalence(GLCsgNode *n)
		{
			if (n->getOperation() == CSG_Difference) //eqivalences 1,3,5
			{
				switch (n->getRightChild()->getOperation())
				{
				case CSG_Union: //eqivalence 1
					applyEq1(n);
					return 1;
					break;
				case CSG_Intersection: //eqivalence 3
					applyEq3(n);
					return 3;
					break;
				case CSG_Difference: //eqivalence 5
					applyEq5(n);
					return 5;
					break;
				};
			}
			else if (n->getOperation() == CSG_Intersection) //equivalences 2,4,6
			{
				switch (n->getRightChild()->getOperation())
				{
				case CSG_Union: //eqivalence 2
					applyEq2(n);
					return 2;
					break;
				case CSG_Intersection: //eqivalence 4
					applyEq4(n);
					return 4;
					break;
				case CSG_Difference: //eqivalence 6
					applyEq6(n);
					return 6;
					break;
				};
			}
			if (n->getLeftChild()->getOperation() == CSG_Union)
			{
				switch (n->getOperation())
				{
				case CSG_Difference: //eqivalence 7
					applyEq7(n);
					return 7;
					break;
				case CSG_Intersection: //eqivalence 8
					applyEq8(n);
					return 8;
					break;
				};
			}
			return 0;
		}

		void GLCsgNormalizer::applyEq1(GLCsgNode* n)
		{
			GLCsgNode* X;
			GLCsgNode* Y;
			GLCsgNode* Z;
			X = n->getLeftChild();
			Y = n->getRightChild()->getLeftChild();
			Z = n->getRightChild()->getRightChild();
			n->setOperation(CSG_Difference);
			delete (n->getRightChild());
			n->setLeftChild(new GLCsgNode(IdMaster::getNextId(), X, Y, CSG_Difference));
			n->setRightChild(Z);
		}

		void GLCsgNormalizer::applyEq2(GLCsgNode* n)
		{
			GLCsgNode* X;
			GLCsgNode* Y;
			GLCsgNode* Z;
			X = n->getLeftChild();
			Y = n->getRightChild()->getLeftChild();
			Z = n->getRightChild()->getRightChild();
			n->setOperation(CSG_Union);
			n->setLeftChild(new GLCsgNode(IdMaster::getNextId(), X, Y, CSG_Intersection));
			n->getRightChild()->setOperation(CSG_Intersection);
			n->getRightChild()->setLeftChild(new GLCsgNode(X));
		}

		void GLCsgNormalizer::applyEq3(GLCsgNode* n)
		{
			GLCsgNode* X;
			GLCsgNode* Y;
			GLCsgNode* Z;
			X = n->getLeftChild();
			Y = n->getRightChild()->getLeftChild();
			Z = n->getRightChild()->getRightChild();
			n->setOperation(CSG_Union);
			n->setLeftChild(new GLCsgNode(IdMaster::getNextId(), X, Y, CSG_Difference));
			n->getRightChild()->setOperation(CSG_Difference);
			n->getRightChild()->setLeftChild(new GLCsgNode(X));
		}

		void GLCsgNormalizer::applyEq4(GLCsgNode* n)
		{
			GLCsgNode* X;
			GLCsgNode* Y;
			GLCsgNode* Z;
			X = n->getLeftChild();
			Y = n->getRightChild()->getLeftChild();
			Z = n->getRightChild()->getRightChild();
			delete n->getRightChild();
			n->setRightChild(Z);
			n->setLeftChild(new GLCsgNode(IdMaster::getNextId(), X, Y, CSG_Intersection));
		}

		void GLCsgNormalizer::applyEq5(GLCsgNode* n)
		{
			GLCsgNode* X;
			GLCsgNode* Y;
			GLCsgNode* Z;
			X = n->getLeftChild();
			Y = n->getRightChild()->getLeftChild();
			Z = n->getRightChild()->getRightChild();
			n->setOperation(CSG_Union);
			n->setLeftChild(new GLCsgNode(IdMaster::getNextId(), X, Y, CSG_Difference));
			n->getRightChild()->setOperation(CSG_Intersection);
			n->getRightChild()->setLeftChild(new GLCsgNode(X));
		}

		void GLCsgNormalizer::applyEq6(GLCsgNode* n)
		{
			GLCsgNode* X;
			GLCsgNode* Y;
			GLCsgNode* Z;
			X = n->getLeftChild();
			Y = n->getRightChild()->getLeftChild();
			Z = n->getRightChild()->getRightChild();
			n->setOperation(CSG_Difference);
			delete n->getRightChild();
			n->setRightChild(Z);
			n->setLeftChild(new GLCsgNode(IdMaster::getNextId(), X, Y, CSG_Intersection));
		}

		void GLCsgNormalizer::applyEq7(GLCsgNode* n)
		{
			GLCsgNode* X;
			GLCsgNode* Y;
			GLCsgNode* Z;
			X = n->getLeftChild()->getLeftChild();
			Y = n->getLeftChild()->getRightChild();
			Z = n->getRightChild();
			n->setOperation(CSG_Union);
			n->getLeftChild()->setOperation(CSG_Difference);
			n->getLeftChild()->setRightChild(new GLCsgNode(Z));
			n->setRightChild(new GLCsgNode(IdMaster::getNextId(), Y, Z, CSG_Difference));
		}

		void GLCsgNormalizer::applyEq8(GLCsgNode* n)
		{
			GLCsgNode* X;
			GLCsgNode* Y;
			GLCsgNode* Z;
			X = n->getLeftChild()->getLeftChild();
			Y = n->getLeftChild()->getRightChild();
			Z = n->getRightChild();
			n->setOperation(CSG_Union);
			n->getLeftChild()->setOperation(CSG_Intersection);
			n->getLeftChild()->setRightChild(new GLCsgNode(Z));
			n->setRightChild(new GLCsgNode(IdMaster::getNextId(), Y, Z, CSG_Intersection));
		}

		void GLCsgNormalizer::normalizeTree(GLCsgNode *t)
		{
			if (t->isPrimitive())
				return;
			do
			{
				while (detectEquivalence(t) != 0) {}
				normalizeTree(t->getLeftChild());
			} while( !(t->getOperation() == CSG_Union  || ( t->getRightChild()->isPrimitive() && t->getLeftChild()->getOperation() != CSG_Union )) );
			normalizeTree(t->getRightChild());
		}

		void GLCsgNormalizer::splitToProductNodes(GLCsgNode *t, vector<GLCsgNode*> *baseNodes)
		{
			if (t->getOperation() != CSG_Union)
				baseNodes->push_back(t);
			else
			{
				splitToProductNodes(t->getLeftChild(), baseNodes);
				splitToProductNodes(t->getRightChild(), baseNodes);
			}
		}

		vector<vector<Primitive*>> GLCsgNormalizer::docomposeTree(GLCsgNode *nt)
		{
			vector<vector<Primitive*>> result;
			vector<GLCsgNode*> baseNodes;
			vector<GLEntity*> shapes;
			vector<CSG_Operation> ops;
			splitToProductNodes(nt, &baseNodes);
			GLCsgNode* temp;
			Primitive* p;
			for (vector<GLCsgNode*>::iterator it = baseNodes.begin(); it != baseNodes.end(); ++it)
			{
				shapes.clear();
				ops.clear();
				temp = *it;
				while (!temp->isPrimitive())
				{
					shapes.push_back(temp->getRightChild()->getPrimitive());
					ops.push_back(temp->getOperation());
					temp = temp->getLeftChild();
				}
				result.push_back(vector<Primitive*>());
				result[result.size()-1].push_back(dynamic_cast<Primitive*>(temp->getPrimitive()));
				for (int i=0; i<(int)shapes.size(); ++i)
				{
					p = dynamic_cast<Primitive*>(shapes[i]);
					if (ops[i] == CSG_Intersection)
						p->setOperation(Intersection);
					else if (ops[i] == CSG_Difference)
						p->setOperation(Subtraction);
					result[result.size()-1].push_back(p);
				}
			}
			shapes.clear();
			ops.clear();
			baseNodes.clear();
			temp = NULL;
			p = NULL;
			return result;
		}
	}
}